#!/usr/bin/env python
import rospy

from sensor_msgs.msg import JointState

class ChangeBrainState(Exception):
	def __init__(self, newStateName, *arg, **kwarg):
		self.newStateName = str(newStateName)
		self.arg = arg
		self.kwarg = kwarg

	def __str__(self):
		return str(self.newStateName)

class FSMBrainState(object):
	__GLOBAL_VARIABLE = {}
	def __init__(self, name):
		self.name = str(name)
		self.subBrains = { "None" : None }
		self.__currentSubBrain = None
		self.__currentSubBrainName = None
		self.__previousSubBrain = None
		self.__previousSubBrainName = None
		self.__subBrainFirstStep = True
		self.__leavingPrevSubBraub = False
		self.__firstsubBrain = None
		self.__firstsubBrainName = "None"

		self.rosInterface = None
		self.config = None

	def initialize( self ):
		'''	initialize function
			Override for get config or somethig after __init__ function
		'''
		return

	def initializeBrain(self, rosInterface, config = None):
		self.rosInterface = rosInterface
		self.config = config
		self.initialize()
		self.initialSubBrain()

	def initialSubBrain(self):
		for name,sub in self.subBrains.items():
			if sub is None:
				continue
			sub.initializeBrain(self.rosInterface, config = self.config)

	def end(self):
		self.endSubBrain()

	def endSubBrain(self):
		for name,sub in self.subBrains.items():
			if sub is None:
				continue
			sub.end()

	def __setCurrentSubBrain(self, name):
		self.__currentSubBrain = self.subBrains[name]
		self.__currentSubBrainName = name
		self.__subBrainFirstStep = True

	def __setPreviousSubBrain(self, name):
		self.__previousSubBrain = self.subBrains[name]
		self.__previousSubBrainName = name
		self.__leavingPrevSubBraub = True

	def __doLeavingSubBrainCallback( self ):
		if not self.__leavingPrevSubBraub:
			return

		if self.__previousSubBrain is None:
			self.__leavingPrevSubBraub = False
			return

		self.__previousSubBrain.leaveStateCallBack( )
		self.__leavingPrevSubBraub = False

	def addSubBrain(self, subBrain, name=None):
		if name is None:
			name = subBrain.name
		assert not self.subBrains.has_key(name), "{} already exist.".format(str(name))
		self.subBrains[name] = subBrain
		self.setFirstSubBrain( name )
		# self.__setCurrentSubBrain(name)
		# self.__currentSubBrain = self.subBrains[name]
		# self.__currentSubBrainName = name

	def setFirstSubBrain(self, name):
		assert self.subBrains.has_key(name), "{} sub brain is not exist".format(name)
		self.__setCurrentSubBrain(name)
		self.__firstsubBrain = self.subBrains[name]
		self.__firstsubBrainName = name
		# self.__currentSubBrain = self.subBrains[name]

	def __visitSubBrain(self):
		if self.__leavingPrevSubBraub:
			return

		if self.__currentSubBrain is None:
			return

		try:
			if self.__subBrainFirstStep:
				self.__currentSubBrain.firstUpdate()
				self.__subBrainFirstStep = False
			else:
				self.__currentSubBrain.update()
		except ChangeBrainState as e:
			newStateName = e.newStateName
			if not self.subBrains.has_key(newStateName):
				rospy.logerr("{} do not have sub brain name {}. What the fuck should I do next. Fuck you who wrote this code!!!!!!!!!!.".format(self.name, newStateName))
				return
			# self.__previousSubBrain = self.__currentSubBrain
			# self.__previousSubBrainName = self.__currentSubBrainName
			# self.__currentSubBrain = self.subBrains[newStateName]
			# self.__currentSubBrainName = newStateName
			self.__setPreviousSubBrain(self.__currentSubBrainName)
			self.__setCurrentSubBrain(newStateName)

	def SignalChangeSubBrain(self, name):
		raise ChangeBrainState(name)

	# def SignalChangeToPreviousSubBrain(self):
	# 	raise ChangeBrainState(self.__previousSubBrainName)

	def ChangeSubBrain(self, newStateName):
		if not self.subBrains.has_key(newStateName):
			rospy.logerr("{} do not have sub brain name {}. What the fuck should I do next. Fuck you who wrote this code!!!!!!!!!!.".format(self.name, newStateName))
			return
		if self.__currentSubBrainName == newStateName:
			return
		# self.__previousSubBrain = self.__currentSubBrain
		# self.__previousSubBrainName = self.__currentSubBrainName
		# self.__currentSubBrain = self.subBrains[newStateName]
		# self.__currentSubBrainName = newStateName
		self.__setPreviousSubBrain(self.__currentSubBrainName)
		self.__setCurrentSubBrain(newStateName)

	def resetState(self):
		self.__setCurrentSubBrain(self.__firstsubBrainName)
		self.__previousSubBrain = None
		self.__previousSubBrainName = None

	def firstStep(self):
		pass

	def firstUpdate(self):
		self.resetState( )
		self.firstStep()
		self.__visitSubBrain()
		self.__doLeavingSubBrainCallback()

	def step(self):
		pass

	def update(self):
		self.step()
		self.__visitSubBrain()
		self.__doLeavingSubBrainCallback( )

	def leaveStateCallBack( self ):
		pass

	@property
	def prevSubBrainName(self):
		return self.__previousSubBrainName

	@property
	def currSubBrainName(self):
		return self.__currentSubBrainName

	## ROS helper function.
	def getJointStateFromList(self, jointName, jointPos, jointVel, effort):
		assert len(jointName) == len(jointPos)
		return JointState(	name=jointName,
							position=jointPos,
							velocity=jointVel,
							effort=effort)
							
	def setGlobalVariable( self, name, value ):
		assert type( name ) == str
		self.__GLOBAL_VARIABLE[ name ] = value
		
	def getGlobalVariable( self, name ):
		return self.__GLOBAL_VARIABLE[ name ] if self.__GLOBAL_VARIABLE.has_key( name ) else None
