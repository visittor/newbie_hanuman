#!/usr/bin/env python

from newbie_hanuman.srv import MotorCortexCommand,MotorCortexCommandResponse
from newbie_hanuman.srv import PanTiltPlannerCommand
from newbie_hanuman.srv import reqMotorCortexIntegration
from newbie_hanuman.srv import local_map_service


from newbie_hanuman.msg import HanumanStatusMsg
from newbie_hanuman.msg import gameState, TeamInfo, RobotInfo
from newbie_hanuman.srv import Localization_command

from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose2D

from std_msgs.msg import String

from RosInterface import RosInterface
from visionManager.visionModule import KinematicModule

from gameController.gamestate import GameState

import rospy

class HanumanRosInterface(RosInterface):

	def __init__(self):
		super(HanumanRosInterface, self).__init__()
		self.interfaceWithService(	"motor_cortex_command", 
									MotorCortexCommand, 
									"LocoCommand")

		self.interfaceWithService(	"request_integration", 
									reqMotorCortexIntegration, 
									"getWalkingDistance")

		self.interfaceWithService( "pantiltplannercommand",
									PanTiltPlannerCommand,
									"Pantilt")

		self.interfaceWithService( "local_map_info",
									local_map_service,
									"local_map")

		self.interfaceWithPublisher("/spinalcord/hanuman_status", 
									HanumanStatusMsg, 
									"robotStatus")

		self.interfaceWithPublisher("/game_state", 
									String, 
									"_gameState" )
									
		self.interfaceWithPublisher( '/spinalcord/sternocleidomastoid_position',
									JointState,
									'pantiltJS' )

		self.interfaceWithPublisher( '/localization/robot_pose',
									 Pose2D,
									 'robot_pose',
									 )

		self.interfaceWithService( '/Localization/command',
									Localization_command,
									'localization_command'
									)

		self.waitAllInterface( 1 )

	def setKinematicInterface(self, msgType):

		self.interfaceWithPublisher("/vision_manager/kinematic_topic",
									msgType,
									"visionManager",
									initialVal = msgType() )

	@property
	def gameState( self ):
		if self._gameState == None:
			return None
		parseState = GameState.parse( self._gameState.data )
		return parseState
