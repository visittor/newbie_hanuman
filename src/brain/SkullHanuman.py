#!/usr/bin/env python
import rospy

from cell.nodeCell import NodeBase
from HanumanRosInterface import HanumanRosInterface
from brain.brainState import FSMBrainState
from visionManager.visionModule import KinematicModule
from utility.utility import load_module

import configobj

class HanumanSkull(NodeBase):
	def __init__(self):
		super(HanumanSkull, self).__init__("frontal_cortex")
		self.__main_brain = None
		self.rosInitNode()

		self.__brainFirstStep = True
		# self.setFrequencyFromParam("/brain/brain_frequency")
		self.rosInterface = HanumanRosInterface()
		self.setFrequency( 15 )

	def setMainBrain(self, main_brain):
		self.__main_brain = FSMBrainState( 'Main' )
		self.__main_brain.addSubBrain( main_brain )

	def initial(self, main_brain):
		self.setMainBrain(main_brain)
		config = self.getRobotConfig( )
		self.__main_brain.initializeBrain(self.rosInterface, config = config)

	def __firstStep(self):
		self.__main_brain.firstUpdate()

	def run(self):
		rospy.loginfo("Start frontal_cortex node")
		while not rospy.is_shutdown():
			if self.__brainFirstStep:
				self.__firstStep()
				self.__brainFirstStep = False
				continue
			self.__main_brain.update()
			self.sleep()

	def end(self):
		self.__main_brain.end()
		rospy.loginfo("Close frontal_cortex node.")

def main():

	fileName = rospy.get_param("/brain/brain_module_path", '')
	if fileName == '':
		rospy.logwarn("NO BRAIN!!")
		module = FSMBrainState("Default")
	else:
		brain_module = load_module(fileName)
		assert hasattr( brain_module, 'main_brain')
		module = brain_module.main_brain

	fileName = rospy.get_param("/vision_manager/vision_module_path", '')
	if fileName == '':
		visionModule = KinematicModule()
	else:
		vision_module = load_module(fileName)
		assert hasattr( vision_module, 'kinematic_module' )
		visionModule = vision_module.kinematic_module

	node = HanumanSkull()
	node.rosInterface.setKinematicInterface( visionModule.posDictMsgType )

	try:
		node.initial(module)
		node.run()
	except rospy.ROSInterruptException as e:
		rospy.logwarn(str(e))
	finally:
		node.end()