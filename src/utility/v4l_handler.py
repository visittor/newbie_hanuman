#!/usr/bin/env python
#
# Copyright (C) 2018  FIBO/KMUTT
#			Written by Nasrun Hayeeyama
#

########################################################
#
#	STANDARD IMPORTS
#

import sys
import os

import subprocess

import time

########################################################
#
#	LOCAL IMPORTS
#

import configobj

########################################################
#
#	GLOBALS
#

CAMERA_DEVICE_DEFAULT = '/dev/video0'
EXTERNAL_CAMERA_DEVICE = '/dev/video1'

########################################################
#
#	EXCEPTION DEFINITIONS
#

########################################################
#
#	HELPER FUNCTIONS
#

########################################################
#
#	CLASS DEFINITIONS
#

class V4L_Handler( object ):

	def __init__( self, cameraDevice, configPath ):
		"""
		For setting up camera parameter
			cameraID : identify camera device (expect external webcam camera)
			configPath : robot config format ini
		"""

		#   get dictionary
		cameraParametersDict = configobj.ConfigObj( configPath )[ "CameraParameters" ]
		
		if type( cameraDevice ) == int:

			#   Get camera device
			if cameraDevice == 0:
				self.cameraDevice = CAMERA_DEVICE_DEFAULT
			elif cameraDevice == 1:
				self.cameraDevice = EXTERNAL_CAMERA_DEVICE
		
		elif type( cameraDevice ) == str:
			
			self.cameraDevice = cameraDevice

		#
		#   set parameter
		#
		self.whiteBalanceTemperatureAuto = int( cameraParametersDict[ "white_balance_temperature_auto" ] ) 
		self.whiteBalanceTemperature     = int( cameraParametersDict[ "white_balance_temperature" ] )
		self.exposureAutoPriority        = int( cameraParametersDict[ "exposure_auto_priority" ] )
		self.exposureAuto                = int( cameraParametersDict[ "exposure_auto" ] )
		self.exposureAbsolute            = int( cameraParametersDict[ "exposure_absolute" ] )
		self.brightness                  = int( cameraParametersDict[ "brightness" ] )
		self.contrast                    = int( cameraParametersDict[ "contrast" ] )
		self.saturation                  = int( cameraParametersDict[ "saturation" ] )
		self.gain                        = int( cameraParametersDict[ "gain" ] )
		self.sharpness                   = int( cameraParametersDict[ "sharpness" ] )
		self.focusAuto                   = int( cameraParametersDict[ "focus_auto" ] )

	def setAutoWhiteBalanceTemperature( self, logicEnable = None ):
		"""
		set auto white balance temperature
			parameter:
				logicEnable : True if need auto balance, false if don't need.
		"""
		
		if logicEnable is None:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "white_balance_temperature_auto={}".format( self.whiteBalanceTemperatureAuto ) ]
		else:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "white_balance_temperature_auto={}".format( int( logicEnable ) ) ]

		#   execute!
		self.execute( command )

	def setWhiteBalanceTemperature( self, value = None ):
		"""
		set white balance temperature
			parameter:
				value : white balance value range -> ( 2000, 6500 )
		"""
		if value is None:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "white_balance_temperature={}".format( self.whiteBalanceTemperature ) ]
		else:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "white_balance_temperature={}".format( int( value ) ) ]
		
		#   execute!
		self.execute( command )
		
	def setExposureAutoPriority( self, value = None ):
		"""
		set priority of auto exposure
			parameter:
				value : white balance value range -> ( 2000, 6500 )
		"""
		
		if value is None:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "exposure_auto_priority={}".format( self.exposureAutoPriority ) ]
		else:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "exposure_auto_priority={}".format( int( value ) ) ]

		#   execute!
		self.execute( command )

	def setExposureAuto( self, value = None ):
		"""
		set auto exposure
			parameter:
				value : index menu ( for external webcam have only 1, 3 choices )
		"""
		
		if value is None:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "exposure_auto={}".format( self.exposureAuto ) ]
		else:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "exposure_auto={}".format( int( value ) ) ]

		#   execute!
		self.execute( command )

	def setExposureAbsolute( self, value = None ):
		"""
		set absolute exposure value
			parameter:
				value : absolute exposure value range -> ( 3, 1, 2047 )
		"""
		
		if value is None:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "exposure_absolute={}".format( self.exposureAbsolute ) ]
		else:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "exposure_absolute={}".format( int( value ) ) ]

		#   execute!
		self.execute( command )        

	def setBrightness( self, value = None ):
		"""
		set brightness value
			parameter:
				value : brightness value range -> ( 0, 255 )
		"""
		
		if value is None:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "brightness={}".format( self.brightness ) ]
		else:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "brightness={}".format( int( value ) ) ]

		#   execute!
		self.execute( command )

	def setContrast( self, value = None ):
		"""
		set contrast value
			parameter:
				value : contrast value range -> ( 0, 255 )
		"""
		if value is None:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "contrast={}".format( self.contrast ) ]
		else:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "contrast={}".format( int( value ) ) ]

		#   execute!
		self.execute( command )

	def setSaturation( self, value = None ):
		"""
		set saturation value
			parameter:
				value : saturation value range -> ( 0, 255 )
		"""
		if value is None:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "saturation={}".format( self.saturation ) ]
		else:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "saturation={}".format( int( value ) ) ]

		#   execute!
		self.execute( command )
	
	def setGain( self, value = None ):
		"""
		set gain value
			parameter:
				value : gain value range -> ( 0, 255 )
		"""
		if value is None:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "gain={}".format( self.gain ) ]
		else:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "gain={}".format( value ) ]
		
		#   execute!
		self.execute( command )

	def setSharpness( self, value = None ):
		"""
		set sharpness value
			parameter:
				value : sharpness value range -> ( 0, 255 )
		"""
		if value is None:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "sharpness={}".format( self.sharpness ) ]
		else:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "sharpness={}".format( value ) ]

		#   execute!
		self.execute( command )

	def setAutoFocus( self, logicEnable = None ):
		"""
		set auto focus
			parameter:
				logicEnable : True if need auto focus, false if don't need.
		"""
		if logicEnable is None:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "focus_auto={}".format( self.focusAuto ) ]
		else:
			command = [ "v4l2-ctl", "-d", self.cameraDevice, "-c", "focus_auto={}".format( logicEnable ) ]

		#   execute
		self.execute( command )

	def executeAllCommandByDefaultArgument( self ):
		"""
		call all function
		"""
		
		#
		#   call all set function
		#
		self.setAutoWhiteBalanceTemperature()
		self.setWhiteBalanceTemperature()
		self.setAutoFocus()
		self.setExposureAutoPriority()
		self.setExposureAuto()
		self.setExposureAbsolute()
		self.setBrightness()
		self.setContrast()
		self.setSharpness()
		self.setSaturation()
		self.setGain()

	def execute( self, command ):
		"""
		execute input command by subprocess
		"""
		#   call subprocess for execute
		subprocess.Popen( command )

		#   wait!
		time.sleep( 0.25 )

if __name__ == "__main__":
	
	camSetup = V4L_Handler( 1, '../../config/robot_config.ini' )

	camSetup.executeAllCommandByDefaultArgument()
