import numpy as np
from numpy import sin as s
from numpy import cos as c

import configobj

import math
## Robot dimension in m
# _H = 0.46
# _L = 0.05
# _h1 = 0.02
# _l1 = 0.03
# _h2 = 0.1
# _l2 = 0.02
# _lx = 0.005
# _Phi = math.radians( 5.0 )
_H = 4.41522754e-01
_L = 8.78644672e-03
_h1 = 0.0205
_l1 = 0.01075
_h2 = 0.046
_l2 = 0.034
_lx = 0.002
_Phi = math.radians( 1.43430832e+01 )
_Beta = math.radians( 0.0 )
_Alpha = math.radians( 0.0 )

_prefix = 1e-0

H = _H
L = _L
h1 = _h1
l1 = _l1
h2 = _h2
l2 = _l2
lx = _lx
Phi = _Phi
Beta = _Beta
Alpha = _Alpha

def __getTransformationMatrixDHTable_1Row( theta, d, r, alpha ):

	rot_z = np.array( [ [c(theta), 	-s(theta), 	0, 	0],
						[s(theta), 	c(theta),	0, 	0],
						[0,			0,			1,	0],
						[0,			0,			0,	1] ] )

	tran_z = np.array( [[1,	0,	0,	0],
						[0,	1,	0,	0],
						[0,	0,	1,	d],
						[0,	0,	0,	1]] )

	tran_x = np.array( [[1,	0,	0,	r],
						[0,	1,	0,	0],
						[0,	0,	1,	0],
						[0,	0,	0,	1]])

	rot_x = np.array( [	[1,	0,			0,			0],
						[0,	c(alpha),	-s(alpha),	0],
						[0,	s(alpha),	c(alpha),	0],
						[0,	0,			0,			1]])

	return np.matmul( rot_z, np.matmul( tran_z, np.matmul( tran_x , rot_x ) ) )

def __getTransformationMatrixDHTable( dh ):

	tranMat = np.eye(4)

	for theta, d, r, alpha in dh:

		tranMat = np.matmul( tranMat, __getTransformationMatrixDHTable_1Row(theta, d, r, alpha) )

	return tranMat

def getMatrixForForwardKinematic( *q ):
	'''
	Forward kinematic from robot base to camera.
	DH table
	=================================================
	| index	|	theta	|	d 	|	r 	| 	alpha	|
	=================================================
	|	1	|	0		|	H	|	L	|	-pi/2	|
	-------------------------------------------------
	|	2	|	phi		|	0	|	0	| pi/2+Beta	|
	=================================================
	DH table from robot base to pan-tilt base.^
	-------------------------------------------------
	DH table from pan-tilt base to camera. v
	=================================================
	|	3	|	q1		|	h1	|	l1	|	-pi/2	|
	-------------------------------------------------
	|	4	|	q2-pi/2	|	0	|	h2	|	-pi/2	|
	-------------------------------------------------
	|	5	|	pi/2	|	l2	|	lx	|	0		|
	-------------------------------------------------
	|	6	|	alpha  	|	0	|	0	|	0		|
	=================================================
	**NOTE: q[0] == pan, q[1] == tilt
	'''
	global H, L, h1, l1, h2, l2, Phi, lx, Beta, Alpha
	q1 = q[0]
	q2 = q[1]
	q3 = q2 - ( np.pi / 2 )

	H_ = _prefix * H
	L_ = _prefix * L
	h1_ = _prefix * h1
	l1_ = _prefix * l1
	h2_ = _prefix * h2
	l2_ = _prefix * l2
	lx_ = _prefix * lx
	# print 
	
	# T_R_j3 = [
	# 			[  c(Phi)*s(q1), 	-c(q2)*s(Phi) - c(Phi)*c(q1)*s(q2),		c(Phi)*c(q1)*c(q2) - s(Phi)*s(q2), 		L_ + h1_*s(Phi) + l1_*c(Phi)*c(q1) + h2_*c(q2)*s(Phi) - l2_*s(Phi)*s(q2) + l2_*c(Phi)*c(q1)*c(q2) + h2_*c(Phi)*c(q1)*s(q2) - c(Phi)*s(q1)*lx_],
	# 			[  -c(q1),          -s(q1)*s(q2),                     		c(q2)*s(q1),                        	s(q1)*(l1_ + l2_*c(q2) + h2_*s(q2))],
	# 			[  -s(Phi)*s(q1),   c(q1)*s(Phi)*s(q2) - c(Phi)*c(q2), 		-c(Phi)*s(q2) - c(q1)*c(q2)*s(Phi), 	H_ + h1_*c(Phi) + h2_*c(Phi)*c(q2) - l1_*c(q1)*s(Phi) - l2_*c(Phi)*s(q2) - l2_*c(q1)*c(q2)*s(Phi) - h2_*c(q1)*s(Phi)*s(q2)],
	# 			[  0,               0,                                  	0,                                  	1]
	# 		 ]
	# T_R_j3 = np.array( T_R_j3, dtype = np.float64 )

	dh = [ 	(0, 		H_, 	L_, 	-np.pi/2),
			(Phi,		0,		Beta,	(np.pi/2) + Beta ),
			(q1,		h1_,	l1_,	-np.pi/2),
			(q3,		0,		h2_,	-np.pi/2),
			(np.pi/2,	l2_,	lx_,	0	),
			(Alpha,		0,		0,		0	),	]

	# print "new"
	# print np.around(__getTransformationMatrixDHTable( dh ), 7)
	# print "old"
	# print np.around(T_R_j3, 7)

	return __getTransformationMatrixDHTable( dh )



def setPrefix( n ):
	'''
	Set prefix for length. Currently this module use meters.
	argument:
		n 	:	( float ) prefix.
	'''
	global _prefix

	_prefix = n

def setNewRobotConfiguration( HNew, LNew, h1New, l1New, h2New, l2New, lxNew, phiNew, betaNew, alphaNew ):
	'''
	Set robot configuration.
	H, L, h1, l1, h2, l2, lx and phi respectively.
	Here is DH-table for forwardkinematic.

	Forward kinematic from robot base to camera.
	DH table
	=================================================
	| index	|	theta	|	d 	|	r 	| 	alpha	|
	=================================================
	|	1	|	0		|	H	|	L	|	-pi/2	|
	-------------------------------------------------
	|	2	|	phi		|	0	|  	beta|	 pi/2	|
	=================================================
	DH table from robot base to pan-tilt base.^
	-------------------------------------------------
	DH table from pan-tilt base to camera. v
	=================================================
	|	3	|	q1		|	h1	|	l1	|	-pi/2	|
	-------------------------------------------------
	|	4	|	q2-pi/2	|	0	|	h2	|	-pi/2	|
	-------------------------------------------------
	|	5	|	pi/2	|	l2	|	lx	|	0		|
	=================================================
	
	arguments:
		H, L, h1, l1, h2, l2, lx and phi respectively. ( float )

	'''
	global H, L, h1, l1, h2, l2, Phi, lx, Beta, Alpha

	H = HNew
	L = LNew
	h1 = h1New
	l1 = l1New
	h2 = h2New
	l2 = l2New
	lx = lxNew
	Phi = math.radians( phiNew )
	Beta = math.radians( betaNew )
	Alpha = math.radians( alphaNew )

def getRobotConfiguration( ):
	return H, L, h1, l1, h2, l2, lx, math.degrees(Phi), math.degrees(Beta), math.degrees(Alpha)

def resetConfiguration( ):
	global H, L, h1, l1, h2, l2, Phi, lx, Beta, Alpha

	H = _H
	L = _L
	h1 = _h1
	l1 = _l1
	h2 = _h2
	l2 = _l2
	lx = _lx
	Phi = _Phi
	Beta = _Beta
	Alpha = _Alpha

def loadDimensionFromConfig( filePath ):
	'''
	Load robot dimension from config file.
	argument:
		filePath	:	( str )
	'''

	config = configobj.ConfigObj( filePath )
	
	if not config.has_key( 'RobotDimension' ):
		print "WARNING : robot config not contain \'RobotDimension\' section."
		return

	global H, L, h1, l1, h2, l2, Phi, lx, Beta, Alpha

	config = config[ 'RobotDimension' ]

	H = float( config['H'] )
	L = float( config['L'] )
	h1 = float( config['h1'] )
	l1 = float( config['l1'] )
	h2 = float( config['h2'] )
	l2 = float( config['l2'] )
	lx = float( config['lx'] )
	Phi = math.radians( float( config['Phi'] ) )
	Beta = math.radians( float( config['Beta'] ) )
	Alpha = math.radians( float( config['Alpha'] ) )

def saveDimension( filePath ):
	'''
	Save current robot dimension to config file.
	arguments:
		filePath	:	( str )
	'''
	dimensionDict = { 	'H' : 	H,
						'L'	:	L,
						'h1':	h1,
						'l1':	l1,
						'h2':	h2,
						'l2':	l2,
						'lx':	lx,
						'Phi':	math.degrees( Phi ),
						'Beta': math.degrees( Beta ),
						'Alpha': math.degrees( Alpha )
					}

	try:
		config = configobj.ConfigObj( filePath )
	except Exception:
		config = configobj.ConfigObj()
		
	config[ 'RobotDimension' ] = dimensionDict

	config.filename = filePath

	config.write()