from sensor_msgs.msg import CompressedImage, Image
from std_msgs.msg import Header

import numpy as np
import cv2
import sys
import rospy

numpyTypeToCVTypeDict = {'uint8': '8U', 'int8': '8S', 'uint16': '16U',
						 'int16': '16S', 'int32': '32S', 'float32': '32F',
						 'float64': '64F'}




cvTypeToNumpyTypeDict = dict( (v,k) for k,v in numpyTypeToCVTypeDict.items() )

NumpyTypeLookup = { '8': 'uint8', '16': 'uint16'}

def getEncodingType( dtype, nChannel ):
	return '%sC%d'%( numpyTypeToCVTypeDict[dtype.name], nChannel )

def getDtypeAndNumChannel( encoding ):
	
	if 'C' in encoding:
		sepIdx = 0
		for i,char in enumerate(encoding):
			if char == 'C':
				sepIdx = i
		dtypeStr = cvTypeToNumpyTypeDict[encoding[:sepIdx]]
		nChannel = int(encoding[sepIdx+1:])

	else:
		sepIdx = 0
		for i,char in enumerate(encoding):
			if char.isdigit():
				sepIdx = i
				break

		dtypeStr = NumpyTypeLookup[encoding[sepIdx:]]
		nChannel = sepIdx

	return dtypeStr, nChannel

def cvtCVImageToImageMessage( cvImg, encodingType = 'bgr8' ):

	imgMsg = Image()

	stamp = rospy.Time.now()
	header = Header(stamp = stamp)

	imgMsg.height, imgMsg.width = cvImg.shape[:2]

	if cvImg.ndim == 3:
		encoding = getEncodingType( cvImg.dtype, cvImg.shape[2] )
	elif cvImg.ndim == 2:
		encoding = getEncodingType( cvImg.dtype, 1 )
	else:
		ValueError( "cvImg should have ndim of 2 or 3" )

	if encodingType is None:
		imgMsg.encoding = encoding
	else:
		imgMsg.encoding = 'bgr8'

	if cvImg.dtype.byteorder == '>':
		imgMsg.is_bigendian = True

	imgMsg.data = cvImg.tostring()
	imgMsg.step = int( len( imgMsg.data ) / imgMsg.width )
	imgMsg.header = header

	return imgMsg

def cvtImageMessageToCVImage( imgMsg ):

	encoding = imgMsg.encoding
	height, width = imgMsg.height, imgMsg.width

	dtypeStr, nChannel = getDtypeAndNumChannel( encoding )
	byteOrder = '>' if imgMsg.is_bigendian else '<'
	dtype = np.dtype( dtypeStr ).newbyteorder( byteOrder )
	
	cvImg = np.ndarray(shape=(imgMsg.height, imgMsg.width, nChannel),
				dtype=dtype, buffer=imgMsg.data)

	# If the byt order is different between the message and the system.
	if imgMsg.is_bigendian == (sys.byteorder == 'little'):
		cvImg = cvImg.byteswap().newbyteorder()

	return cvImg