#!/usr/bin/env python
import numpy as np 
import cv2

def Project2Dto3D(point, HomoMat, intrinMat):

	invHomo = getInverseHomoMat(HomoMat)

	point = np.array( point )
	assert point.ndim == 2 or point.ndim == 1
	if point.ndim == 2:
		point = point.T

	elif point.ndim == 1:
		point = point[:, np.newaxis ]

	Cpoint_3 = np.matmul(invHomo, np.array([0,0,0,1]) )[:3, np.newaxis]
	z_plane_cam = np.matmul(invHomo, np.array([0,0,1,1]))[:3, np.newaxis]

	invIntrin = np.linalg.inv(intrinMat)

	point = np.vstack( (point, np.ones(point.shape[1])) )
	pPoint = np.matmul(invIntrin, point)

	pPoint = np.vstack( ( pPoint, np.ones(pPoint.shape[1]) ) )
	pPoint_3 = np.matmul(invHomo, pPoint)[:3, :]
	# pPoint_3 = pPoint_3/pPoint_3[2]

	unitVec = pPoint_3 - Cpoint_3
	unitVec /= np.linalg.norm(unitVec, axis = 0)

	# if np.dot(unitVec, z_plane_cam) > 0:
	# 	print np.dot(unitVec, z_plane_cam)
	# 	return None

	lambdaVal = -(Cpoint_3[2, :] / unitVec[2, :])

	finalPoint = Cpoint_3 + lambdaVal*unitVec

	finalPoint = finalPoint.T

	if len(finalPoint) == 1:
		if lambdaVal[0] <= 0:
			return None

		return finalPoint.reshape( -1 )

	return finalPoint

def Project3Dto2D(point, HomoMat, intrinMat):
	point = np.array( point )
	
	assert point.ndim == 1 or point.ndim == 2

	if point.ndim == 1:
		point = np.hstack((point, 1))

		tranPoint = np.matmul(HomoMat, point)[:3]
		if tranPoint[2] < 0:
			return
		tranPoint = np.matmul(intrinMat, tranPoint)
		tranPoint = tranPoint / tranPoint[2]

		return tranPoint[:2]

	point = np.hstack( ( point, np.ones( point.shape[0] ) )).T

	tranPoint = np.matmul( HomoMat, point )[:3]
	
	tranPoint = np.matmul( intrinMat, tranPoint )

	return tranPoint[:2]

def getInverseHomoMat(HomoMat):
	rotMat = HomoMat[:3, :3].T
	tvec = HomoMat[:3,3].reshape(-1,1)
	tvec = -1.0*(np.matmul(rotMat, tvec))

	invMat = np.hstack((rotMat, tvec))
	invMat = np.vstack((invMat, np.array([0,0,0,1],float)))

	# return invMat
	return np.linalg.inv( HomoMat )
