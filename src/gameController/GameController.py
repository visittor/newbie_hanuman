#!/usr/bin/env python
#
# Copyright (C) 2019  FIBO/KMUTT
#			Written by Kongkiet Rothomphiwat, 
#                      Nasrun Hayeeyama
#

########################################################
#
#	STANDARD IMPORTS
#

import sys
import os

import socket

import rospy


########################################################
#
#	LOCAL IMPORTS
#

from newbie_hanuman.msg import gameState, RobotInfo, TeamInfo

from std_msgs.msg import String

from cell.nodeCell import NodeBase

from construct import Container, ConstError
from gamestate import GameState, ReturnData, GAME_CONTROLLER_RESPONSE_VERSION

########################################################
#
#	GLOBALS
#

DefaultTimeOut = 3.0 # s

########################################################
#
#	EXCEPTION DEFINITIONS
#

########################################################
#
#	HELPER FUNCTIONS
#

########################################################
#
#	CLASS DEFINITIONS
#

class Receiver( NodeBase ):

	def __init__( self ):

		super( Receiver, self ).__init__( "GameControllerReceiver" )
		
		#   Init node stuff
		self.rosInitNode()

		self.rosInitPublisher( "/game_state", String )

		config = self.getRobotConfig()

		self.teamNumber = int( config[ "GameControllerParameter" ][ "Team" ] )
		self.playerNumber = int( config[ "GameControllerParameter" ][ "Player" ] )

		self.listeningPort = int( config[ "GameControllerParameter" ][ "ListeningPort" ] )
		self.listeningHost = config[ "GameControllerParameter" ][ "ListeningHost" ] # 0.0.0.0
			   
		self.answerPort = int( config[ "GameControllerParameter" ][ "AnsweringPort" ] )

		self.socket = None

		#   I think it mockup
		self.manualPenalize = False

		self.initSocket()

	def initSocket( self ):

		self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
		self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.socket.bind( ( self.listeningHost, self.listeningPort ) )
		self.socket.settimeout( DefaultTimeOut )

	def receiveFromGameController( self ):

		try:
			data, peer = self.socket.recvfrom( GameState.sizeof() )
			return data, peer

		except AssertionError as ae:
			rospy.logerr( ae.message )
		except socket.timeout:
			rospy.logwarn( "Socket timeout" )
		except ConstError:
			rospy.logwarn( "Parse protocal error, maybe old protocal" )
		except Exception as e:
			rospy.logwarn(  "receiveFromGameController() exception : {}".format( e ) )

	def answerToGameController( self, peerAddress ):
		
		#   Mockup return msg from manual pernalize
		returnMsg = 0 if self.manualPenalize else 2

		#   Build return data
		data = Container(
			hearder = b"RGrt",
			version=GAME_CONTROLLER_RESPONSE_VERSION,
			team=self.teamNumber,
			player=self.playerNumber,
			message=returnMsg
		)

		try:
			addressDestination = peerAddress[ 0 ], self.answerPort
			self.socket.sendto( ReturnData.build( data ), addressDestination )
		except Exception as e:
			rospy.logwarn( "Network error : {}".format( e ) )


	def run( self ):
		rospy.loginfo( "Start GameControllerReceiver node" )
		self.setFrequency( 10 )
		while not rospy.is_shutdown():
			try:
				data, peer = self.receiveFromGameController()
				msg = String( data )
				self.publish( msg )

				self.answerToGameController( peer )
			except Exception as e:
				# rospy.logwarn( "run() exception : {} ".format( e ) )
				pass

			self.sleep()

	def end( self ):
		rospy.loginfo( "Close GameControllerReceiver node" )

def main( ):

	try:
		node = Receiver()
		node.run()
	except rospy.ROSInternalException as e:
		node.end()
