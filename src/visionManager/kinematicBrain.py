#!/usr/bin/env python

import rospy

from message_filters import Subscriber,ApproximateTimeSynchronizer
from sensor_msgs.msg import JointState

from newbie_hanuman.msg import scanlinePointClound, localizationMsg
from newbie_hanuman.msg import HanumanStatusMsg

from cell.nodeCell import NodeBase
from visionModule import KinematicModule
from utility.utility import load_module

import inspect
import os
import imp
import sys
from Queue import Queue
import copy
import collections

class KinematicBrain(NodeBase):

	def __init__(self):
		super(KinematicBrain, self).__init__("kinematic_brain")
		self.__occipitalMessageType = None
		self.__posDictMessageType = None
		self.__motorCortexTopicName = None
		self.__motorCortexMsgType = None

		self.__motorCotexMsg = None

		self.__messageFilter = None

		self.__kinematicFunction = lambda x,y:x
		self.__loop = lambda :None
		self.__module = None

		self.__objMsg = None
		self.__panTiltMsg = None

	def initial(self, module):
		self.__module = module

		self.__module.setConfig( self.getRobotConfig( ) )

		self.setOccipitalMessageType(self.__module.objectsMsgType)
		self.setPosDictMessageType(self.__module.posDictMsgType)
		self.setKinematicFunction(self.__module.kinematicCalculation)
		self.setKinematicLoop(self.__module.loop)
		# self.setMotorCortexTopic(self.__module.motorCortexMsgType, 
		# 						 self.__module.motorCortexTopicName)
		self.__rosInit()
		self.setFrequency(60)

	def setOccipitalMessageType(self, msgType):
		self.__occipitalMessageType = msgType

	def setPosDictMessageType(self, msgType):
		self.__posDictMessageType = msgType

	def setMotorCortexTopic(self, msgType, topicname):
		self.__motorCortexTopicName = topicname
		self.__motorCortexMsgType = msgType

	def setKinematicFunction(self, func):
		self.__kinematicFunction = func

	def setKinematicLoop(self, func):
		self.__loop = func
	
	def __rosInit(self):
		self.rosInitNode()
		self.__initPublisher()
		self.__initMessageFilter()

	def __initPublisher(self):
		self.rosInitPublisher(	"/vision_manager/kinematic_topic", 
								self.__posDictMessageType)

		self.__scanLinePublisher = rospy.Publisher( "/localization/scan_line",
										localizationMsg,
										queue_size = 1 )

	def __initMessageFilter(self):
		# rospy.Subscriber("/vision_manager/occipital_lobe_topic",
		# 				self.__occipitalMessageType,
		# 				self.ddd, 
		# 				queue_size=1)

		# rospy.Subscriber("/spinalcord/sternocleidomastoid_position",
		# 				JointState,
		# 				self.eee, 
		# 		queue_size=1)

		if self.__motorCortexMsgType is not None and self.__motorCortexTopicName is not None:
			rospy.Subscriber(	self.__motorCortexTopicName,
								self.__motorCortexMsgType,
								self.__motorCortexCallback, 
								queue_size=1)

		if self.__module.subscribeMotorCortex:
			rospy.Subscriber(	"/spinalcord/hanuman_status",
								HanumanStatusMsg,
								self.__motorCortexCallback, 
								queue_size=1)

		objSub = Subscriber("/vision_manager/occipital_lobe_topic",
							self.__occipitalMessageType, buff_size = 2**24)
		panTiltSub = Subscriber("/spinalcord/sternocleidomastoid_position",
								JointState)
		self.__messageFilter=ApproximateTimeSynchronizer([objSub,panTiltSub],
													5,
													0.05,
													allow_headerless=True)
		self.__messageFilter.registerCallback(self.__callback)

	# def ddd(self,msg):
	# 	diff = rospy.Time.now().to_sec() - msg.header.stamp.to_sec()
	# 	rospy.logdebug("Image   "+str(msg.header.stamp.to_sec()))

	# def eee(self, msg):
	# 	diff = rospy.Time.now().to_sec() - msg.header.stamp.to_sec()
	# 	rospy.logdebug("Pantilt "+str(msg.header.stamp.to_sec()))	

	def __motorCortexCallback(self, msg):
		self.__motorCotexMsg = msg

	def __callback(self, objMsg, panTiltMsg):
		self.__objMsg = objMsg
		self.__panTiltMsg = panTiltMsg
		
	def run(self):
		rospy.loginfo("Start kinematic_brain node.")
		while not rospy.is_shutdown():

			if self.__objMsg is None or self.__panTiltMsg is None:
				pass

			elif self.__module.subscribeMotorCortex:
				msg = self.__kinematicFunction(	self.__objMsg, 
												self.__panTiltMsg, 
												self.__motorCotexMsg)

				if isinstance( msg, collections.Sequence ):
					if len( msg )==2:
						self.publish( msg[ 0 ] )
						self.__scanLinePublisher.publish( msg[ 1 ])
					else:
						rospy.logwarn( "Expecting sequence of lenght 2 from kinematic function." )

				else:
					self.publish( msg )

			else:
				msg = self.__kinematicFunction(self.__objMsg, self.__panTiltMsg)
				
				if isinstance( msg, collections.Sequence ):
					if len( msg )==2:
						self.publish( msg[ 0 ] )
						self.__scanLinePublisher.publish( msg[ 1 ])
					else:
						rospy.logwarn( "Expecting sequence of lenght 2 from kinematic function." )

				else:
					self.publish( msg )

			self.__loop()
			self.sleep()
		rospy.loginfo("Close kinematic_brain node.")

	def end(self):
		self.__module.end()

def main():
	fileName = rospy.get_param("/vision_manager/vision_module_path", '')
	if fileName == '':
		module = KinematicModule()
	else:
		print "get module"
		lobe_module = load_module(fileName)
		assert hasattr( lobe_module, 'kinematic_module' )
		module = lobe_module.kinematic_module

	node = KinematicBrain()
	try:
		node.initial(module)
		node.run()
	except rospy.ROSInterruptException as e:
	# except Exception as e:
		print "\n\n\n"
		print e
		print "\n\n\n"
	finally:
		node.end()
