#!/usr/bin/env python
##
import cv2
import numpy as np 

import configobj

import rospy
import roslib

from sensor_msgs.msg import CompressedImage, Image

from newbie_hanuman.msg import ColorDef
from newbie_hanuman.srv import RequestColorDef, RequestColorDefResponse
from utility.imageToMessage import cvtCVImageToImageMessage, cvtImageMessageToCVImage

from visionManager.cranialNerveII import CranialNerveII, DEFAULT_COLORCONFIG

class CranialNerveIIForSimulation( CranialNerveII ):

	def __init__( self ):

		super( CranialNerveIIForSimulation, self ).__init__()

		self.__image = None

	def rosInitPublisherAndService( self ):
		super( CranialNerveIIForSimulation, self ).rosInitPublisherAndService()

		self.rosInitSubscriber( "/hanuman/camera1/image_raw",
								Image, self.subscriberCallback )

	def subscriberCallback( self, msg ):
		self.__image = cvtImageMessageToCVImage( msg )

	def getInputImage( self ):

		if self.__image is None:
			return False, self.__image

		else:
			return True, self.__image[:,:,::-1]

def main( ):
	try:
		cranailNerve2 = CranialNerveIIForSimulation()
		cranailNerve2.run()
	except rospy.ROSInterruptException:
		cranailNerve2.end()