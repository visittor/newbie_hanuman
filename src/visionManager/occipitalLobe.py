#!/usr/bin/env python
import numpy as np 
import cv2

import rospy
import roslib
from sensor_msgs.msg import CompressedImage, JointState, Image
from std_msgs.msg import Header

from newbie_hanuman.srv import RequestColorDef

from cell.nodeCell import NodeBase
from visionModule import VisionModule
from utility.utility import load_module
from utility.imageToMessage import cvtImageMessageToCVImage, cvtCVImageToImageMessage

import inspect
import os
import imp
import sys

class OccipitalLobeException( Exception ):
	pass

class OccipitalLobe(NodeBase):

	def __init__(self):
		super(OccipitalLobe, self).__init__("occipital_lobe")
		self.__subscribeCallback = lambda x:x
		self.__serviceCallback = None
		self.__publisherMessageType = None
		self.__visualizeFunc = lambda x,y:x
		self.__img = None
		self.__pantiltMsg = None
		self.__subPantilt = False

		self.__vis = self.getParam( '/visualize', 1 )

	def set_subscribeCallback(self, func):
		self.__subscribeCallback = func

	def set_publisherMessageType(self, msgType):
		self.__publisherMessageType = msgType

	def set_visualizeFunction(self, func):
		if len(inspect.getargspec(func)[0]) != 3:
			raise OccipitalLobeException( "Subscibe call back must have three argument." )
		self.__visualizeFunc = func

	def initial(self, module):
		config = self.getRobotConfig( )

		module.setConfig( config )
		self.set_subscribeCallback(module.ImageProcessingFunction)
		self.set_publisherMessageType(module.objectsMsgType)
		self.set_visualizeFunction(module.visualizeFunction)
		self.__subPantilt = module.subscribePantiltTopic
		self.rosInitNode()
		self.rosInitPublisher( 	"/vision_manager/occipital_lobe_topic",
								self.__publisherMessageType,
								queue_size = 1)
		self.__rospub = rospy.Publisher("/vision_manager/occipital_debug_topic",
								Image,
								queue_size = 1)
		self.rosInitSubscriber("/vision_manager/cranial_nerve_ii_topic",
								 Image, 
								 self.__receiveImage,
								 queue_size = 1)

		rospy.wait_for_service( "/vision_manager/request_color_def" )
		serviceProxy = rospy.ServiceProxy( "/vision_manager/request_color_def", RequestColorDef )
		colorConfig = serviceProxy()
		module._setColorConfig( colorConfig )

		if self.__subPantilt:
			rospy.Subscriber("/spinalcord/sternocleidomastoid_position", 
							 JointState, 
							 self.__pantiltCallback, 
							 queue_size=1)

		self.setFrequencyFromParam("/vision_manager/cranial_nerve_ii_frquency")

	def __receiveImage(self, msg):
		# h, w, c = eval( msg.format )
		# npArray = np.fromstring(msg.data, dtype=np.uint8).reshape( h, w, c )
		
		self.__img = cvtImageMessageToCVImage( msg )
		self.__header = msg.header

	def __pantiltCallback(self, msg):
		self.__pantiltMsg = msg

	def __createCompressedImage(self, img, stamp):
		msg = CompressedImage()
		msg.header.stamp = stamp
		msg.format = "jpeg"
		msg.data = np.array(cv2.imencode(".jpg", img)[1]).tostring()
		return msg

	def publish(self, imgMsg, objectMsg):
		super(OccipitalLobe, self).publish(objectMsg)
		self.__rospub.publish(imgMsg)

	def run(self):
		rospy.loginfo("Start OccipitalLobe node.")
		while not rospy.is_shutdown():
			# ret, img = self.__cap.read()
			if self.__img is None:
				self.sleep()
				continue
			img = self.__img.copy()
			stamp = rospy.Time.now()
			header = self.__header

			if self.__subPantilt:
				objectMsg = self.__subscribeCallback(img, 
													 header,
													 self.__pantiltMsg)
			else:
				objectMsg = self.__subscribeCallback(img, 
													 header)

			if self.__vis:
				self.__visualizeFunc(img, objectMsg)

			imgMsg = cvtCVImageToImageMessage( img )
			imgMsg.header.stamp = stamp
			self.publish(imgMsg, objectMsg)
			self.sleep()

	def end(self):
		# self.__cap.release()
		rospy.loginfo("Close OccipitalLobe node.")

def main():
	fileName = rospy.get_param("/vision_manager/vision_module_path", '')
	if fileName == '':
		module = VisionModule()
	else:
		print "get module"
		lobe_module = load_module(fileName)
		assert hasattr( lobe_module, 'vision_module' )
		module = lobe_module.vision_module

	node = OccipitalLobe()
	try:
		node.initial(module)
		node.run()
	except rospy.ROSInterruptException as e:
		rospy.logwarn(str(e))
	finally:
		node.end()