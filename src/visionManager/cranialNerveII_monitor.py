import cv2
import numpy as np 

import rospy
import roslib
from message_filters import ApproximateTimeSynchronizer, Subscriber,TimeSynchronizer
from sensor_msgs.msg import CompressedImage, Image

from cell.nodeCell import NodeBase
from utility.imageToMessage import cvtImageMessageToCVImage
# from occipitalLobe import 	

import os
import sys
import imp

class cranialNerveII_monitor(NodeBase):
	def __init__(self):
		super(cranialNerveII_monitor, self).__init__("cranial_nerve_ii_monitor")
		self.__recieveImg = np.zeros((640,480,3), dtype=np.uint8)
		self.__visualizeFunction = lambda x,y:None
		self.__cranialAlive = False
		self.__occipitalAlive = False

	def initial(self,):
		self.__rosInitNode()
		self.rosInitSubscriber("/vision_manager/occipital_debug_topic", Image, self.__callback, buff_size=8*3*480*640)

	def __rosInitNode(self):
		self.rosInitNode()
		self.setFrequencyFromParam('/vision_manager/cranial_nerve_ii_frquency')

	def __callback(self, imgMsg):
		## Read buffer from data and convert it to image.
		self.__recieveImg = cvtImageMessageToCVImage( imgMsg )
		
		## get time stamp.
		timeStamp = imgMsg.header.stamp
		fontFace =  cv2.FONT_HERSHEY_PLAIN
		
	def run(self):
		while not rospy.is_shutdown():
			cv2.imshow("img", self.__recieveImg)
			cv2.waitKey(1)
			self.sleep()
		self.end()

	def end(self):
		cv2.destroyAllWindows()

def main():
	try:
		monitor = cranialNerveII_monitor()
		monitor.initial()
		monitor.run()
	except rospy.ROSInterruptException:
		pass
	finally:
		monitor.end()