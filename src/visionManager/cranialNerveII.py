#!/usr/bin/env python
##
import cv2
import numpy as np 

import configobj

import rospy
import roslib

from sensor_msgs.msg import CompressedImage, Image

from newbie_hanuman.msg import ColorDef
from newbie_hanuman.srv import RequestColorDef, RequestColorDefResponse
from utility.imageToMessage import cvtCVImageToImageMessage

from cell.nodeCell import NodeBase

DEFAULT_COLORCONFIG = [
						{ 	'ID'		:	1,
							'Name'		:	'green',
							'H_max'		:	86,
							'S_max'		:	255,
							'V_max'		:	255,
							'H_min'		:	13,
							'S_min'		:	72,
							'V_min'		:	0,
							'RenderColor_RGB': '( 0, 128, 0 )',
							'MinArea_pixels' : 4 },
						{ 	'ID'		:	2,
							'Name'		:	'black',
							'H_max'		:	255,
							'S_max'		:	50,
							'V_max'		:	50,
							'H_min'		:	0,
							'S_min'		:	0,
							'V_min'		:	0,
							'RenderColor_RGB': '( 0, 0, 0 )',
							'MinArea_pixels' : 4 },
						{ 	'ID'		:	3,
							'Name'		:	'orange',
							'H_max'		:	255,
							'S_max'		:	255,
							'V_max'		:	255,
							'H_min'		:	255,
							'S_min'		:	255,
							'V_min'		:	255,
							'RenderColor_RGB': '( 255, 128, 0 )',
							'MinArea_pixels' : 4 },
						{ 	'ID'		:	4,
							'Name'		:	'blue',
							'H_max'		:	255,
							'S_max'		:	255,
							'V_max'		:	255,
							'H_min'		:	255,
							'S_min'		:	255,
							'V_min'		:	255,
							'RenderColor_RGB': '( 0, 0, 255 )',
							'MinArea_pixels' : 4 },
						{ 	'ID'		:	5,
							'Name'		:	'yellow',
							'H_max'		:	255,
							'S_max'		:	255,
							'V_max'		:	255,
							'H_min'		:	255,
							'S_min'		:	255,
							'V_min'		:	255,
							'RenderColor_RGB': '( 255, 255, 0 )',
							'MinArea_pixels' : 4 },
						{ 	'ID'		:	6,
							'Name'		:	'magenta',
							'H_max'		:	190,
							'S_max'		:	255,
							'V_max'		:	255,
							'H_min'		:	165,
							'S_min'		:	89,
							'V_min'		:	0,
							'RenderColor_RGB': '( 128, 0, 255 )',
							'MinArea_pixels' : 4 },
						{ 	'ID'		:	7,
							'Name'		:	'cyan',
							'H_max'		:	100,
							'S_max'		:	255,
							'V_max'		:	255,
							'H_min'		:	88,
							'S_min'		:	90,
							'V_min'		:	0,
							'RenderColor_RGB': '( 255, 255, 0 )',
							'MinArea_pixels' : 4 },
						{ 	'ID'		:	8,
							'Name'		:	'white',
							'H_max'		:	255,
							'S_max'		:	127,
							'V_max'		:	255,
							'H_min'		:	0,
							'S_min'		:	0,
							'V_min'		:	50,
							'RenderColor_RGB': '( 255, 255, 255 )',
							'MinArea_pixels' : 4 },
]
								


class CranialNerveII(NodeBase):
	"""docstring for CranialNerveII"""
	def __init__(self):
		# self.ros_pub = rospy.Publisher("/vision_manager/cranial_nerve_ii_topic", CompressedImage, queue_size = 1)
		# rospy.init_node("cranial_nerve_ii", anonymous = True)
		# cameraID = rospy.get_param('/vision_manager/cameraID', 0)
		# frequency = rospy.get_param('/master_frequency', 60)
		# frequency = rospy.get_param('/vision_manager/cranial_nerve_ii_frquency', frequency)
		# self.__rate = rospy.Rate(frequency)
		super(CranialNerveII, self).__init__("cranial_nerve_ii")
		# self.setFrequencyFromParam("/vision_manager/cranial_nerve_ii_frquency")

		cameraID = self.getParam( '/vision_manager/cameraID', 0 )
		self.__isDoSegment = self.getParam( '/vision_manager/autoSegment', 0 )

		colorConfigFile = self.getParam( '/robot_config', '' )

		if colorConfigFile == '':
			colorConfig = sorted( DEFAULT_COLORCONFIG, key = lambda x : x['ID'] )

		else:
			config = configobj.ConfigObj( colorConfigFile )

			if config.has_key( "ColorDefinitions" ):
				colorConfig = config["ColorDefinitions"].values()
			else:
				colorConfig = sorted( DEFAULT_COLORCONFIG, key = lambda x : x['ID'] )
				rospy.logwarn( 'Parameter "/vision_manager/colorConfig" should be location of file colorConfig file' )

			if config.has_key( "CameraParameters" ):
				if config["CameraParameters"].has_key( "scale" ):
					scale = float( config["CameraParameters"]["scale"] )
					imgW = int( config["CameraParameters"]["imgW"] )
					imgH = int( config["CameraParameters"]["imgH"] )

				else:
					scale = 1.0
					imgW = 640
					imgH = 480

			else:
				scale = 1.0
				imgW = 640
				imgH = 480

		self.__colorConfig = self.createColorDefMsg( colorConfig )
		if cameraID == -1:
			self.__cap = None
		else:
			self.__cap = cv2.VideoCapture(cameraID)
			if colorConfigFile != '':
				print scale
				self.__cap.set( cv2.CAP_PROP_FRAME_WIDTH, int(imgW * scale) )
				self.__cap.set( cv2.CAP_PROP_FRAME_HEIGHT, int(imgH * scale) )

		self.rosInitNode()
		self.rosInitPublisherAndService()
		self.setFrequency(70)

	def rosInitPublisherAndService( self ):
		self.rosInitPublisher("/vision_manager/cranial_nerve_ii_topic", Image, queue_size = 1)
		self.rosInitService( "/vision_manager/request_color_def", RequestColorDef, self.serviceHandle )
		
	def getInputImage( self ):
		return self.__cap.read()

	def createColorDefMsg( self, colorConfigList ):
		colorDefList = []
		for colorConfig in colorConfigList:
			colorDef = ColorDef( 
								ID = int(colorConfig["ID"]),
								Name = colorConfig["Name"],
								RenderColor_RGB = colorConfig["RenderColor_RGB"],
								MinArea_pixels = int(colorConfig["MinArea_pixels"]),
								H_max = int(colorConfig["H_max"]),
								H_min = int(colorConfig["H_min"]),
								S_max = int(colorConfig["S_max"]),
								S_min = int(colorConfig["S_min"]),
								V_max = int(colorConfig["V_max"]),
								V_min = int(colorConfig["V_min"]),
			 					)
			
			colorDefList.append( colorDef )

		return colorDefList

	def serviceHandle( self, req ):
		return RequestColorDefResponse( colorDefList = self.__colorConfig )

	def publish(self, img):
		# msg = CompressedImage()
		# msg.header.stamp = rospy.Time.now()
		# msg.format = "({},{},{})".format( *img.shape )
		# # msg.data = np.array(cv2.imencode(".jpg", img)[1]).tostring()
		# msg.data = img.tostring()
		msg = cvtCVImageToImageMessage( img )
		super(CranialNerveII, self).publish(msg)
		# print "published"

	def segmentation( self, img ):

		gray = cv2.cvtColor( img, cv2.COLOR_BGR2GRAY )
		hsv = cv2.cvtColor( img, cv2.COLOR_BGR2HSV )
		hsv = cv2.GaussianBlur( hsv, (5,5), 0 )

		marker = np.zeros( (img.shape[0], img.shape[1]), dtype = np.int32 )
		for color in self.__colorConfig:
			lower = np.array( [ color.H_min, color.S_min, color.V_min ] )
			upper = np.array( [ color.H_max, color.S_max, color.V_max ] )
			
			colorLoc = cv2.inRange( hsv, lower, upper )
 			marker[ colorLoc != 0 ] = color.ID

 		colorSeg = cv2.watershed( hsv, marker.copy() )

 		returnImg = np.zeros( ( hsv.shape[0], hsv.shape[1], 3 ), dtype=np.uint8 )
 		returnImg[:,:,0] = gray
 		returnImg[:,:,1] = colorSeg

 		return returnImg

	def run(self):
		rospy.loginfo("Start cranial_nerve_ii node.")
		while not rospy.is_shutdown():
			if self.__cap is None:
				self.sleep( )
				continue
				
			ret, img = self.getInputImage( )

			if self.__isDoSegment and ret:
				img = self.segmentation( img )

			if ret:
				self.publish(img)
			
			self.sleep()
		rospy.loginfo("Close cranial_nerve_ii node.")

	def end(self):
		self.__cap.release()

def main():
	try:
		cranialNerve2 = CranialNerveII()
		cranialNerve2.run()
	except rospy.ROSInterruptException:
		cranialNerve2.end()