#!/usr/bin/env python
from HumanoidBase import *
from Queue import PriorityQueue, Queue

import time
import math

SpecialPostureTable =	{
				'ReadyToMove'		:	'2', 
				'StandStill'		:	'n',
				'LeftKick'			:	'j', 
				'RightKick'			:	'k', 
				'Standup'			:	'h', 
				'TouchBall'			:	'y',
				'LeftTouch'			:	'z', 
				'RightTouch'		:	'x',
				'LeftSideKick'		:	'9', 
				'RightSideKick'		:	'0',
				'Happy'				:	'a', 
				'Upset'				:	'b',
				'standToSit'		:	'3',
				'sitToStand'		:	'4', 
				
				'ContinuousWalk' 	:	'o',
				'OneStepWalk'		:	'p',
				
				#
				# Goalie commands (do not use with ordinary robot)
				#
				'GoalieSaveRight'	:	'f',
				'GoalieSaveLeft'	:	'g',
				
				#
				# The following commands are temporary used, 
				#	till the omni-walk firmware can handle 
				#	the side direction movement properly
				#
				'OldStyleLeftSlide'			:	'd', 
				'OldStyleRightSlide'		:	'c', 
				'OldStyleLeftCurveSlide'	:	's', 
				'OldStyleRightCurveSlide'	:	'r',
				
				#
				# we use this when vx & vy == 0, but has some v_omega
				#
				'OldStyleTurnLeft'			:	'p',
				'OldStyleTurnRight'			:	'q',
}

## Change character in dictionary to number.
for n,v in SpecialPostureTable.items():
	SpecialPostureTable[n] = ord(v)

class LowLevelInterfaceException(Exception):
	pass 

class CommandQueue(object):

	def __init__(self, timeTolerance = None):
		self.__commandQueue = Queue()
		self.__timeTolerance = timeTolerance

		self.__finalCommand = None
		self.__finalCommandTimeStamp = None

	def add_command(self, timeStamp, command, ignorable = True):
		'''
		Put command to queue.
		arguments:
			timeStamp 	:	( float )time stamp for this command.
			command 	: 	( LocomotionCommand or LocomotionSpecialCommand )
			optional:
				ignorable 	:	( bool ) Default : True, If true this command will not
								be ignored.
		'''
		## If queue is full, ignore a oldest command
## TODO : chattarin : Get a better to handle this. If the oldest command is not ignorable
##		this function will ignore it anyway. This incident is never happen though.
		if self.__commandQueue.full():
			self.__commandQueue.get()
			self.__commandQueue.task_done()

		## We will not put a same command twice.
		if command == self.__finalCommand:
			return

		## Put command to queue
		self.__commandQueue.put( (timeStamp, command, ignorable) )
		self.__finalCommand = command
		self.__finalCommandTimeStamp = timeStamp

	def getFinalCommandInfo(self):
		'''
		Get the latest command.
		'''
		return self.__finalCommand, self.__finalCommandTimeStamp

	def get_command(self, currentTime):
		'''
		Get the oldest command from queue.
		'''
		command = None
		if currentTime is None:
			currentTime = time.time()

		## Get a command from queue. If command is too old and ignarable, ignore it and 
		## get a new command from queue.
		while 1==1:
			if self.__commandQueue.empty():
				self.__finalCommand = None
				self.__finalCommandTimeStamp = None
				break
			command = self.__commandQueue.get()
			if 	self.__timeTolerance is None or currentTime - command[0] <= self.__timeTolerance or not command[2]:
				break
			command = None
		return command

	def task_done(self):
		self.__commandQueue.task_done()

	def setTimeTolerance(self, value):
		if value > 0:
			self.__timeTolerance = value

class LocomotionCommand(object):

	def __init__(self, velX, velY, omgZ, oneStep=False):
		self.velX = velX
		self.velY = velY
		self.omgZ = omgZ

		self.oneStep = oneStep

	def tupleForm(sefl):
		return (self.velX, self.velY, self.omgZ)

	def __hash__(self):
		return hash(self.tupleForm())

	def __eq__(self, othr):
		try:
			return (self.velX==othr.velX and 
					self.velY==othr.velY and 
					self.omgZ==othr.omgZ)
		except AttributeError:
			return False

	def __ne__(self, othr):
		return not self.__eq__(othr)

	def __str__(self):
		return str(self.velX)+","+str(self.velY)+","+str(self.omgZ)

class LocomotionSpecialCommand(object):

	def __init__(self, command):
		assert type(command) == str
		self.__command = command

	def stringForm(self):
		return self.__command

	def __eq__(self, othr):
		try:
			return self.__command == othr
		except TypeError:
			return False

	def __ne__(self, othr):
		return not self.__eq__(othr)

	def __str__(self):
		return self.__command

class HanumanStatus(object):

	def __init__(self, statusPackage):

		self.firmware 		= 		statusPackage["REG_FIRMWARE_VERSION_L"]
		self.firmware 		+=		statusPackage["REG_FIRMWARE_VERSION_H"]<<8
		self.lococommand 	=		statusPackage["REG_LOCOMOTION_COMMAND"]
		self.locostatus 	= 		statusPackage["REG_LOCOMOTION_STATUS_L"]
		self.locostatus 	+=		statusPackage["REG_LOCOMOTION_STATUS_H"]<<8

		self.backFallDown 	=		HanumanInterface.is_backFallDown(
												self.locostatus )

		self.frontFallDown 	=		HanumanInterface.is_frontFallDown(
										self.locostatus )
		
		# self.compass 		= 		statusPackage["REG_COMPASS_L"]
		# self.compass 		+= 		statusPackage["REG_COMPASS_H"]<<8
		# self.gyroX 			= 		statusPackage["REG_GYRO_ROT_X_L"]
		# self.gyroX 			+= 		statusPackage["REG_GYRO_ROT_X_H"]<<8
		# self.gyroY 			= 		statusPackage["REG_GYRO_ROT_Y_L"]
		# self.gyroY 			+= 		statusPackage["REG_GYRO_ROT_Y_H"]<<8
		# self.gyroZ 			= 		statusPackage["REG_GYRO_ROT_Z_L"]
		# self.gyroZ 			+= 		statusPackage["REG_GYRO_ROT_Z_H"]<<8
		self.linearVelX 	= 		statusPackage["REG_ROBOT_LINEAR_VELOCITY_X"]
		self.linearVelY 	= 		statusPackage["REG_ROBOT_LINEAR_VELOCITY_Y"]
		self.angularVel 	= 		statusPackage["REG_ROBOT_ANGULAR_VELOCITY"]
		# self.magnetoX 		= 		statusPackage["REG_MAGNETO_X_L"]
		# self.magnetoX 		+= 		statusPackage["REG_MAGNETO_X_H"]<<8
		# self.magnetoY 		= 		statusPackage["REG_MAGNETO_Y_L"]
		# self.magnetoY 		+= 		statusPackage["REG_MAGNETO_Y_H"]<<8
		# self.magnetoZ 		= 		statusPackage["REG_MAGNETO_Z_L"]
		# self.magnetoZ 		+= 		statusPackage["REG_MAGNETO_Z_H"]<<9

		self.pitch			=		statusPackage["WORLD_PITCH_ANGLE_L"]
		self.pitch 			+=		statusPackage["WORLD_PITCH_ANGLE_H"]<<8
		self.pitch 			-=		127

		self.roll			=		statusPackage["WORLD_ROLL_ANGLE_L"]
		self.roll 			+=		statusPackage["WORLD_ROLL_ANGLE_H"]<<8
		self.roll 			-=		127

	@property
	def statusDict( self ):
		statusDict = {}
		statusDict["firmware"] 		= 	self.firmware
		statusDict['lococommand'] 	= 	self.lococommand
		statusDict['locostatus'] 	= 	self.locostatus
		statusDict['backFallDown'] 	= 	self.backFallDown
		statusDict['frontFallDown'] = 	self.frontFallDown
		# statusDict['compass'] 		= 	self.compass
		# statusDict['gyroX'] 		= 	self.gyroX
		# statusDict['gyroY'] 		= 	self.gyroY
		# statusDict['gyroZ'] 		= 	self.gyroZ
		statusDict['linearVelX'] 	= 	self.linearVelX
		statusDict['linearVelY'] 	= 	self.linearVelY
		statusDict['angularVel'] 	= 	self.angularVel
		# statusDict['magnetoX'] 		= 	self.magnetoX
		# statusDict['magnetoY'] 		= 	self.magnetoY
		# statusDict['magnetoZ'] 		= 	self.magnetoZ
		statusDict['pitch'] 		= 	self.pitch
		statusDict['roll'] 			= 	self.roll

		return statusDict

class HanumanInterface(object):

	def __init__(self, serial,
			XWorldTranMax, XWorldTranMin, YWorldTranMax, YWorldTranMin, 
			ZWorldAnglMax, ZWorldAnglMin, XRegisTranMax, XRegisTranMin, 
			YRegisTranMax, YRegisTranMin, ZRegisAnglMax, ZRegisAnglMin,
			timeout = None, timeTolerance = None, transitionTime = 1.0):

		self.spinal_cord = HanumanProtocol(serial)

		## maximum velocity / angular vel in real world. ( m ) / radian
		## maximum velocity / angular vel value of low level register.
		self.__XWorldTranMax = max(XWorldTranMax, 0)
		self.__XWorldTranMin = min(XWorldTranMin, 0)
		self.__XRegisTranMax = max(XRegisTranMax, 128)
		self.__XRegisTranMin = min(XRegisTranMin, 126)

		self.__YWorldTranMax = max(YWorldTranMax, 0)
		self.__YWorldTranMin = min(YWorldTranMin, 0)
		self.__YRegisTranMax = max(YRegisTranMax, 128)
		self.__YRegisTranMin = min(YRegisTranMin, 126)

		self.__ZWorldAnglMax = max(ZWorldAnglMax, 0)
		self.__ZWorldAnglMin = min(ZWorldAnglMin, 0)
		self.__ZRegisAnglMax = max(ZRegisAnglMax, 128)
		self.__ZRegisAnglMin = min(ZRegisAnglMin, 126)

		## time out for serial. if low level do not response in time it's considered
		## low level not response
		self.__timeout = timeout

		## life time of command. if command exist longer than time tolerance
		## and it is ignorable, it will be deleted from queue.
		self.__timeTolerance = timeTolerance

		## Queue for querying a command
		self.__commandQueue = CommandQueue(self.__timeTolerance)

		## next/previouse command to do
		self.__nextCommand = LocomotionSpecialCommand("StandStill")
		self.__prevCommand = LocomotionSpecialCommand("StandStill")

		# self.__commandQueue.add_command(time.time(), self.__nextCommand)

		## time we start/stop a new command, use for integrate distance
		self.__startCommandTime = time.time()
		self.__stopCommandTime = time.time()

		## command for stop robot
		self.__stopCommand = self.convertCommandVelocityToRegister(LocomotionCommand(0.0,
																					0.0,
																					0.0 ))

		## Some command may need some transition time, i.e. stand up, kick etc.
		self.__transitionTime = transitionTime
		self.__waitTransition = False

		self._roll_offset = 0.0
		self._pitch_offset = 0.0

	@staticmethod
	def convertLocomotionRegToWorld(regis, regisMax, regisMin, worldMax, worldMin):
		'''
		Convert register value to real velocity in real world.
		'''
		regis = float( regis )
		regisMax = float( regisMax )
		regisMin = float( regisMin )
		worldMax = float( worldMax )
		worldMin = float( worldMin )

		if regis > 127:
			val =   worldMax * ( regis - 127 ) / ( regisMax - 127 ) 

		elif regis < 127:
			val = worldMin * ( 127 - regis ) / ( 127 - regisMin )

		else:
			val = 0
 
		return val

	@staticmethod
	def convertLocomotionWorldToReg(scale, regisMax, regisMin, worldMax, worldMin):
		'''
		Convert velocity in real world to register value
		'''
		regisMax = float( regisMax )
		regisMin = float( regisMin )
		worldMax = float( worldMax )
		worldMin = float( worldMin ) 

		scale = float(max( min( scale, 1 ), -1 ))

		if scale > 0.0:
			regis = (scale * ( regisMax - 127 )) + 127

		elif scale < 0.0:
			regis = (scale * ( 127 - regisMin )) + 127

		else:
			regis = 127

		return int( regis )

	def convertCommandVelocityToRegister(self, lococommand):
		'''
		Convert velocity x, y, and z to register value
		'''
		reg_x = self.convertLocomotionWorldToReg(lococommand.velX,
												 self.__XRegisTranMax,
												 self.__XRegisTranMin,
												 self.__XWorldTranMax,
												 self.__XWorldTranMin)
		reg_y = self.convertLocomotionWorldToReg(lococommand.velY,
												 self.__YRegisTranMax,
												 self.__YRegisTranMin,
												 self.__YWorldTranMax,
												 self.__YWorldTranMin)
		reg_z = self.convertLocomotionWorldToReg(lococommand.omgZ, 
												 self.__ZRegisAnglMax, 
												 self.__ZRegisAnglMin,
												 self.__ZWorldAnglMax,
												 self.__ZWorldAnglMin)

		clip_x, clip_y, clip_z = self._clipVelocity(reg_x, reg_y, reg_z)

		return LocomotionCommand(clip_x, clip_y, clip_z, oneStep = lococommand.oneStep)

	def _clipVelocity( self, reg_x, reg_y, reg_z ):

		## invert matrix 2x2
		## A = 	|a 	b| 
		## 		|c 	d|
		## inv A = 1/( ad-bc )	|d 		-b|
		##						|-c 	 a|

		if reg_x == 127 and reg_y == 127:
			return reg_x, reg_y, reg_z

		## 1st Quadrant
		if reg_x >= 127 and reg_y >= 127:

			a = 127 - reg_x
			b = 127 - self.__XRegisTranMax
			c = 127 - reg_y
			d = self.__YRegisTranMax - 127

			t = d*(127 - reg_x) - b*(self.__YRegisTranMax - reg_y)
			t /= ( a*d - b*c )

			intersectX = t*127 + ( 1 - t )*reg_x
			intersectY = t*127 + ( 1 - t )*reg_y

		## 2nd Quadrant
		elif reg_x <= 127 and reg_y >= 127:

			a = 127 - reg_x
			b = self.__XRegisTranMin - 127
			c = 127 - reg_y
			d = 127 - self.__YRegisTranMax

			t = d*(self.__XRegisTranMin - reg_x) - b*(127 - reg_y)
			t /= ( a*d - b*c )

			intersectX = t*127 + ( 1 - t )*reg_x
			intersectY = t*127 + ( 1 - t )*reg_y

		## 3rd Quadrant
		elif reg_x <= 127 and reg_y <= 127:

			a = 127 - reg_x
			b = 127 - self.__XRegisTranMin
			c = 127 - reg_y
			d = self.__YRegisTranMin - 127

			t = d*(127 - reg_x) - b*(self.__YRegisTranMin - reg_y)
			t /= ( a*d - b*c )

			intersectX = t*127 + ( 1 - t )*reg_x
			intersectY = t*127 + ( 1 - t )*reg_y

		## 4th Quadrant
		else:

			a = 127 - reg_x
			b = self.__XRegisTranMax - 127
			c = 127 - reg_y
			d = 127 - self.__YRegisTranMin

			t = d*(self.__XRegisTranMax - reg_x) - b*(127 - reg_y)
			t /= ( a*d - b*c )

			intersectX = t*127 + ( 1 - t )*reg_x
			intersectY = t*127 + ( 1 - t )*reg_y

		r_max = (127 - intersectX)**2 + (127 - intersectY)**2
		r = (127 - reg_x)**2 + (127 - reg_y)**2

		if r < r_max:
			return reg_x, reg_y, reg_z

		return intersectX, intersectY, reg_z

	def convertCommandRegisterToVelocity(self, lococommand):
		'''
		Convert register value to register value velocity x, y, and z 
		'''
		vel_x = self.convertLocomotionRegToWorld(lococommand.velX,
												 self.__XRegisTranMax,
												 self.__XRegisTranMin,
												 self.__XWorldTranMax,
												 self.__XWorldTranMin)
		vel_y = self.convertLocomotionRegToWorld(lococommand.velY,
												 self.__YRegisTranMax,
												 self.__YRegisTranMin,
												 self.__YWorldTranMax,
												 self.__YWorldTranMin)
		vel_z = self.convertLocomotionRegToWorld(lococommand.omgZ,
												 self.__ZRegisAnglMax,
												 self.__ZRegisAnglMin,
												 self.__ZWorldAnglMax,
												 self.__ZWorldAnglMin)
		return LocomotionCommand(vel_x, vel_y, vel_z, oneStep = lococommand.oneStep)

	def setCommand(self, command, timeStamp = None, ignorable = True):
		'''
		Add command to commandQueue
		'''
		## Find and actual command ( convert register value to real world value )
		if isinstance(command, LocomotionCommand):
			actualCommand = self.convertCommandVelocityToRegister( command )
		elif isinstance(command, LocomotionSpecialCommand):
			actualCommand = command
		else:
			raise LowLevelInterfaceException("Command is neither LocomotionSpecialCommand nor LocomotionSpecialCommand.")

		## Consider from final command, Do we need to stop before do this command
		finalCommand = self.__commandQueue.getFinalCommandInfo()
		self.__filterCommand(actualCommand, finalCommand[0], timeStamp)

		if timeStamp is None:
			self.__commandQueue.add_command(time.time(), 
											actualCommand, 
											ignorable = ignorable)
		else:
			# print "Should add command here"
			self.__commandQueue.add_command(timeStamp, 
											actualCommand,
											ignorable = ignorable)

	def __filterCommand(self, command, finalCommand, timeStamp):
		if command == finalCommand:
			return

		if self.__shouldStopBeforeDoCommand(command, finalCommand):
			if timeStamp is None:
				self.__commandQueue.add_command(time.time(),
												self.__stopCommand,
												ignorable = False)
			else:
				self.__commandQueue.add_command(timeStamp,
												self.__stopCommand,
												ignorable = False)
			return

	def __shouldStopBeforeDoCommand(self, command, finalCommand):
		if isinstance(command, LocomotionSpecialCommand):
			return True

		elif isinstance(command, LocomotionCommand) and isinstance(finalCommand, LocomotionCommand):
			# Change direction in X direction.
			if command.velX * finalCommand.velX < 0:
				return True
			# Change direction in Y direction.
			elif command.velY * finalCommand.velY < 0:
				return True
			# Change rotation direction.
			elif command.omgZ * finalCommand.omgZ < 0:
				return True

			if command.oneStep:
				return True

		return False

	def forceStop(self, currentTime = None):
		'''
		Force robot to stop moving
		'''
		self.__nextCommand = self.__stopCommand
		self.__executeCommand()

	def forceStandup(self, currentTime = None):
		'''
		Force robot to standup
		'''
		self.__nextCommand = LocomotionSpecialCommand("Standup")
		self.__executeCommand()

	def doCurrentCommand(self, currentTime = None):
		'''
		Get a command from queue and  execute it.
		'''
		if currentTime is None:
			currentTime = time.time()

		## if Later command need to a transition time, we wait.
		if self.__waitTransition and time.time() - self.__startCommandTime <= self.__transitionTime:
			return
		self.__waitTransition = False

		## Get command from command queue and execute it
		command = self.__commandQueue.get_command(currentTime)
		self.__nextCommand = command[1] if command is not None else None
		
		isSuccess = self.__executeCommand()
		
		if isSuccess:
			rospy.loginfo("[MotorCotex] Do "+str(self.__nextCommand))

		if self.__nextCommand is not None:
			self.__commandQueue.task_done()

	def __executeCommand(self):
		isSuccess = False
		## Execute command, if it is a special command it will need transitin time.
		if self.__nextCommand is None:
			isSuccess = False

		elif isinstance(self.__nextCommand, LocomotionCommand):
			isSuccess = self.__executeOmniwalk(self.__nextCommand)

		elif isinstance(self.__nextCommand, LocomotionSpecialCommand):
			isSuccess = self.__executeSpecial(self.__nextCommand)
			self.__waitTransition = isSuccess

		else:
			raise LowLevelInterfaceException("Command is neither LocomotionSpecialCommand nor LocomotionSpecialCommand.")


		## If this command successfully execute
		if isSuccess:
			self.__prevCommand = self.__nextCommand
			self.__startCommandTime = time.time()

		return isSuccess

	def __executeOmniwalk(self, command):
		## Send serial to robot, set a velocity first
		response = self.spinal_cord.set_locomotion_velocity(command.velX, 
															command.velY, 
															command.omgZ, 
													timeout = self.__timeout)
		## response is None mean low level is not response
		if response is None:
			return False

		time.sleep( 0.05 )

		if command.oneStep:
			## This command will tell robot to do just 1 step.
			response = self.spinal_cord.set_locomotion_command(SpecialPostureTable['OneStepWalk'], timeout = self.__timeout)
			self.__waitTransition = True if response is not None else False

		else:
			## Send serial to robot to tell it to change velocity
			response = self.spinal_cord.set_locomotion_command(SpecialPostureTable["ContinuousWalk"], timeout = self.__timeout)
		
		return True if response is not None else False

	def __executeSpecial(self, command):
		if not SpecialPostureTable.has_key(command.stringForm()):
			raise LowLevelInterfaceException("Error excecute command \""+command.stringForm()+"\". No such a command.")
		
		## Send serial to low level
		response = self.spinal_cord.set_locomotion_command(
									SpecialPostureTable[command.stringForm()],
									timeout = self.__timeout
									)
		return True if response is not None else False

	@staticmethod
	def is_backFallDown( robotStatus ):
		return TiltStatus_FallDown_FaceUp == (robotStatus&TiltStatusMask)

	@staticmethod
	def is_frontFallDown( robotStatus ):
		return TiltStatus_FallDown_FaceDown == (robotStatus&TiltStatusMask)

	def is_robotStanding(self):
		# print "ee"
		self.spinal_cord.read_AllLowLevelData(timeout = self.__timeout)
		# print "dd"
		robotStatus = self.spinal_cord.get_locomotionStatus()
		# print robotStatus
		if robotStatus is None:
			return True

		if self.is_backFallDown( robotStatus ):
			return False

		elif self.is_frontFallDown( robotStatus ):
			return False

		return True

	def getHanumanStatus(self):
		self.spinal_cord.read_AllLowLevelData(timeout = self.__timeout)
		statusPackage = self.spinal_cord.get_statusPackage()
		if statusPackage is None:
			return
		hanumanStatus = HanumanStatus(statusPackage)

		## Change register value to velocity.
		loco = self.convertCommandRegisterToVelocity( LocomotionCommand(
											hanumanStatus.linearVelX,
 											hanumanStatus.linearVelY,
 											hanumanStatus.angularVel) )

		hanumanStatus.linearVelX = loco.velX
		hanumanStatus.linearVelY = loco.velY
		hanumanStatus.angularVel = loco.omgZ

		hanumanStatus.roll -= self._roll_offset
		hanumanStatus.pitch -= self._pitch_offset
		
		return hanumanStatus

	def calibrateIMU( self ):

		rollList = []
		pitchList = []

		rospy.loginfo( "Calibrating IMU.")

		for i in range( 5 ):

			self.spinal_cord.read_AllLowLevelData(timeout = self.__timeout)
			statusPackage = self.spinal_cord.get_statusPackage( )

			if statusPackage is None:
				return False

			hanumanStatus = HanumanStatus(statusPackage)

			rollList.append( float(hanumanStatus.roll) )
			pitchList.append( float(hanumanStatus.pitch) )

		self._roll_offset = sum( rollList ) / len( rollList )
		self._pitch_offset = sum( pitchList ) / len( pitchList )
		# self._roll_offset = 0.0
		# self._pitch_offset = 0.0

		rospy.loginfo( "roll offset: {}, pitch offset: {}".format( self._roll_offset, self._pitch_offset ) )

		return True

	def disconnectSpinalcord( self ):
		self.spinal_cord.disconnect( )