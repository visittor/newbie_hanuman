import time
import math
import numpy as np

class VelocityAccumulator( object ):

	def __init__( self, xInit = 0.0, yInit = 0.0, wInit = 0.0, covMat = None,
				mat = None ):
		## current velocity x, y and angular velocity
		self._currentVelX = 0.0
		self._currentVelY = 0.0
		self._currentOmgZ = 0.0

		## initial start time with None indicating accumulator is not running.
		self._startTime = None

		self.__locking = False

		## current position; x, y, and theta
		self._currentX = xInit
		self._currentY = yInit
		self._currentAngle = wInit

		## init position
		self._initX = xInit
		self._initY = yInit
		self._initW = wInit

		if covMat is None:
			covMat = np.diag( [ 1, 1, 1 ] )

		assert covMat.shape == ( 3, 3 )

		self.__covMat0 = covMat.copy( )
		self._currentCovMat = covMat.copy( )

		self._uncertainty = np.zeros( (3,3) )

		if mat is None:
			self.__mat = np.diag( [1,1,1] ).astype( np.float64 )

		else:
			self.__mat = mat

	def setVelocity( self, velX, velY, omgZ, uncertainty = None ):
		'''
		Set velocity velX, velY and angular velocity omgZ.
		If accumulator is running, it stop running and accumulate previous vel 
		befor setting new velocities.
		arguments:
			velX 	:	( float, int ) linear velocity X.
			velY 	:	( float, int ) linear velocity Y.
			omgZ 	: 	( float, int ) angular velocity.
		'''
		if self.isRunning:
			self.endAccumulate( )

		self._currentVelX = float( velX )
		self._currentVelY = float( velY )
		self._currentOmgZ = float( omgZ )

		if uncertainty is not None:
			self._uncertainty = uncertainty

	def setPosition( self, x, y, w, covMat = None ):

		if self.isRunning:
			self.endAccumulate

		self._currentX = x
		self._currentY = y
		self._currentAngle = w

		if covMat is not None:
			self._currentCovMat = covMat.copy( )

	def setConvMat( self, convMat ):
		self._currentCovMat = convMat.copy()

	def startAccumulate( self ):
		'''
		Start accumulator. If accumulator is already running, do nothing.
		'''
		if self.__locking:
			return

		if self.isRunning:
			return 

		self._startTime = time.time()

	def getAccumulateVelocity( self ):
		'''
		Get cuurent position from accumulator.
		return 
			( float )currentX, ( float )currentY, ( float )theta
		'''
		## If accumulator is runnning, must update current pos before return.
		if self.isRunning:
			self.accumulateVelocity()

		return self._currentX, self._currentY, self._currentAngle

	def getCurrentCov( self ):

		return self._currentCovMat.copy( )

	def endAccumulate( self ):
		'''
		Stop accumulator. If accumulator is not running, do nothing.
		Otherwise, update current velocity once then stop accumulate.
		'''
		if self.__locking:
			return

		if not self.isRunning:
			return

		## Update current accumulate once before stop acumulate.
		self.accumulateVelocity()
		self._startTime = None

	def lock( self ):

		if self.isRunning:
			self.endAccumulate( )

		self.__locking = True

	def unlock( self ):

		self.__locking = False

	def accumulateVelocity( self ):
		'''
		Update current position base on current velocity and time pass.
		'''
		## vel_robot = < velX , velY >
		## vel_world = rotMat * vel_robot
		## rotMat = |cos(Z)		sin(z)	||velX|
		##			|					||	  |
		##			|-sin(z)	cos(z)	||velY|
		## velX_world = velX_robot*cos(omg*T) - velY_robot*sin(omg*T)
		## velY_world = velX_robot*sin(omg*T) + velY_robot*cos(omg*T)
		##
		## integrate( velX_w, T ) = (velX_r*sin(omg*T) + velY_r*cos(omg*T))/omg 
		## integrate( velY_w, T ) = ( -velX_r*cos(omg*T) + velY_r*sin(omg*T) ) / omg
		##
		velX = self._currentVelX
		velY = self._currentVelY
		omgZ = self._currentOmgZ

		vel = np.matmul( self.__mat, np.array( [velX, velY, omgZ] ).reshape(3,1) ).flatten()

		velX = vel[0]
		velY = vel[1]
		omgZ = vel[2]

		dT = time.time() - self._startTime
		self._startTime = time.time()

		if omgZ != 0:
			## Vx_world = Vx * cos( w ) - Vy * sin( w )
			## Vy_world = Vx * sin( w ) + Vy * cos( w )
			dX = (velX * math.sin(omgZ * dT) + velY * math.cos(omgZ * dT)) / omgZ
			dX -= velY / omgZ
			
			dY = (velY * math.sin(omgZ * dT) - velX * math.cos(omgZ * dT)) / omgZ
			dY += velX / omgZ

		else:
			dX = velX * dT
			dY = velY * dT

		dTheta = omgZ * dT

		dX_world = (dX*math.cos( self._currentAngle )) - (dY*math.sin( self._currentAngle ))
		dY_world = (dY*math.cos( self._currentAngle )) + (dX*math.sin( self._currentAngle ))

		self._currentX += dX_world
		self._currentY += dY_world
		self._currentAngle += dTheta
		self._currentAngle %= (2 * math.pi)
		
		dx_w_dx = 1
		dx_w_dy = 0
		dx_w_dw = -dX*math.sin( self._currentAngle ) - dY*math.cos( self._currentAngle )
		
		dy_w_dx = 0
		dy_w_dy = 1
		dy_w_dw = -dY*math.sin( self._currentAngle ) + dX*math.cos( self._currentAngle )

##				dx 			dy 			dw
		jac = [ [dx_w_dx,	dx_w_dy,	dx_w_dw ],#dx
				[dy_w_dx,	dy_w_dy,	dy_w_dw ],#dy
				[0,			0,			1		],#dw
			]
		jac = np.array( jac )

		self._currentCovMat = np.matmul( jac, np.matmul( self._currentCovMat, jac.T ) ) + self._uncertainty

	def reset( self ):
		'''
		Reset velocity and current position then stop accumulate.
		'''
		self._currentVelX = 0
		self._currentVelY = 0
		self._currentOmgZ = 0

		self._currentX = self._initX
		self._currentY = self._initY
		self._currentAngle = self._initW

		self._currentCovMat = self.__covMat0.copy( )

		self.endAccumulate()

	def resetCovmat( self ):
		self._currentCovMat = self.__covMat0.copy( )

	@property
	def isRunning( self ):
		return self._startTime is not None

	@property
	def locking( self ):
		return self.__locking


if __name__ == '__main__':
	pass

