#!/usr/bin/env python
import rospy

from sensor_msgs.msg import JointState
from std_msgs.msg import Float64
from newbie_hanuman.msg import PanTiltCommand

from cell.nodeCell import NodeBase
from spinalCord.Sternocleidomastoid import Sternocleidomastoid

import numpy as np 

import math

class ControllerSimulator( Sternocleidomastoid ):

	def __init__( self ):

		# rospy.set_param( "/hanuman/joint_state_controller/type",
		# 				"joint_state_controller/JointStateController" )
		# rospy.set_param( "/hanuman/joint_state_controller/publish_rate",
		# 				50)

		# rospy.set_param( "/hanuman/pan_position_controller/type",
		# 				 "effort_controllers/JointPositionController" )
		# rospy.set_param( "/hanuman/pan_position_controller/joint",
		# 				 "pan" )
		# rospy.set_param( "/hanuman/pan_position_controller/pid/p", 100.0 )
		# rospy.set_param( "/hanuman/pan_position_controller/pid/i", 0.001 )
		# rospy.set_param( "/hanuman/pan_position_controller/pid/d", 10.0  )

		# rospy.set_param( "/hanuman/tilt_position_controller/type",
		# 				 "effort_controllers/JointPositionController" )
		# rospy.set_param( "/hanuman/tilt_position_controller/joint",
		# 				 "tilt" )
		# rospy.set_param( "/hanuman/tilt_position_controller/pid", 100.0 )
		# rospy.set_param( "/hanuman/tilt_position_controller/pid", 0.001 )
		# rospy.set_param( "/hanuman/tilt_position_controller/pid", 10.0  )

		self.__panPositionPublisher = rospy.Publisher( "/hanuman/pan_position_controller/command",
													Float64 )
		self.__tiltPositionPublisher = rospy.Publisher( "/hanuman/tilt_position_controller/command",
													Float64 )
		rospy.Subscriber( "/hanuman/joint_states", JointState, self.recieveJointStateCallback )

		super( ControllerSimulator, self ).__init__(  )
		
		self.currentPan = 0.0
		self.currentTilt = 0.0
		self.setFrequency( 50 )

	def recieveJointStateCallback( self, msg ):
		self.currentPan = msg.position[msg.name.index("pan")]
		self.currentTilt = msg.position[msg.name.index("tilt")]

	def _moveMotor( self, panPosition, panSpeed, tiltPosition, tiltSpeed ):

		self.__panPositionPublisher.publish( Float64( panPosition ) )
		self.__tiltPositionPublisher.publish( Float64( tiltPosition ) )

		return 1, 1

	def _readPos( self ):
		return self.currentPan, self.currentTilt

def main( ):
	node = ControllerSimulator()
	try:
		node.run()
	except rospy.ROSInterruptException:
		node.end()
	finally:
		node.end() 