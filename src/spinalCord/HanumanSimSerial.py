#!/usr/bin/env python
import numpy as np

from HumanoidBase import RegisterAddrToNameDict
from HanumanInterface import HanumanInterface
from VelocityAccumulator import VelocityAccumulator
from RobotisProtocolBase import RobotisProtocol

import rospy
import tf

from gazebo_msgs.msg import ModelState
from geometry_msgs.msg import Pose, Twist, Point, Quaternion

from cell.nodeCell import NodeBase

import time
import serial
import math

RegisterValue = {
					'REG_FIRMWARE_VERSION_L' : 1, 
					'REG_FIRMWARE_VERSION_H' : 1, 
					'REG_LOCOMOTION_COMMAND' : ord('n'),
					'reserved' : 0, 
					'REG_LOCOMOTION_STATUS_L' : 0, 
					'REG_LOCOMOTION_STATUS_H' : 0, 
					'REG_COMPASS_L' : 0, 
					'REG_COMPASS_H' : 0, 
					'REG_GYRO_ROT_X_L' : 0, 
					'REG_GYRO_ROT_X_H' : 0, 
					'REG_GYRO_ROT_Y_L' : 0, 
					'REG_GYRO_ROT_Y_H' : 0, 
					'REG_GYRO_ROT_Z_L' : 0, 
					'REG_GYRO_ROT_Z_H' : 0, 
					'REG_TILT_STATUS' : 0,
					'REG_RELOAD_REGISTER' : 0,
					'REG_ROBOT_LINEAR_VELOCITY_X' : 0,
					'REG_ROBOT_LINEAR_VELOCITY_Y' : 0,
					'REG_ROBOT_ANGULAR_VELOCITY' : 0, 
					'REG_MAGNETO_X_L' : 0,
					'REG_MAGNETO_X_H' : 0,
					'REG_MAGNETO_Y_L' : 0,
					'REG_MAGNETO_Y_H' : 0,
					'REG_MAGNETO_Z_L' : 0,
					'REG_MAGNETO_Z_H' : 0,
				}

class HanumanSerialSim( NodeBase ):

	def __init__( self ):

		self.RegisterValue = {
					'REG_FIRMWARE_VERSION_L' : 1, 
					'REG_FIRMWARE_VERSION_H' : 1, 
					'REG_LOCOMOTION_COMMAND' : ord('n'),
					'reserved' : 0, 
					'REG_LOCOMOTION_STATUS_L' : 0, 
					'REG_LOCOMOTION_STATUS_H' : 0, 
					'REG_COMPASS_L' : 0, 
					'REG_COMPASS_H' : 0, 
					'REG_GYRO_ROT_X_L' : 0, 
					'REG_GYRO_ROT_X_H' : 0, 
					'REG_GYRO_ROT_Y_L' : 0, 
					'REG_GYRO_ROT_Y_H' : 0, 
					'REG_GYRO_ROT_Z_L' : 0, 
					'REG_GYRO_ROT_Z_H' : 0, 
					'REG_TILT_STATUS' : 0,
					'REG_RELOAD_REGISTER' : 0,
					'REG_ROBOT_LINEAR_VELOCITY_X' : 127,
					'REG_ROBOT_LINEAR_VELOCITY_Y' : 127,
					'REG_ROBOT_ANGULAR_VELOCITY' : 127, 
					'REG_MAGNETO_X_L' : 0,
					'REG_MAGNETO_X_H' : 0,
					'REG_MAGNETO_Y_L' : 0,
					'REG_MAGNETO_Y_H' : 0,
					'REG_MAGNETO_Z_L' : 0,
					'REG_MAGNETO_Z_H' : 0,

					'WORLD_PITCH_ANGLE_L' : 127,
					'WORLD_PITCH_ANGLE_H' : 0,
					'WORLD_ROLL_ANGLE_L'  : 127,
					'WORLD_ROLL_ANGLE_H'  : 0,
				}

		self.__XWorldTranMax =  0.25
		self.__XWorldTranMin =  -0.25
		self.__YWorldTranMax = 0.105
		self.__YWorldTranMin =  -0.105
		self.__ZWorldAnglMax =  math.radians(66.0)
		self.__ZWorldAnglMin =  math.radians(-66.0)
		self.__XRegisTranMax = 167
		self.__XRegisTranMin = 87
		self.__YRegisTranMax = 167
		self.__YRegisTranMin = 87
		self.__ZRegisAnglMax = 167
		self.__ZRegisAnglMin = 87


		super( HanumanSerialSim, self ).__init__( "hanuman_serial_sim" )

		self.__comPort = self.getParam( self.nameSpace+"motor_cortex/sim_serial_port", "/tmp/tty0" )
		self.__baudrate = self.getParam( self.nameSpace+"motor_cortex/baudrate", 9600 )

		self.__serial = serial.Serial(self.__comPort, self.__baudrate, rtscts = True, dsrdtr = True)

		self.__velAccum = VelocityAccumulator( )

		self.rosInitNode()
		self.rosInitPublisher( "/gazebo/set_model_state", ModelState, queue_size = 1)
		self.setFrequency(100)

	def __createMessage( self, x, y, w ):

		position = Point( x = x, y = y, z = 0.0 )

		quaternion = tf.transformations.quaternion_from_euler( 0, 0, w )
		orientation = Quaternion( x = quaternion[0], 
								y = quaternion[1], 
								z = quaternion[2], 
								w = quaternion[3] )

		msg = ModelState()
		msg.model_name = "hanuman"
		msg.pose = Pose( position = position, orientation = orientation )
		msg.twist = Twist( )
		msg.reference_frame = "world"

		return msg

	def __getVelocityFromRegister( self ):
		linearX_reg = self.RegisterValue[ 'REG_ROBOT_LINEAR_VELOCITY_X' ]
		linearY_reg = self.RegisterValue[ 'REG_ROBOT_LINEAR_VELOCITY_Y' ]
		angular_reg = self.RegisterValue[ 'REG_ROBOT_ANGULAR_VELOCITY' ]

		linearX_world = HanumanInterface.convertLocomotionRegToWorld( linearX_reg, 
														self.__XRegisTranMax,
														self.__XRegisTranMin,
														self.__XWorldTranMax,
														self.__XWorldTranMin
														)
		linearY_world = HanumanInterface.convertLocomotionRegToWorld( linearY_reg, 
														self.__YRegisTranMax,
														self.__YRegisTranMin,
														self.__YWorldTranMax,
														self.__YWorldTranMin
														)
		angular_world = HanumanInterface.convertLocomotionRegToWorld( angular_reg, 
														self.__ZRegisAnglMax,
														self.__ZRegisAnglMin,
														self.__ZWorldAnglMax,
														self.__ZWorldAnglMin
														)		
		return linearX_world, linearY_world, angular_world

	def __rosInitPunlisher( self ):
		self.rosInitPublisher( "/gazebo/set_model_state",
								ModelState )

	def analyzePackage( self, package ):

		defaultResponse = [ ]

		if len( package ) == 0:
			return None

		if len( package ) < 4 or len( package ) != package[3] + 4:
			rospy.logwarn("Wrong package protocol"+str(package))
			return defaultResponse

		elif package[0] != 0xff or package[1] != 0xff or package[2] != 0x01:
			rospy.logwarn("Wrong package protocol",str(package))
			return defaultResponse

		if package[4] == 0x02:
			startAddr = package[5]
			nData = package[6]
			data = []
			for i in range( nData ):
				addrName = RegisterAddrToNameDict[ i + startAddr ]
				data.append( self.RegisterValue[ addrName ] )
			return data

		elif package[4] == 0x03:
			startAddr = package[5]
			nAddr = package[3] - 3
			data = package[6:-1]
			for i in range( nAddr ):
				addrName = RegisterAddrToNameDict[ i + startAddr ]
				self.RegisterValue[ addrName ] = data[i]

			return defaultResponse

		return None

	def run( self ):

		rospy.loginfo( "Start serial sim." )
		while not rospy.is_shutdown():

			package = self.__serial.read( self.__serial.inWaiting() )
			package = map( lambda x: ord(x), list( package ) )

			response = self.analyzePackage( package )
			
			if response is not None:
				returnPack = [ 0xff, 0xff, 0x01, len(response)+2, 0x00 ]
				returnPack.extend( response )
				returnPack.append( RobotisProtocol.computeChecksum( returnPack ) )

				self.__serial.write( returnPack )

			velX, velY, angZ = self.__getVelocityFromRegister( )

			self.__velAccum.setVelocity( velX, velY, angZ )
			x, y, w = self.__velAccum.getAccumulateVelocity( )
			self.__velAccum.startAccumulate( )

			msg = self.__createMessage( x, y, w )
			# print msg
			# self.publish( msg )

			self.sleep()

		rospy.loginfo( "Exit serial sim." )

	def end( self ):
		self.__serial.close( )
		time.sleep( 0.001 )
		if self.__serial.is_open:
			rospy.logwarn( "Cannot close"+str( self.__serial.port ) )

def main( ):
	node = HanumanSerialSim( )
	try:
		node.run( )
	except rospy.ROSInterruptException:
		pass
	finally:
		node.end()