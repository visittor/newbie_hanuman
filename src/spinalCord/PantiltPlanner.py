#!/usr/bin/env python
import rospy

from sensor_msgs.msg import JointState
from newbie_hanuman.msg import PanTiltCommand
from newbie_hanuman.msg import postDictMsg, visionMsg

from newbie_hanuman.srv import PanTiltPlannerCommand, PanTiltPlannerCommandResponse

from cell.nodeCell import NodeBase

from utility.utility import ObjectIndexLookUp

import numpy as np

import math

import configobj

import time

from ScannerBase import ScannerBase

#################################################
#
#	Global variable
#		       


PATTERN = {}

PATTERN["basic_pattern"] = [ (math.radians(-30), math.radians(0)),
			     (math.radians(30), math.radians(0)),
			     (math.radians(30), math.radians(30)),
			     (math.radians(-30), math.radians(30))]
			     
PATTERN["basic_pattern_near"] = [ (math.radians(-30), math.radians(10)),
			          (math.radians(30), math.radians(10)),
			          (math.radians(30), math.radians(40)),
			          (math.radians(-30), math.radians(40))]

PATTERN["scan_horizon"] = [ (math.radians(-75), math.radians(0)),
			     			(math.radians(75), math.radians(0))]

PATTERN["scan_horizon_low"] = [ (math.radians(-75), math.radians(30)),
			     			(math.radians(75), math.radians(30))]

PATTERN["findball_pattern"] = [ ( math.radians( -30 ), math.radians( 15 ) ), 
								( math.radians( 30 ),  math.radians( 15 ) ), 
								( math.radians( 30 ), math.radians( 45 ) ), 
								( math.radians( -30 ),  math.radians( 45 ) ) ]
			     
PATTERN[ 'find_goal' ] = [ (math.radians(-30), math.radians(5)),
			   (math.radians(30), math.radians(5))]

PATTERN[ 'find_goal_inv' ] = [ (math.radians(30), math.radians(5)),
			       (math.radians(-30), math.radians(5))]


#################################################
#
#	Class and Function Defenitions
#		       



def getJsPosFromName(Js, name):
	indexJs = Js.name.index(name)
	return Js.position[indexJs]

def getJsVelFromName(Js, name):
	indexJs = Js.name.index(name)
	return Js.velocity[indexJs]

class PantiltPlanner(NodeBase):

	class DummyScaner(object):
		def initialScan(self):
			pass

		def execute(self):
			pass

		def terminate(self):
			pass

	class Scan(object):
		def __init__(self, velocity, patterns = PATTERN):
			self.__velocity = velocity
			self.__patterns = patterns

			self.__startTime = None
			self.__currentPattern = None
			self.__currentIndex = None
			self.__goalPosition = None
			self.__prevPosition = None

			self.__isTerminate = True

		def findClosestIndex( self, pattern, currentPosition ):
			curPan, curTilt = currentPosition
			distance = -1
			index = 0
			for i, (pan, tilt) in enumerate(pattern):

				d = max( math.fabs(curPan-pan), math.fabs(curTilt-tilt) )
				if distance < 0 or d < distance:
					index = i
					distance = d

			return index

		def initialScan(self, patternName, currentPosition):
			if not self.__patterns.has_key(patternName):
				rospy.logwarn("No pattern name \""+patternName+"\". Terminate this scan.")
				return False
			self.__startTime = time.time()
			if self.__currentPattern == patternName and not self.__isTerminate:
				return False

			self.__prevPosition = currentPosition
			self.__currentPattern = self.__patterns[patternName]
			self.__currentIndex = self.findClosestIndex( self.__currentPattern, currentPosition )
			rospy.loginfo( "[PantiltPlanner]Do scan pattern \'{}\'".format(patternName))
			rospy.logdebug( "[PantiltPlanner]Start scan index {}".format(self.__currentIndex) )
			self.__goalPosition = self.__currentPattern[self.__currentIndex]
			self.__startTime = time.time()

			self.__isTerminate = False
			return True

		def execute(self):
			if self.__isTerminate:
				return

			s = self.__velocity * (time.time() - self.__startTime)

			direcPan  = 1 if self.__goalPosition[0]>self.__prevPosition[0] else -1
			direcTilt  = 1 if self.__goalPosition[1]>self.__prevPosition[1] else -1

			desirePan = self.__prevPosition[0] + (direcPan*s)
			desireTilt= self.__prevPosition[1] + (direcTilt*s)
			tiltReach = False
			panReach = False
			if time.time() - self.__startTime >= math.fabs(self.__goalPosition[0] - self.__prevPosition[0])/self.__velocity:
				panReach = True
				desirePan = self.__goalPosition[0]

			if time.time() - self.__startTime >= math.fabs(self.__goalPosition[1] - self.__prevPosition[1])/self.__velocity:
				tiltReach = True
				desireTilt = self.__goalPosition[1]

			if panReach and tiltReach:
				self.__getNextGoalPosition()

			c = PanTiltCommand(	name 		= 	["pan", "tilt"],
								position 	=	[desirePan, desireTilt],
								velocity 	=	[0,0],
								command 	=	[1,1])
			return c

		def __getNextGoalPosition(self):
			self.__prevPosition = self.__goalPosition
			self.__currentIndex += 1
			self.__currentIndex %= len(self.__currentPattern)
			self.__goalPosition = self.__currentPattern[self.__currentIndex]
			self.__startTime = time.time()

		def terminate(self):
			self.__startTime = None
			self.__currentPattern = None
			self.__currentIndex = None
			self.__goalPosition = None
			self.__prevPosition = None

			self.__isTerminate = True

	class SetPosition(object):
		def __init__(self, velocity):
			self.__velocity = velocity

			self.__startTime = None
			self.__goalPosition = None
			self.__prevPosition = None
			self.__actualVel = (self.__velocity, self.__velocity)

			self.__isTerminate = True

		def initialScan(self, goalPosition, currentPosition, velocity=None):

			self.__goalPosition = goalPosition
			self.__prevPosition = currentPosition

			if velocity is not None:
				self.__actualVel = velocity
			else:
				self.__actualVel = (self.__velocity, self.__velocity)

			self.__startTime = time.time()
			self.__isTerminate = False

		def execute(self):
			if self.__isTerminate:
				return

			s_pan = self.__actualVel[0] * (time.time() - self.__startTime)
			s_tilt = self.__actualVel[1] * (time.time()- self.__startTime)

			direcPan  = 1 if self.__goalPosition[0]>self.__prevPosition[0] else -1
			direcTilt  = 1 if self.__goalPosition[1]>self.__prevPosition[1] else -1

			desirePan = self.__prevPosition[0] + (direcPan*s_pan)
			desireTilt = self.__prevPosition[1] + (direcTilt*s_tilt)

			tiltReach = False
			panReach = False

			if time.time()-self.__startTime>=math.fabs(self.__goalPosition[0] - self.__prevPosition[0])/self.__actualVel[0]:
				panReach = True
				desirePan = self.__goalPosition[0]

			if time.time()-self.__startTime>=math.fabs(self.__goalPosition[1] - self.__prevPosition[1])/self.__actualVel[1]:
				tiltReach = True
				desireTilt = self.__goalPosition[1]

			c = PanTiltCommand(	name 		= 	["pan", "tilt"],
								position 	=	[desirePan, desireTilt],
								velocity 	=	[0,0],
								command 	=	[1,1])
			return c

		def terminate(self):
			self.__startTime = None
			self.__goalPosition = None
			self.__prevPosition = None
			self.__actualVel = (self.__velocity, self.__velocity)

			self.__isTerminate = True

	class PDControl(object):
		def __init__(self, kp, kd):
			self.__kp = kp
			self.__kd = kd
			self.__prevTime = time.time()

			self.__prevError = None
			self.__currError = None
			self.__isTerminate = True

		def initialScan(self, error):
			self.__currError = error
			self.__isTerminate = False


		def execute(self):
			if self.__isTerminate:
				return

			errorPan = min(max(self.__currError[0], -1.0), 1.0)
			errorTilt = min(max(self.__currError[1], -1.0), 1.0)

			if -0.2 < errorPan < 0.2:
				errorPan = 0.0

			if -0.2 < errorTilt < 0.2:
				errorTilt = 0.0

			derivTermPan = self.__kd*(errorPan - self.__prevError[0]) if self.__prevError is not None else 0.0
			propTermPan = self.__kp*errorPan
			pdPan = propTermPan - derivTermPan/(time.time() - self.__prevTime)
			# print propTermPan, derivTermPan, errorPan

			derivTermTilt = self.__kd*(errorTilt - self.__prevError[1]) if self.__prevError is not None else 0.0
			propTermTilt = self.__kp*errorTilt
			pdTilt = propTermTilt - derivTermTilt/(time.time() - self.__prevTime)

			self.__prevError = self.__currError
			self.__prevTime = time.time()
			c = PanTiltCommand(	name 		= 	["pan", "tilt"],
								position 	=	[pdPan, pdTilt],
								velocity 	=	[0,0],
								command 	=	[0,0])
			self.__currError = [0,0]
			return c

		def terminate(self):
			self.__isTerminate = True
			# self.__prevError = None
			# self.__currError = None
			# self.__prevTime = time.time()

	
	class TrackingPanTilt( ScannerBase ):
		'''
			tracking ball by using field of view
		'''
		def __init__( self, config ):
			
			self.__isTerminate = True

			# #	load robot parameter via configboj
			# robotConfigPathStr = rospy.get_param( "/robot_config", None )
			
			# if robotConfigPathStr is None:
			# 	raise TypeError( "Robot config should not NoneType" )
			# config = configobj.ConfigObj( robotConfigPathStr )
		
			#	get camera parameter
			self.focalWidth = float( config[ "CameraParameters" ][ "fx" ] )
			self.focalHeight = float( config[ "CameraParameters" ][ "fy" ] )

			self.fov_x_gain = float( config[ "PanTiltPlanner" ]["fov_x_gain"] )
			self.fov_y_gain = float( config[ "PanTiltPlanner" ]["fov_y_gain"] )

			self.fov_x_dgain = float( config[ "PanTiltPlanner" ][ "fov_x_dgain" ] )
			self.fov_y_dgain = float( config[ "PanTiltPlanner" ][ "fov_y_dgain" ] )

			self.timeLoopForLookAtObject = float( config[ "PanTiltPlanner" ][ "TimeLoopForLookAtObject" ] )

			self.__posDictMsg = None

			self.previousErrorX = 0.0
			self.previousErrorY = 0.0
			
			#	initial attribute for get index to stare object
			self.objectIndex = 0 
			
			#	attribute for store previous time
			self.previousTime = time.time()

			self.objectName = None

		def initialScan( self, objectName ):
			'''
				initial scan
				arg:
					objectName : set object to track
			'''

			#	set termimate false
			self.__isTerminate = False

			self.objectName = objectName

			self.previousErrorX = None
			self.previousErrorY = None
			# try:
			# 	#	get object index
			# 	self.objectIndex = self.__posDictMsg.object_name.index( objectName )
			# except:
			# 	raise( TypeError( 'Pantilt planner does not support this {}'.format( objectName ) ) )
			 
			#	recive time
			# self.previousTime = time.time()
		
		def calculataFieldOfView( self, imgWidth, imgHeight ):
			'''
				Calculate field of view
				NOTE : Mathematic calculation for finding pan/tilt
				fov = 2 * atan( 0.5 * w or h / focal_lenght )
    		''' 
			fovWidth = 2 * np.arctan( 0.5 * imgWidth / self.focalWidth )
			fovHeight =  2 * np.arctan( 0.5 * imgHeight / self.focalHeight )
		
			return fovWidth, fovHeight
		
		def execute( self ):
			'''
				calulate angle of pantilt\n
				NOTE :
					Directon of motor	
					Pan -> left ( + ), right ( - )
					Tilt -> down ( + ), up ( - )
					* direction relate to robot
			'''
			if self.__posDictMsg is None:
				return None

			if time.time() - self.previousTime >= self.timeLoopForLookAtObject:
				# print time.time() - self.previousTime 
				#	get value from posdict msg
				imgWidth = self.__posDictMsg.imgW
				imgHeight = self.__posDictMsg.imgH

				#	check element in list
				if self.objectName not in self.__posDictMsg.object_name:

					command = PanTiltCommand( name = [ "pan", "tilt" ],
								  position = [ 0, 0 ],
								  velocity = [ 0, 0 ],
								  command  = [ 0, 0 ] )

					return None

				objectIndex = self.__posDictMsg.object_name.index( self.objectName  )

				#	recieve error from msg
				ballErrorX = self.__posDictMsg.object_error[ objectIndex ].x
				ballErrorY = self.__posDictMsg.object_error[ objectIndex ].y

				if math.fabs( ballErrorX ) < 0.1:
					self.previousErrorX = 0.0
					ballErrorX = 0.0
				
				if math.fabs( ballErrorY ) < 0.1:
					self.previousErrorY = 0.0
					ballErrorY = 0.0

				#	calculate fov
				fovWidth, fovHeight = self.calculataFieldOfView( imgWidth, imgHeight )

				#	calculate pan and tilt angle for object tracking
				#	ratio * 0.5 * field of view
				#	Pan and tilt from zero center
				#	TODO: check error sign later
				errorPanAngle = ( -1 * ballErrorX * fovWidth / 2 )
				errorTiltAngle = ( ballErrorY * fovHeight / 2 )

				dT = time.time() - self.previousTime 

				if self.previousErrorX is None or self.previousErrorY is None:
					#	PD control
					movePanAngle = (self.fov_x_gain * errorPanAngle)
					moveTiltAngle = (self.fov_y_gain * errorTiltAngle)

				else:
					movePanAngle = (self.fov_x_gain * errorPanAngle)  +  (self.fov_x_dgain * ( errorPanAngle - self.previousErrorX ) / dT )
					moveTiltAngle = (self.fov_y_gain * errorTiltAngle) + (self.fov_y_dgain * ( errorTiltAngle - self.previousErrorY ) / dT )

				rospy.logdebug( " panAngle : {}, tiltAngle : {} ".format( np.rad2deg( movePanAngle ), np.rad2deg( moveTiltAngle ) ) )

				# #	send message to pantilt by using PantiltCommand
				command = PanTiltCommand( name = [ "pan", "tilt" ],
							  position = [ movePanAngle, moveTiltAngle ],
							  velocity = [ 0, 0 ],
							  command = [ 0, 0 ] )
				
				self.previousTime = time.time()
				self.__posDictMsg = None

				self.previousErrorX = errorPanAngle
				self.previousErrorY = errorTiltAngle

				return command
				
			else:
				
				return None


		def terminate( self ):
			'''
				terminate tracking
			'''
			self.__isTerminate = True
		
		def getPosdictMsgFromCallBackFcn( self, posDictMsg ):
			'''
				get posdict msg when pantilt planner call callback function
				when message posdict send to this node
			'''
			self.__posDictMsg = posDictMsg

		def getPosdictMsg( self ):
			return self.__posDictMsg
		

	def __init__(self):
		super(PantiltPlanner, self).__init__("pantiltplanner")
		self.rosInitNode()

		DEFAULT_CONFIG = { 'PanTiltPlanner' : { 'scan_velocity' : '30',
												'scan_pattern'  : None,
												'TimeLoopForLookAtObject' : '1.0',
												'fov_x_gain'	: '1.0',
												'fov_y_gain'	: '1.0',  },

							'CameraParameters' : {  'fx' : '589.1634268101999',
													'fy' : '578.6669308830411' } }

		configPath = rospy.get_param( "/robot_config", None )
		
		if configPath is None:
			config = DEFAULT_CONFIG
		else:
			config = configobj.ConfigObj( configPath )
			
			config.setdefault( 'PanTiltPlanner', DEFAULT_CONFIG['PanTiltPlanner'] )
			config.setdefault( 'CameraParameters', DEFAULT_CONFIG['CameraParameters'])

			for key, val in DEFAULT_CONFIG['PanTiltPlanner'].items( ):
				config['PanTiltPlanner'].setdefault( key, val )

			for key, val in DEFAULT_CONFIG['CameraParameters'].items( ):
				config['CameraParameters'].setdefault( key, val )

		pantiltVel = math.radians(float(config['PanTiltPlanner']['scan_velocity']))

		patterns = self._readPatternFromConfig( config['PanTiltPlanner']['scan_pattern'] )

		rospy.logdebug( patterns )

		self.__dummy = PantiltPlanner.DummyScaner()

		self.__patternScaner = PantiltPlanner.Scan(	pantiltVel, 
													patterns = patterns)

		self.__setPosition   = PantiltPlanner.SetPosition(pantiltVel)

		self.__scaner = self.__dummy

		self.__executeFunction = None
		self.__nextExecuteFunc = None

		self.__currentPosition = (0.0,0.0)

		# self.setFrequencyFromParam(
		# 					self.nameSpace+'spinalcord/pantilt_frequency')
		self.setFrequency( 20 )
		self.__rosInitSubPubSer()

		#	define error pan and tilt
		self.__errorPan = None
		self.__errorTilt = None
		self.__trackingObject = PantiltPlanner.TrackingPanTilt( config )

	def _readPatternFromConfig( self, patternConfig ):
		if patternConfig is None:
			return PATTERN

		for name, patterns in patternConfig.items( ):

			patternList = []

			i = 1
			while patterns.has_key( str( i ) ):
				pan, tilt = eval(patterns[str(i)])
				patternList.append( (math.radians(float(pan)), math.radians(float(tilt))) )
				i += 1

			PATTERN[ name ] = patternList

		return PATTERN

	def __rosInitSubPubSer(self):
		self.rosInitPublisher(	"/spinalcord/sternocleidomastoid_command",
			 					PanTiltCommand,
			 					queue_size = 1)
		self.rosInitSubscriber(	"/spinalcord/sternocleidomastoid_position", 
								JointState, 
								self.__callback)
		self.rosInitService(	"pantiltplannercommand", 
								PanTiltPlannerCommand, 
								self.__serviceHandle)

		#	add subscriber for subscripte kinematic_topic
		rospy.Subscriber( "/vision_manager/occipital_lobe_topic", 
						  visionMsg, 
						  self.__posdictCallback )

	def __serviceHandle(self, req):

		rospy.loginfo( "PantiltPlanner :: serviceHandle -> command req = {}".format( req.command ) )

		if req.command == 1:
			self.__scaner.terminate()
			self.__patternScaner.initialScan(req.pattern, 
											self.__currentPosition)
			self.__scaner = self.__patternScaner

		elif req.command == 0:
			if len(req.velocity) >= 2:
				panVel = getJsVelFromName(req, 'pan')
				tiltVel = getJsVelFromName(req, 'tilt')
				velocity = (panVel, tiltVel) if panVel * tiltVel != 0 else None
			else:
				velocity = None
			panGoal = getJsPosFromName(req, 'pan')
			tiltGoal= getJsPosFromName(req, 'tilt')

			self.__scaner.terminate()
			self.__setPosition.initialScan( (panGoal, tiltGoal),
											self.__currentPosition,
											velocity = velocity)
			self.__scaner = self.__setPosition

		elif req.command == 2:
			self.__scaner.terminate()
			self.__trackingObject.initialScan( req.pattern )
			self.__scaner = self.__trackingObject

		elif req.command == 3:
			self.__scaner.terminate()
			self.__scaner = self.__dummy

		else:
			return PanTiltPlannerCommandResponse(success = False)

		return PanTiltPlannerCommandResponse(success = True)

	def __callback(self, msg):
		panAng = getJsPosFromName(msg, "pan")
		tiltAng = getJsPosFromName(msg, "tilt")
		self.__currentPosition = (panAng, tiltAng)

	def __posdictCallback( self, msg ):
		'''
			callback function 
		'''
		
		#	get position dictionary message to tracking object class
		self.__trackingObject.getPosdictMsgFromCallBackFcn( msg )

	def run(self):
		rospy.loginfo("Start pantilt planer node.")
		while  not rospy.is_shutdown():
			command = self.__scaner.execute()
			# print command
			if command is not None:
				self.publish(command)
			self.sleep()

	def end(self):
		rospy.loginfo("Exit pantilt planer node.")

def main():
	node = PantiltPlanner()
	try:
		node.run()
	except rospy.ROSInterruptException as e:
		print e
		rospy.logwarn(e)
	# except Exception as e:
		# print e
	# 	rospy.logwarn(e)
	finally:
		node.end()
