#!/usr/bin/env python
#
# Copyright (C) 2019  FIBO/KMUTT
#			Written by Nasrun (NeverHoliday) Hayeeyama
#

########################################################
#
#	STANDARD IMPORTS
#

import sys
import os

########################################################
#
#	LOCAL IMPORTS
#

########################################################
#
#	GLOBALS
#

########################################################
#
#	EXCEPTION DEFINITIONS
#

########################################################
#
#	HELPER FUNCTIONS
#

########################################################
#
#	CLASS DEFINITIONS
#

class ScannerBase( object ):
		
	def initialScan( self, *args, **kwargs ):
		
		raise NotImplementedError
		
	def execute( self ):
		
		raise NotImplementedError
		
	def terminate( self ):
		
		raise NotImplementedError
