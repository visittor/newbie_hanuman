#!/usr/bin/env python
from HanumanInterface import HanumanInterface, LocomotionCommand,LocomotionSpecialCommand
from VelocityAccumulator import VelocityAccumulator

import rospy
import roslib

from newbie_hanuman.srv import MotorCortexCommand,MotorCortexCommandResponse
from newbie_hanuman.srv import reqMotorCortexIntegration, reqMotorCortexIntegrationResponse
from newbie_hanuman.msg import HanumanStatusMsg

from cell.nodeCell import NodeBase

import time
import math
from serial import Serial
import configobj

DEFAULT_ROBOT_VEL = { 	'max_positive_velX'	:	0.155,
						'max_negative_velX'	:	0.125,
						
						'max_register_velX'	:	160,
						'min_register_velX'	:	87,
						
						'max_positive_velY'	:	0.068,
						'max_negative_velY'	:	0.087,
						
						'max_register_velY'	:	157,
						'min_register_velY'	:	97,
						
						'max_positive_omgZ'	:	28.4760024,
						'max_negative_omgZ'	:	28.7624813,
						
						'max_register_omgZ'	:	147,
						'min_register_omgZ'	:	107,

						'slidecurve_left_register_velX'		:	127,
						'slidecurve_left_register_velY'		:	142,
						'slidecurve_left_register_omgZ'		:	119,

						'slidecurve_right_register_velX'	:	127,
						'slidecurve_right_register_velY'	:	112,
						'slidecurve_right_register_omgZ'	:	135,
					}

class MotorCortex(NodeBase):

	def __init__(self):
		super(MotorCortex, self).__init__("motor_cortex")

		configPath = self.getParam( '/robot_config', None )

		if configPath is not None:
			config = configobj.ConfigObj( configPath )

			if not config.has_key( 'RobotVelocity' ):
				configVelocity = DEFAULT_ROBOT_VEL

			else:
				configVelocity = config[ 'RobotVelocity' ]

		else:
			configVelocity = DEFAULT_ROBOT_VEL

		## Set robot velocity config
		XWorldTranMax = float(configVelocity[ 'max_positive_velX' ])
		XWorldTranMin = -1.0 * float(configVelocity[ 'max_negative_velX' ])
		YWorldTranMax = float(configVelocity[ 'max_positive_velY' ])
		YWorldTranMin = -1.0 * float(configVelocity[ 'max_negative_velY' ])
		ZWorldAnglMax = math.radians(float(configVelocity[ 'max_positive_omgZ' ]))
		ZWorldAnglMin = -1.0 * math.radians(float(configVelocity[ 'max_negative_omgZ' ]))
		XRegisTranMax = float(configVelocity[ 'max_register_velX' ])
		XRegisTranMin = float(configVelocity[ 'min_register_velX' ])
		YRegisTranMax = float(configVelocity[ 'max_register_velY' ])
		YRegisTranMin = float(configVelocity[ 'min_register_velY' ])
		ZRegisAnglMax = float(configVelocity[ 'max_register_omgZ' ])
		ZRegisAnglMin = float(configVelocity[ 'min_register_omgZ' ])

		slideLeftRegisterX = float(configVelocity[ 'slidecurve_left_register_velX'])
		slideLeftRegisterY = float(configVelocity[ 'slidecurve_left_register_velY'])
		slideLeftRegisterZ = float(configVelocity[ 'slidecurve_left_register_omgZ'])

		slideRightRegisterX = float(configVelocity[ 'slidecurve_right_register_velX'])
		slideRightRegisterY = float(configVelocity[ 'slidecurve_right_register_velY'])
		slideRightRegisterZ = float(configVelocity[ 'slidecurve_right_register_omgZ'])

		self.slide_L_scale = (  (slideLeftRegisterX-127)/(XRegisTranMax-127),
								(slideLeftRegisterY-127)/(YRegisTranMax-127),
								(slideLeftRegisterZ-127)/(127-ZRegisAnglMin), )

		self.slide_R_scale = (  (slideRightRegisterX-127)/(XRegisTranMax-127),
								(slideRightRegisterY-127)/(127-YRegisTranMin),
								(slideRightRegisterZ-127)/(ZRegisAnglMax-127), )

		print configVelocity

		## Set timeout for each low-level comunication
		timeout = self.getParam(self.nameSpace+'motor_cortex/timeout',
								0.1)

		## Get time tolerance for each command. If any command exist longer than
		## time tolerance and not ignorable, that command will be ignore.
		timeTolerance = self.getParam(self.nameSpace+'motor_cortex/timeTolerance',1)

		## Serial config for comunicating with low-level
		comPort = self.getParam(self.nameSpace+'motor_cortex/comport','/tmp/ttyACM0')
		baudrate = self.getParam(self.nameSpace+'motor_cortex/baudrate',
										115200)
		rts = self.getParam(self.nameSpace+'motor_cortex/rts',0)
		dtr = self.getParam(self.nameSpace+'motor_cortex/dtr',0)

		## init ros node
		self.rosInitNode()

		## init serial port 
		serial = self.__connectSerialPort( comPort, baudrate, rts, dtr )

		## init hanuman interface
		self.__hanumanInterface = HanumanInterface( serial, 
													XWorldTranMax,
													XWorldTranMin,
													YWorldTranMax,
													YWorldTranMin,
													ZWorldAnglMax,
													ZWorldAnglMin,
													XRegisTranMax,
													XRegisTranMin,
													YRegisTranMax,
													YRegisTranMin,
													ZRegisAnglMax,
													ZRegisAnglMin,
													timeout = timeout,
										timeTolerance = timeTolerance)

		## init velocity accumulator
		self.__velocityAccum = VelocityAccumulator()

		## time for last reset integration and last fall down
		self.__lastResetIntegration = rospy.Time.now()
		self.__lastFallDown = rospy.Time.now()

		self.__initLocomotion()
		self.__rosInitSubPubSer()
		self.setFrequency(2)

	def __connectSerialPort(self, comPort, baudrate, rts, dtr ):
		serial = Serial()

		## if serial port is already open, we should shut it first.
		if serial.is_open:
			serial.close()

		## Set comport, baudrate, RTS and DTR.
		serial.port = str( comPort )
		serial.baudrate = int( baudrate )
		serial.setDTR( dtr )
		serial.setRTS( rts )

		## Try open a serial connection.
		## If any error occure, we will warn user and ignore that error.
		## NOTE : We will not stop this node if any error occure.
		try:
			serial.open()
		# except serial.SerialException as e:
		# 	rospy.logwarn(str(e))
		except IOError:
			rospy.logwarn( "This is a hack to handle error when using virtual serial port.")

		time.sleep(0.02)
		
		## Warn user if serial port is not opened.
		if not serial.is_open:
			rospy.logwarn("Serial port for locomotion ("+str(comPort)+") cannot be opened.")
		else:
			serial.flushInput()
			serial.flushOutput()
			rospy.loginfo("Connected to "+str(comPort)+".")

		return serial

	def __initLocomotion(self):
		## Force robot to stop first.
## NOTE : Chattarin : Not sure why we have to doCurrentCommand here. May be it is not neccessary.
		self.__hanumanInterface.forceStop()
		self.__hanumanInterface.doCurrentCommand()

		# if self.__hanumanInterface.getHanumanStatus( ) is None:
		# 	rospy.logwarn( "Cannot get lowlevel status" )

		time.sleep( 1 )

		for i in range( 5 ):

			is_success = self.__hanumanInterface.calibrateIMU( )
			
			if is_success:
				rospy.loginfo( "Success to calibrating IMU in attemp {}".format(i) )
				break

			rospy.logwarn( "Attemp {}, Calibration Fail.".format( i ) )
			time.sleep( 1 )

		else:
			rospy.logwarn( "Fail to calibrate IMU." )

	def __rosInitSubPubSer(self):
		## Init publisher and service
		self.rosInitPublisher(	"/spinalcord/hanuman_status",
								HanumanStatusMsg)
		self.rosInitService("motor_cortex_command", 
							MotorCortexCommand,
							self.__receiveCommand)

		rospy.Service(	"request_integration",
						reqMotorCortexIntegration,
						self.__reqIntegration)

	def __receiveCommand(self, req):
		'''
		Callback for motor_cortex_command
		'''
		if req.commandType == req.LOCOMOTIONCOMMAND:
			## Get valocities from request and create lococommand.
			vel_x = req.velX
			vel_y = req.velY
			omg_z = req.omgZ

			if req.command == 'OneStepWalk':
				command = LocomotionCommand(vel_x, vel_y, omg_z, oneStep = True)
			
			elif req.command == 'SlideCurve':
				if vel_y >= 0:
					command = LocomotionCommand(*self.slide_L_scale, oneStep = False)
				else:
					command = LocomotionCommand(*self.slide_R_scale, oneStep = False)

			else:
				command = LocomotionCommand(vel_x, vel_y, omg_z, oneStep = False)

			self.__hanumanInterface.setCommand(	command, 
												timeStamp = time.time(),
												ignorable= req.ignorable)

		elif req.commandType == req.SPECIALCOMMAND:
			## Get sspecial command and create lococommand
			commandString = req.command
			command = LocomotionSpecialCommand(commandString)
			self.__hanumanInterface.setCommand(	command, 
												timeStamp=time.time(),
												ignorable = req.ignorable)

		## Response to a request
		return MotorCortexCommandResponse(True)

	def __reqIntegration(self, req):
		'''
		Call back for request_integration.
		'''
		## User want to reset this accumulator then reset it and 
		## set last reset time to current time.
		if req.resetIntegration:
			self.__velocityAccum.reset()
			self.__lastResetIntegration = rospy.Time.now()

		## Get current x, y, and theta from accumulator
		x, y, theta = self.__velocityAccum.getAccumulateVelocity()

		## Create response.
		response = reqMotorCortexIntegrationResponse(x = x,
													 y = y,
													 theta = theta,
											lastFallDown = self.__lastFallDown,
							lastResetIntegration = self.__lastResetIntegration)
		return response

	def publish(self, status):
## TODO : Chattarin : if status is None, this node should publish some  
##					thing to indicate that this node cannot communicate with low-level.
		if status is None:
			return

		## Create message and blah, blah, blah.
		status.lastFallDownStamp = self.__lastFallDown
		msg = HanumanStatusMsg( **status.statusDict )
		super(MotorCortex, self).publish(msg)

	def run(self):
		rospy.loginfo("Start motor cortex node.")
		status = self.__hanumanInterface.getHanumanStatus()
		time.sleep(0.1)
		while not rospy.is_shutdown():
			## Status is None mean cannot communicate with low-level.
			if status is None:
				rospy.logwarn("Cannot get lowlevel status.")

			## If robot is fall down, force it to get up reset lastFallDown time
			elif status.statusDict["backFallDown"] or status.statusDict["frontFallDown"]:
				rospy.loginfo("Robot fall down. Reset integration and force standup.")
				time.sleep(0.1)
				self.__lastFallDown = rospy.Time.now()
				self.__hanumanInterface.forceStandup()
## NOTE : chattarin : I think it's not neccessary to reset integration.
				# self.__velocityAccum.reset()
				# self.__lastResetIntegration = rospy.Time.now()

			else:
				self.__hanumanInterface.doCurrentCommand()

			self.publish(status)

			time.sleep(0.1)

			## Get status after set lococommand.
			status = self.__hanumanInterface.getHanumanStatus()

			if status is not None:
				## Set accumulator base on status from low-level.
				self.__velocityAccum.setVelocity( status.linearVelX,
												status.linearVelY, 
												status.angularVel )
				self.__velocityAccum.startAccumulate()

			self.sleep()
		rospy.loginfo("Close motor cortex node.")

	def end(self):
		## force robot to stop and close serial port.
		self.__hanumanInterface.forceStop()

		self.__hanumanInterface.disconnectSpinalcord( )

def main():
	node = MotorCortex()
	try:
		node.run()
	except rospy.ROSInterruptException as e:
		node.end()
		rospy.logwarn(str(e))
	finally:
		node.end()