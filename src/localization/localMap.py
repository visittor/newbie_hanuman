import numpy as np
import cv2
import configobj
import copy
import matplotlib.pyplot as plt

from newbie_hanuman.msg import scanlinePointClound, localizationMsg
from newbie_hanuman.msg import HanumanStatusMsg, postDictMsg

from newbie_hanuman.srv import local_map_service, local_map_serviceResponse

from geometry_msgs.msg import Point32

from cell.nodeCell import NodeBase
from spinalCord.VelocityAccumulator import VelocityAccumulator

import time
import timeit
import math
import random

import rospy

import threading

lock = threading.Lock()

OBJECT_NAME =  ['field_corner', 'ball', 'goal', 'blue', 'yellow', 'magenta', 'orange' ]

RECOVERY_RATE = { n:0.75 for n in OBJECT_NAME }

DECAY_RATE = { n:0.99 for n in OBJECT_NAME }

class Object( object ):

	def __init__( self, name, x = 0, y = 0, score = 0, decayRate = 0.99, recoveryRate = 0.75,
				mergeRange = 2.0 ):
		self.__name = name
		self.__x = x
		self.__y = y

		self.__score = score * recoveryRate

		self.__decayRate = decayRate
		self.__recoveryRate = recoveryRate

		self.__mergeRange = mergeRange

	def seen( self, confidence ):
		if self.__score > confidence:
			return

		self.__score = confidence*self.__recoveryRate + self.__score*(1-self.__recoveryRate)

	def unseen( self ):
		self.__score = self.__decayRate*self.__score

	def getPolarCoor( self ):
		rho = math.sqrt( self.__x**2 + self.__y**2 )
		phi = math.atan2( self.__y, self.__x )
		return rho, phi

	def getCartCoor( self ):
		return self.__x, self.__y

	def move( self, dx, dy, dw ):
		self.__x -= dx
		self.__y -= dy

		self.__x = (math.cos( dw )*self.__x) + (math.sin( dw )*self.__y)
		self.__y = (math.cos( dw )*self.__y) - (math.sin( dw )*self.__x)

	def merge( self, x, y ):
		self.__x = 0.5*self.__x + 0.5*x
		self.__y = 0.5*self.__y + 0.5*y

	def distance( self, other ):

		if hasattr( other, 'getCartCoor' ):
			x, y = other.getCartCoor( )
		else:
			x, y = other[:2]

		return math.sqrt( (x-self.__x)**2 + (y-self.__y)**2 )

	def canMerge( self, other ):

		d = self.distance( other )
		return d <= self.__mergeRange

	@property
	def score( self ):
		return self.__score

def createObj( name, x, y, score ):
	if name == 'field_corner':
		return Object( name, x=x, y=y, score=score, mergeRange=5 )

	elif name == 'goal':
		return Object( name, x=x, y=y, score=score, mergeRange=0.5 )

	elif name == 'ball':
		return Object( name, x=x, y=y, score=score, mergeRange=0.1 )

	elif name in ['blue', 'yellow', 'magenta', 'orange']:
		return Object( name, x=x, y=y, score=score, mergeRange=1 )

class LocalMap( NodeBase ):

	def __init__( self ):

		super( LocalMap, self ).__init__( 'LocalMap' )

		self.rosInitNode( )

		self.setFrequencyFromParam( '/local_map/frequency' )

		self.__vis = self.getParam( '/visualize', 1 )

		self.__velocityAccum = VelocityAccumulator( xInit = 0.0,
													yInit = 0.0,
													wInit = 0.0,
													covMat = np.diag( [0.25,0.25,np.pi/10] ) 
												)

		self.objectDict = {'field_corner':[], 'goal':[], 'ball':[], 
							'blue':[], 'yellow':[], 'magenta':[], 'orange':[] }
		self.objectLimit = {'field_corner':2, 'goal':3, 'ball':1,
							'blue':1, 'yellow':1, 'magenta':1, 'orange':1 }
		self.msgDict = {}

		self.setRecoveryAndDecayRate( )

		self.rosInitSubscriber("/spinalcord/hanuman_status",
								HanumanStatusMsg,
								self.getHanumanStatusCallback
								)

		self.rosInitSubscriber("/vision_manager/kinematic_topic",
								postDictMsg,
								self.getActualSensorDataCallback
								)

		self.rosInitService("local_map_info", 
							local_map_service, 
							self.__serviceHandle)

	def setRecoveryAndDecayRate( self ):
		configPath = self.getParam( '/robot_config', '' )

		if configPath == '':
			return

		config = configobj.ConfigObj( configPath )

		if config.has_key( 'LocalMap' ):

			for key in OBJECT_NAME:
				if config['LocalMap'].has_key( key ):
					if config['LocalMap'][key].has_key( 'recovery_rate' ):
						RECOVERY_RATE[ key ] = float(config['LocalMap'][key]['recovery_rate'])
					if config['LocalMap'][key].has_key( 'decay_rate' ):
						DECAY_RATE[ key ] = float(config['LocalMap'][key]['decay_rate'])


	def __serviceHandle( self, req ):

		if req.reset:
			self.objectDict = {'field_corner':[], 'goal':[], 'ball':[], 
							'blue':[], 'yellow':[], 'magenta':[], 'orange':[] }

		object_names = []
		polars = []
		carts = []
		confidences = []
		for name, objList in self.objectDict.items( ):

			for obj in objList:
				object_names.append( name )
				
				rho, phi = obj.getPolarCoor( )
				polars.append( Point32( x = rho, y = phi, z = 0 ) )

				x, y = obj.getCartCoor( )
				carts.append( Point32( x = x, y = y, z = 0 ) )

				confidences.append( obj.score )

		postDict = postDictMsg( object_name = object_names,
								pos3D_cart = carts,
								pos2D_polar = polars,
								object_confidence = confidences )

		return local_map_serviceResponse( postDict = postDict )

	def getHanumanStatusCallback( self, msg ):

		# print "vel", msg.linearVelX, msg.linearVelY, msg.angularVel

		self.__velocityAccum.setVelocity( msg.linearVelX,
											msg.linearVelY, 
											msg.angularVel )
		self.__velocityAccum.startAccumulate( )

	def getActualSensorDataCallback( self, msg ):
		with lock:
			self.msgDict = {}
			for p, n, c in zip( msg.pos3D_cart, msg.object_name, msg.object_confidence ):
				self.msgDict.setdefault( n, [] ).append( (p.x, p.y, c) )

	def unseenObject( self, cutoff ):
		for name in self.objectDict:
			newObjList = []
			for obj in self.objectDict[name]:
				obj.unseen( )
				if obj.score > cutoff:
					newObjList.append( obj )
			self.objectDict[ name ] = newObjList

	def updateObject( self, mergeOnly = False ):

		seenObj = self.msgDict.items( )

		for name, coorList in seenObj:

			if not self.objectDict.has_key( name ):
				continue

			if len( coorList ) == 0:
				continue

			if len( self.objectDict[name] ) == 0 and not mergeOnly:
				coor = max( coorList, key = lambda x: x[2] )
				obj = createObj( name, coor[0], coor[1], coor[2] )
				self.objectDict[name].append( obj )

				i = coorList.index( coor )
				coorList.pop( i )

			for coor in coorList:

				minDist = -1
				minObj = None
				for obj in self.objectDict[ name ]:

					d = obj.distance( coor )
					if minDist == -1 or d < minDist  :
						minDist = d
						minObj = obj

				if minObj is not None and minObj.canMerge( coor ):
					minObj.merge( *coor[:2] )
					minObj.seen( coor[2] )

				elif not mergeOnly:
					if len(self.objectDict[name]) < self.objectLimit[name]:
						self.objectDict[name].append(createObj(name, coor[0], coor[1], coor[2]))
						continue

					minObj = min(self.objectDict[name], key = lambda x : x.score)
					minIdx = self.objectDict[name].index( minObj )

					self.objectDict[name][minIdx] = createObj(name, coor[0], coor[1], coor[2]) if minObj.score <= coor[2] else minObj
					continue

	def moveObject( self, dx, dy, dw ):
		for n, objList in self.objectDict.items( ):
			for obj in objList:
				obj.move( dx, dy, dw )

	def getRobotMovement( self ):
		# self._currPosition = self.__serviceHandle( resetIntegration = False )
	
		# theta = self._startPosition.theta

		# dx_world = self._currPosition.x - self._startPosition.x
		# dy_world = self._currPosition.y - self._startPosition.y

		# self._dPosition.x = dx_world*math.cos(theta) + dy_world*math.sin(theta)
		# self._dPosition.y = dy_world*math.cos(theta) - dx_world*math.sin(theta)
		# self._dPosition.theta = self._currPosition.theta - self._startPosition.theta

		# return self._dPosition.x, self._dPosition.y, self._dPosition.theta

		self.__velocityAccum.lock( )

		dx, dy, dw = self.__velocityAccum.getAccumulateVelocity( )

		self.__velocityAccum.setPosition( 0.0, 0.0, 0.0 )

		self.__velocityAccum.unlock( )

		self.__velocityAccum.startAccumulate( )

		return dx, dy, dw

	def findShootDirection( self, objectDict ):

		if len( objectDict[ 'goal' ] ) > 1:
			nearestGoal = min( objectDict[ 'goal' ], key = lambda obj: obj.getPolarCoor()[0] )

			_, phi1 = nearestGoal.getPolarCoor( )

			minDist = 6

			for obj in objectDict[ 'goal' ]:
				dist = nearestGoal.distance( obj )
				if dist < minDist and dist != 0:
					minDist = dist
					_, phi2 = obj.getPolarCoor( )
					# print "CASE 1", math.degrees( phi1 ), math.degrees( phi2 )
					return ( phi1 + phi2 ) / 2.0

		if len( objectDict[ 'goal' ] ) == 1:
			if len( objectDict['field_corner'] ) > 0:

				goal = objectDict['goal'][0]

				minDist = 6

				for corner in objectDict['field_corner']:
					dist = goal.distance(corner)

					if dist < minDist:
						_, phi1 = goal.getPolarCoor( )
						_, phi2 = corner.getPolarCoor( )
						# print "CASE 2"
						return phi1 - ((( phi1 + phi2 ) / 2) - phi1)

			# print "CASE 3"
			return objectDict['goal'][0].getPolarCoor( )[1]

		else:
			# print "CASE 4"
			return 0.0

	def run( self ):
		rospy.loginfo( "Start Local Map Node.")
		while not rospy.is_shutdown( ):

			dx, dy, dw = self.getRobotMovement( )

			self.unseenObject( 0.0 )

			if dx == 0 and dy == 0 and dw == 0:
				walking = False

			else:
				walking = True
				self.moveObject( dx, dy, dw )
			
			with lock:
				self.updateObject( mergeOnly = walking )

			self.sleep( )

	def visualize( self ):

		width = 800
		pixPerMetre = 50

		field = np.zeros( (width, width, 3), dtype = np.uint8 )

		for ii in range( 0, 180, 30 ):
			x1 = int((width / 2) * (1.5 * math.cos( math.radians( ii ) ) + 1 ))
			y1 = int((width / 2) * (1.5 * math.sin( math.radians( ii ) ) + 1 ))

			x2 = int((width / 2) * (-1.5 * math.cos( math.radians( ii ) ) + 1 ))
			y2 = int((width / 2) * (-1.5 * math.sin( math.radians( ii ) ) + 1 ))

			cv2.line( field, (x1,y1), (x2,y2), (255,255,255), 1 )

		for r in range( 0,  width, pixPerMetre ):
			cv2.circle( field, (width/2, width/2), r, (255,255,255), 1 )

			field[ :, r, : ] = [127, 127, 127]
			field[ r, :, : ] = [127, 127, 127]

		for n, objList in self.objectDict.items( ):

			for obj in objList:

				x, y = obj.getCartCoor( )
				x = int( x * pixPerMetre )
				y = int( y * pixPerMetre )
				y = (width / 2) - y
				x = (width / 2) - x

				scale = obj.score

				if n == 'field_corner':
					color = (255, 0, 255)
				elif n == 'goal':
					color = (0, 255, 255)
				elif n == 'ball':
					color = (255,255,0)
				elif n == 'orange':
					color = (0,128,255)
				elif n == 'blue':
					color = (255,0,0)
				elif n == 'magenta':
					color = (255,0,255)
				elif n == 'yellow':
					color = (0,255,255)
				else:
					color = (0,0,0)

				color = tuple( int( c * scale ) for c in color )

				cv2.circle( field, (y,x), 10, color, -1 )

		phi = self.findShootDirection( self.objectDict )
		if phi is not None:
			# print math.degrees( phi ), len( self.objectDict['goal'])
			x1 = int((width / 2) * (-1.5 * math.sin( phi ) + 1 ))
			y1 = int((width / 2) * (-1.5 * math.cos( phi ) + 1 ))

			cv2.line( field, (int(width / 2),int(width / 2)), (x1,y1), (0,255,255), 1 )
		else:
			print 'not find', len( self.objectDict['goal'])

		cv2.imshow( "3D", field )
		# cv2.imshow( "2D", blank)
		k = cv2.waitKey( 1 )

	def end( self ):
		cv2.destroyAllWindows( )
		rospy.loginfo( "Kill Local Node!!" )

def main( ):

	node = LocalMap( )

	try:
		node.run( )

	except rospy.ROSInterruptException as e:
		pass

	finally:
		node.end( )