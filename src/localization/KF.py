import numpy as np 
import cv2
import configobj
import matplotlib.pyplot as plt

from utility.HanumanForwardKinematic import getMatrixForForwardKinematic, loadDimensionFromConfig

from Robot import Robot, Camera
from Field import Field, FieldLine, CircleFieldLine, FieldBoundary, AbstractLandmark
from Painter import Painter

from spinalCord.VelocityAccumulator import VelocityAccumulator

import rospy

import time
import timeit
import math
import random

class KF( object ):

	def __init__( self, initX, initY, initW, Field ):

		self.__state = np.array( [ initX, initY, initW ] )
		self.__conv = np.diag( [1,1,1] ).astype( np.float64 )

	def predictionStep( self, state, convMat, odom, dT, uncertainty ):

		odom = np.array( odom ).reshape( 3, 1 )

		dx = odom[0,0]
		dy = odom[1,0]
		dw = odom[2,0]

		tranMat = np.array([[ math.cos(state[2]),	-math.sin(state[2]),	0 ],
							[ math.sin(state[2]),	math.cos(state[2]),		0 ],
							[ 0,				0,					1] ]
						)

		odomRobot = np.matmul( tranMat, odom )

		newState = state + odomRobot

		return newState.reshape( 3, 1 ), convMat + uncertainty

	def measurementUpdate( self, predState, predConvMat, sensorState, 
						uncertainty ): 

		H = np.eye( 3 )

		sensorState = sensorState.reshape( 3, 1 )
		sensorState[2,0] = sensorState[2,0] % (2*np.pi)
		predState[2,0] = predState[2,0] %(2*np.pi)

		try:
			S = np.linalg.inv( predConvMat + uncertainty )
		except np.linalg.LinAlgError:
			print "fuck"
			return predState.flatten( ).copy( ), predConvMat
		K = np.matmul( predConvMat, S )

		primeState = predState + np.matmul( K, sensorState - predState )
		primeConv = predConvMat - np.matmul( K, predConvMat )

		return primeState.flatten( ), primeConv

	def update( self, sensorState, odom, uncertainty, sensorUncertainty, convMat ):

		state = self.__state.reshape( 3, 1 ).copy()

		predState, predConv = self.predictionStep( state, convMat, odom, 1, uncertainty )

		primeState, primeConv = self.measurementUpdate( predState, predConv, sensorState, sensorUncertainty )

		self.__state = primeState.copy( )
		self.__conv = primeConv.copy( )

		return primeState, primeConv

	def getStateAndConv( self ):
		return self.__state.flatten().copy( ), self.__conv.copy( )