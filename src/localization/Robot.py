from visionManager.visionModule import KinematicModule

import numpy as np
import math
import time
import copy

from collections import OrderedDict, Sequence

kin = KinematicModule( )

def unpackSingleton( a ):
	if hasattr( a, 'ndim' ):
		return a[ (0,)*a.ndim ]

	return a

class Robot( object ):
	
	CACHE_findEffL = OrderedDict( )
	cacheMaxSide = 30

	CUMTIME = [0]

	def __init__( self, x, y, theta, camera, forwardkinematic = None, nScanLine = 8, ID = 0 ):

		self.__x = x
		self.__y = y
		self.__theta = theta

		self.__pan = math.radians( 0.0 )
		self.__tilt = math.radians( 0.0 )

		self._id = ID

		self.__camera = camera

		if forwardkinematic is not None:
			self.__forwardKinFunc = forwardkinematic
		else:
			self.__forwardKinFunc = self.defaultForwardKin

		self.__nScanLine = nScanLine
		self.__scanLine = {}
		self._createScanLine( )

	def setID( self, ID ):
		self._id = ID

	@property
	def ID( self ):
		return self._id

	def split( self, sigmaR=1.0, sigmaTheta=np.pi/100, checkingFunc = lambda x,y : True ):

		valid = False
		while not valid:
			r = np.random.randn( 1 )[0] * sigmaR
			w = np.random.uniform( low=-2*np.pi, high=2*np.pi )

			dTheta = np.random.randn( 1 )[0] * sigmaTheta

			dx = r * math.cos( w )
			dy = r * math.sin( w )

			valid = checkingFunc( self.__x + dx, self.__y + dy )

		return Robot( self.__x+dx, self.__y+dy, self.__theta+dTheta, 
					self.__camera.copy(), forwardkinematic = self.__forwardKinFunc,
					nScanLine = self.__nScanLine, ID = self.ID ) 

	@staticmethod
	def defaultForwardKin( pan, tilt ):
## NOTE : visittor : WTF is this function.
		h = kin.create_transformationMatrix( np.array([ 0, 0, 5 ]), 
									np.array([0, + tilt + (np.pi/2), -np.pi/2]),
									"zyz", )
		return h

	def findEffL( self, n ):

		fovX, fovY = self.__camera.getFieldOfView( )
		imgW, imgH = self.__camera.getImgSize( )

		arg = (n, self.__pan, self.__tilt, fovX, fovY, imgW, imgH)

		if self.CACHE_findEffL.has_key( arg ):
			return self.CACHE_findEffL[ arg ]

		else:
			out = self._findEffL( *arg )

			if len( self.CACHE_findEffL ) >= self.cacheMaxSide:
				self.CACHE_findEffL.popitem( last = False )

			self.CACHE_findEffL[ arg ] = out
			return out

	def _findEffL( self, n, pan, tilt, fovX, fovY, imgW, imgH ):

		points = []
		for i in xrange( n ):
			x = i*imgW / n
			points.append( [x, 0] )
			points.append( [x, imgH] )
		points = np.array( points )

		H = self.__forwardKinFunc( pan, tilt )

		points3D = self.__camera.getCoorFromPoint( points, H )

		startLenghtDict = {}
		stopLenghtDict = {}
		
		for i in range( 0, 2*n, 2 ):
			rad = -( fovX / 2.0 ) + i*( fovX / (2*float(n)) )
			startL = points3D[ i ] if points3D[i] is not None else np.array( [1000, 0] )
			stopL = points3D[ i+1 ] if points3D[i+1] is not None else np.array( [1000, 0] )

			startLenghtDict[rad] = np.linalg.norm(startL)
			stopLenghtDict[rad] = np.linalg.norm(stopL)

		return startLenghtDict, stopLenghtDict

	def _createScanLine( self ):
		
		fovX, fovY = self.__camera.getFieldOfView( )

		startLenghtDict, stopLenghtDict = self.findEffL( self.__nScanLine )

		for i in range( self.__nScanLine ):

			rad = -( fovX / 2.0 ) + i*( fovX / float(self.__nScanLine) )

			scanLine = ScanLine( (self.__x, self.__y), self.__pan + self.__theta + rad,
					startLenght = startLenghtDict[ rad ],
					endLenght = stopLenghtDict[ rad ] )

			self.__scanLine[ rad ] = scanLine

	def _moveScanLine( self ):

		startLenghtDict, stopLenghtDict = self.findEffL( self.__nScanLine )

		for key in self.__scanLine.keys():
			self.__scanLine[ key ].setPos( (self.__x, self.__y), 
									self.__pan + self.__theta + key )

			self.__scanLine[ key ].setStartAndEndLenght( startLenghtDict[key], stopLenghtDict[key] )

			self.__scanLine[ key ].moveToPosition( )

	def setBotPos( self, x, y, theta ):
		self.__x = x
		self.__y = y
		self.__theta = theta
		

	def setBotPosIncremental( self, dx, dy, dTheta ):


		dx_ = dx*math.cos(self.__theta) - dy*math.sin(self.__theta)
		dy_ = dx*math.sin(self.__theta) + dy*math.cos(self.__theta)

		self.__x += dx_
		self.__y += dy_
		self.__theta += dTheta
		self.__theta = self.__theta % (2*np.pi)


	def setPanTilt( self, pan, tilt ):

		self.__pan = pan
		self.__tilt = tilt

	def setPanTiltIncremental( self, dPan, dTilt ):
		self.__pan += dPan
		self.__tilt += dTilt


	def moveBot( self ):
		self._moveScanLine( )

	def getScanLineData( self, field ):
		tranMat = np.array([[self.__x],
							[self.__y]] )
		rotMat = np.array([ [math.cos(self.__theta),	math.sin(self.__theta)],
							[-math.sin(self.__theta),	math.cos(self.__theta) ] ])
		ranges = {}
		for rad, scanLine in self.__scanLine.items( ):
			points = np.array( field.solveScanLine( scanLine ) ).T
			if len( points ) == 0:
				# ranges[ rad ] = []
				continue

			points = np.matmul(rotMat, points) - np.matmul(rotMat, tranMat)
			# ranges[ rad ] = field.solveScanLine( scanLine )
			ranges[ rad ] = points.transpose( )

		return ranges

	def transformToWorldCoor( self, pointList ):

		if type( pointList ) == list:
			pointList = np.array( pointList )
			pointList = pointList if pointList.ndim == 2 else pointList.reshape( 1, 2 )

		tranMat = np.array( [[self.__x],
							 [self.__y]])
		rotMat = np.array([ [math.cos(self.__theta),	-math.sin(self.__theta)],
							[math.sin(self.__theta),	math.cos(self.__theta) ] ])

		pointList = np.array( pointList ).T
		pointList = np.matmul( rotMat, pointList ) + tranMat

		return pointList.T

	def transformToRobotCoor( self, pointList ):
		tranMat = np.array( [[self.__x],
							 [self.__y]])
		rotMat = np.array([ [math.cos(self.__theta),	-math.sin(self.__theta)],
							[math.sin(self.__theta),	math.cos(self.__theta) ] ])

		pointList = np.array( pointList ).T
		pointList = np.matmul(rotMat.T, pointList - tranMat)	

		return pointList

	def getPosition( self ):
		return self.__x, self.__y, self.__theta

	def getPanTiltPos( self ):
		return self.__pan, self.__tilt

	def getNScanLine( self ):
		return self.__nScanLine

	def getScanLine( self ):
		return self.__scanLine

class Camera( object ):

	def __init__( self, fx = 620, fy = 620, cx = 320, cy = 240,
				imgW = 640, imgH = 480 ):

		self.__fx = fx
		self.__fy = fy
		self.__cx = cx
		self.__cy = cy
		self.__imgW = imgW
		self.__imgH = imgH
		self.__fovX, self.__fovY = self._getFieldOfView( )

		self.__kinematicModule = KinematicModule( )
		self._setIntrinsicKinematic( )
		self.__kinematicModule.add_plane( 'ground', np.identity(4), 
					(-np.inf, np.inf), 
					(-np.inf, np.inf), 
					(-20, 20), )

	def copy( self ):
		return Camera( fx = self.__fx, fy = self.__fy, cx = self.__cx,
						cy = self.__cy, imgW = self.__imgW, imgH = self.__imgH )
 
	def _getFieldOfView( self ):
		fovX = 2 * np.arctan( 0.5 * self.__imgW / self.__fx )
		fovY = 2 * np.arctan( 0.5 * self.__imgH / self.__fy )
		return fovX, fovY

	def getFieldOfView( self ):
		return self.__fovX, self.__fovY

	def getImgSize( self ):
		return self.__imgW, self.__imgH

	def _setIntrinsicKinematic( self ):
		intrinMat = np.identity( 3 )
		intrinMat[0,0] = self.__fx
		intrinMat[1,1] = self.__fy
		intrinMat[0,2] = self.__cx
		intrinMat[1,2] = self.__cy

		self.__kinematicModule.set_IntrinsicCameraMatrix( intrinMat )

	def getCoorFromPoint( self, points, H = np.identity( 4 ) ):
		points3D = self.__kinematicModule.calculate3DCoor( points, H )
		return [ p[1] for p in points3D ]

	def setFX( self, fx ):
		self.__fx = fx
		self._setIntrinsicKinematic( )
		self.__fovX, self.__fovY = self._getFieldOfView( )

	def setFX( self, fy ):
		self.__fy = fy
		self._setIntrinsicKinematic( )
		self.__fovX, self.__fovY = self._getFieldOfView( )

	def setFX( self, cx ):
		self.__cx = cx
		self._setIntrinsicKinematic( )
		self.__fovX, self.__fovY = self._getFieldOfView( )

	def setFX( self, cy ):
		self.__cy = cy
		self._setIntriysicKinematic( )
		self.__fovX, self.__fovY = self._getFieldOfView( )

class ScanLine( object ):

	def __init__( self, origin, theta, startLenght = 100, endLenght = 200 ):

		self._origin = origin
		self._theta = theta
		self._startLenght = startLenght
		self._endLenght = endLenght

		self.setPos( origin, theta )
		
		self._startPoint = None
		self._endPoint = None

		self.setStartAndEndLenght( startLenght, endLenght )

		self.moveToPosition( )

	def setPos( self, origin, theta ):
		self._theta = unpackSingleton( theta )
		self._origin = unpackSingleton( origin[0] ), unpackSingleton( origin[1] )
		# self._calStartAndEndPoint( )

	def setStartAndEndLenght( self, startLenght, endLenght ):

		try:
			startLenght = startLenght[0]
		except:
			pass

		try:
			endLenght = endLenght[0]
		except:
			pass

		if hasattr(startLenght, 'ndim'):
			startLenght = startLenght[ (0,)*startLenght.ndim ]

		if hasattr(endLenght, 'ndim' ):
			endLenght = endLenght[ (0,)*endLenght.ndim ]

		self._startLenght = startLenght
		self._endLenght = endLenght
		# self._calStartAndEndPoint( )

	def moveToPosition( self ):
		self._calStartAndEndPoint( )

	def _calStartAndEndPoint( self ):
		x,y = self._origin

		x1 = x + (self._startLenght * math.cos( self._theta ))
		y1 = y + (self._startLenght * math.sin( self._theta ))

		x2 = x + (self._endLenght * math.cos( self._theta ))
		y2 = y + (self._endLenght * math.sin( self._theta ))

		try:
			_ = x1[0], y1[0], x2[0], y2[0]
			print x1, x2
			print self._startLenght, self._endLenght, self._origin
			print "###################################"
			print "IS A SEQUENCE"
			print "####################################"
		except:
			pass

		self._startPoint = ( x1, y1 )
		self._endPoint = ( x2, y2 )

	@property
	def startPoint( self ):
		return self._startPoint

	@property
	def endPoint( self ):
		return self._endPoint

	@property
	def startLenght( self ):
		return self._startLenght

	@property
	def endLenght( self ):
		return self._endLenght

	def getPose( self ):
		return self._origin[0], self._origin[1], self._theta

