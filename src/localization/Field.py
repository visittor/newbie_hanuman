import numpy as np
import cv2
import math

import matplotlib.pyplot as plt

from scipy.ndimage import affine_transform

class AbstractFieldLineClass( object ):

	def solveScanLine( self, scanLine ):
		raise NotImplementedError

	def findClosestDistance( self, x, y ):
		raise NotImplementedError

	def draw( self, img, changeCoorFunc = lambda x,y : (x,y), changeUnitFunc = lambda x: x ):
		pass

class AbstractLandmark( object ):

	def __init__( self, name, position ):
		self._name = name
		self._position = position

	def draw( self, img, changeCoorFunc = lambda x,y: (x,y), changeUnitFunc = lambda x: x ):
		x,y = changeCoorFunc( *self._position )
		cv2.circle( img, (int(x),int(y)), 15, (0,255,255), -1 )

	def getPosPolar( self, particle ):
		robotPos = particle.getPosition()
		phi = math.atan2( self._position[1]-robotPos[1], self._position[0]-robotPos[0] ) - robotPos[2]
		d = math.sqrt( (robotPos[1]-self._position[1])**2 + (robotPos[0]-self._position[0])**2 )

		return d, phi

	def scoringFunction( self, posFromSensor, particle, sigmaD , sigmaPhi, closingPolygon=None ):
		worldPos_sensor = particle.transformToWorldCoor( posFromSensor ).flatten()
		robotPos = particle.getPosition()[:2]

		dx = posFromSensor[0]
		dy = posFromSensor[1]

		distance = math.sqrt( dx**2 + dy**2 )

		phi_sensor = math.atan2( dy, dx )
		# phi_sim = math.atan2( robotPos[1]-self._position[1], robotPos[0]-self._position[0] )
		d_sim, phi_sim = self.getPosPolar( particle )

		prob_distance = self.getNormalizeProbFromDistance( distance - d_sim, sigma = sigmaD )
		prob_angle = self.getNormalizeProbFromDistance( phi_sensor-phi_sim, sigma = sigmaPhi)

		return prob_distance*prob_angle

	def getProbabilityFromDistance( self, d, sigma = None ):
		sigma = sigma if sigma is not None else self._likelihood_sigma

		if type( d ) == float:

			prob = math.exp( -(d**2) / (2*sigma**2) ) / (math.sqrt(2*math.pi)*sigma)

		else:

			prob = np.exp( -(d**2) / (2*sigma**2) ) / (math.sqrt(2*math.pi)*sigma)

		return prob

	def getNormalizeProbFromDistance( self, d, sigma = None ):

		prob = self.getProbabilityFromDistance( d, sigma )
		maxProb = self.getProbabilityFromDistance( 0, sigma )

		return prob / maxProb

	@property
	def name( self ):
		return self._name

	@property
	def position( self ):
		return self._position

class FieldLine( AbstractFieldLineClass ):

	def __init__( self, start, end ):
		'''
		arguments:
			start 	:	( List of tuples or list of lists)
		'''
		self._lineArray = np.hstack( ( start, end ) )

	def getStartAndEndPoint( self ):
		return self._lineArray

	def _findLineIntersection_parametric( self, scanLine ):
		# This is how I solve line intersection using parametric form.
		# X = t1 * x1 + ( 1 - t1 ) * x2
		# Y = t1 * y1 + ( 1 - t1 ) * y2

		# X = t2 * x3 + ( 1 - t2 ) * x4
		# Y = t2 * y3 + ( 1 - t2 ) * y4

		# t1 * x1 + ( 1 - t1 ) * x2 = t2 * x3 + ( 1 - t2 ) * x4
		# ( x1 - x2 ) * t1 + x2 = ( x3 - x4 ) * t2 + x4
		# ( x1 - x2 ) * t1 - ( x3 - x4 ) * t2 = x4 - x2

		# t1 * y1 + ( 1 - t1 ) * y2 = t2 * y3 + ( 1 - t2 ) * y4
		# ( y1 - y2 ) * t1 + y2 = ( y3 - y4 ) * t2 + y4
		# ( y1 - y2 ) * t1 - ( y3 - y4 ) * t2 = y4 - y2

		# |(x1-x2)	(x4-x3)||t1|	|x4-x2|
		# |				   ||  | = 	|	  |
		# |(y1-y2)	(y4-y3)||t2|	|y4-y2|
		diffX1X2 = scanLine.startPoint[0] - scanLine.endPoint[0]
		diffY1Y2 = scanLine.startPoint[1] - scanLine.endPoint[1]

		col2 = self._lineArray[:, 2:] - self._lineArray[:, :2]
		col2 = col2.reshape( self.nLine, 2, 1) 

		col1 = np.repeat(np.array( [[ [diffX1X2], [diffY1Y2] ]] ), self.nLine, axis = 0 )
		a = np.concatenate( (col1, col2), axis = 2 )

		x2y2 = np.array([[ scanLine.endPoint[0], scanLine.endPoint[1] ]])
		b = self._lineArray[:,2:] - x2y2
		b = b.reshape(-1, 2, 1 )

## TODO : Find a better way to handle this. np.linalg.lstsq should be use in this case.
		try:
			T = np.linalg.solve( a, b )
		except np.linalg.linalg.LinAlgError as e:
## NOTE : Visittor : not neccessary to warn anyone.
			# rospy.logdebug("warning : try to solve sigular matrix")
			return

		return T.reshape( -1, 2 )

	def _selectValidLine_parametric( self, T ):
		## This function check if intersection is on both lines or not.
		ret1 = np.logical_and(1>=T[:,0], T[:,0]>=0)
		ret2 = np.logical_and(1>=T[:,1], T[:,1]>=0)
		ret = np.logical_and( ret1, ret2 )

		return T[ret, :].copy( )

	def _transformParametricToCart( self, T, scanLine ):
		## Transform t --> x,y
		x = ( scanLine.startPoint[0] - scanLine.endPoint[0] ) * T[:,0] + scanLine.endPoint[0]
		y = ( scanLine.startPoint[1] - scanLine.endPoint[1] ) * T[:,0] + scanLine.endPoint[1]

		return np.vstack( (x,y) ).T

	def solveScanLine( self, scanLine ):
		## Find intersection between scanline and field line.
		T = self._findLineIntersection_parametric( scanLine )

		if T is None:
			return []

		Tnew = self._selectValidLine_parametric( T )

		return self._transformParametricToCart( Tnew, scanLine )

	def findClosestDistance( self, x, y ):
		## Find the closest distance from point, (x,y), each lines in list.
		## Return the closest one.
		#                    o(x,y)  
		#                   /| \
		#    vector 0to2<--/ |  \-->vector 0to1
		#                 /  |   \ 
		#         (start)o........o(end)
		#        .           ^
		#				     vector 1to2
		#
		#
		#                 
		vec_x1 = self._lineArray[:, 2:]
		vec_x2 = self._lineArray[:, :2]
		vec_x0 = np.repeat( np.array( [[x,y]] ), self.nLine, axis = 0 )

		vec_0to1 = vec_x1 - vec_x0
		vec_0to2 = vec_x2 - vec_x0
		vec_1to2 = vec_x2 - vec_x1

		# find unit vector which perpendicular to a line.
		v = vec_x2 - vec_x1
		v[:,0] *= -1
		v[:,:] = v[:,::-1]
		v /= np.sqrt( v[:,0:1]**2 + v[:,1:]**2 )

		# find vector from point to start point.
		r = vec_x1 - vec_x0

		# Distance is r dot v
		d_perpendicular = np.sum(v*r, axis = 1)

		## check if ppoint (x,y) place between start and end points of a line.
		
		# check an angle between two vector (whether it larger or lesser than 90)
		angle_012 = np.sum(vec_0to1*vec_1to2, axis = 1)
		angle_022 = np.sum(vec_0to2*vec_1to2, axis = 1)

		d_perpendicular = np.absolute( d_perpendicular )

		d_x1 = np.linalg.norm( vec_x0 - vec_x1, axis = 1 )

		d_x2 = np.linalg.norm( vec_x0 - vec_x2, axis = 1 )

		d = np.zeros( d_perpendicular.shape )

		# if both angle are lesser or both angles are larger than 90 degrees, point(x,y)
		# is place outside a line.
		ret_perpendicular = angle_012*angle_022 <= 0
		ret_other = np.logical_not( ret_perpendicular )
		d[ ret_perpendicular ] = d_perpendicular[ret_perpendicular]

		# if point(x,y) is outside a line, find distance from point to line end point (or start point).
		d[ ret_other ] = np.minimum( d_x1[ ret_other ], d_x2[ ret_other ] ) 

		return min( np.absolute(d) )

	def draw( self, img, changeCoorFunc = lambda x,y : (x,y), changeUnitFunc = lambda x: x ):
		for x1, y1, x2, y2 in self._lineArray:
			x1, y1 = changeCoorFunc( x1, y1 )
			x2, y2 = changeCoorFunc( x2, y2 )

			cv2.line( img, (x1,y1), (x2,y2), (255,255,255), 3 )

	@property
	def nLine( self ):
		return self._lineArray.shape[0]

class FieldBoundary( FieldLine ):

	# def _selectValidLine_parametric( self, T ):
	# 	ret = np.logical_and(1>=T[:,1], T[:,1]>=0)

	# 	Tnew = T[ret, :].copy( )

	# 	Tnew[:,0] = np.where(  Tnew[:,0] > 1, Tnew[:,0], 1 )

	# 	return T[ret, :].copy( )

	def draw( self, img, changeCoorFunc = lambda x,y : (x,y), changeUnitFunc = lambda x: x ):
		for x1, y1, x2, y2 in self._lineArray:
			x1, y1 = changeCoorFunc( x1, y1 )
			x2, y2 = changeCoorFunc( x2, y2 )

			cv2.line( img, (x1,y1), (x2,y2), (0,127,0), 3 )

class CircleFieldLine( AbstractFieldLineClass ):

	def __init__( self, x, y, r ):
		self.__x = x
		self.__y = y
		self.__r = r

	def solveScanLine( self, scanLine ):
		# X = (x1 - x2) * t1 + x2 = f*t1 + x2
		# Y = (y1 - y2) * t1 + y2 = g*t1 + y2
		# 0 = (X - xc)^2 + (Y - yc)^2 - r^2
		# t = f*(xc-x2) + g*(yc-y2) +/- sqrt(r^2*(f^2+g^2) - ( f*(yc-y2) - g*(xc-x2) )^2) / (f^2 + g^2)

		f = float( scanLine.startPoint[0] - scanLine.endPoint[0] )
		g = float( scanLine.startPoint[1] - scanLine.endPoint[1] )
		x2 = float( scanLine.endPoint[0] )
		y2 = float( scanLine.endPoint[1] )
		xc = float( self.__x )
		yc = float( self.__y )
		r = float( self.__r )

		inRoot = r**2*(f**2 + g**2) - ( f*(yc-y2) - g*(xc-x2) )**2

		if f == 0 and g == 0:
			return []

		elif inRoot == 0:
			t = ( f*(xc-x2) + g*(yc-y2) ) / (f**2 + g**2)

			t = [t]

		elif inRoot > 0:
			t1 = ( f*(xc-x2) + g*(yc-y2) + math.sqrt(inRoot) ) / (f**2 + g**2)
			t2 = ( f*(xc-x2) + g*(yc-y2) - math.sqrt(inRoot) ) / (f**2 + g**2)
			t = [t1,t2]

		else:
			t = []

		coor = []
		for tt in t:
			if 0<=tt<=1:
				coor.append( np.array(( f*tt + x2, g*tt + y2 )) )
		return coor

	def findClosestDistance( self, x, y ):

		d_pointToCenter = math.sqrt( (x-self.__x)**2 + (y-self.__y)**2 )

		return math.fabs(d_pointToCenter - self.__r)

	def draw( self, img, changeCoorFunc = lambda x,y : (x,y), changeUnitFunc = lambda x: x ):

		x,y = changeCoorFunc( self.__x, self.__y )
		r = changeUnitFunc( self.__r )
		cv2.circle( img, (x,y), r, (255,255,255), 3 )

	@property
	def x( self ):
		return self.__x

	@property
	def y( self ):
		return self.__y

	@property
	def r( self ):
		return self.__r

class FieldCorner( AbstractLandmark ):

	def __init__( self, position ):

		super( FieldCorner, self ).__init__( 'field_corner', position )

	def draw( self, img, changeCoorFunc = lambda x,y: (x,y), changeUnitFunc = lambda x: x ):
		x,y = changeCoorFunc( *self._position )
		cv2.circle( img, (int(x),int(y)), 15, (255,0,255), -1 )

class CircleLandmark( AbstractLandmark ):

	def __init__( self, position ):

		super( CircleLandmark, self ).__init__( 'circle', position )

	def draw( self, img, changeCoorFunc = lambda x,y: (x,y), changeUnitFunc = lambda x: x ):
		x,y = changeCoorFunc( *self._position )
		cv2.circle( img, (int(x),int(y)), 15, (255,255,0), -1 )

class Field( object ):

	def __init__( self, fieldLineList, lowerX = -5.2, upperX = 5.2,
					lowerY = -3.7, upperY = 3.7, scanLineMaxRange = 7.0, uniformWeight = 0.02 ):

		self.__lowerX = lowerX
		self.__lowerY = lowerY

		self.__upperX = upperX
		self.__upperY = upperY

		# boundaryCoor = [ 	(lowerX, upperY, upperX, upperY),
		# 					(upperX, upperY, upperX, lowerY),
		# 					(upperX, lowerY, lowerX, lowerY),
		# 					(lowerX, lowerY, lowerX, upperY) ]

		# fieldBoundary = FieldBoundary(  [ p[:2] for p in boundaryCoor ],
		# 								[ p[2:] for p in boundaryCoor ] )

		# fieldLineList.append( fieldBoundary )

		self._fieldLineList = fieldLineList

		self._landmarkList = []

		## Every field have field corner.
		self.addLandmark( FieldCorner( (lowerX, lowerY) ) )
		self.addLandmark( FieldCorner( (lowerX, upperY) ) )
		self.addLandmark( FieldCorner( (upperX, lowerY) ) )
		self.addLandmark( FieldCorner( (upperX, upperY) ) )

		## Precompute map
		self._distanceMap = None
		self._likelihoodField = None
		self._gradientX = None
		self._gradientY = None

		## 
		self._likelihood_sigma = 0.1
		self._scanLineMaxRange = scanLineMaxRange

		self._uniformWeight = uniformWeight
		self._normalWeight = 1.0 - uniformWeight

		self._mapPaddingSize = 0.0

	def addFieldLine( self, fieldLine ):
		self._fieldLineList.append( fieldLine )

	def addLandmark( self, landmark ):

		assert isinstance( landmark, AbstractLandmark )

		self._landmarkList.append( landmark )

	def getLandmarkPosition( self, name ):

		return [ l.position for l in self._landmarkList if l.name == name ]

	def getAllLandmarkPosition( self ):
		landmarkDict = {}

		for l in self._landmarkList:
			if not landmarkDict.has_key( l.name ):
				landmarkDict[ l.name ] = [l.position]
				continue

			landmarkDict[ l.name ].append( l.position )

		return landmarkDict	

	def drawFieldLine( self, img, changeCoorFunc = lambda x,y : (x,y), changeUnitFunc = lambda x: x ):

		for fieldLine in self._fieldLineList:
			fieldLine.draw( img, changeCoorFunc = changeCoorFunc, changeUnitFunc = changeUnitFunc )

		for landmark in self._landmarkList:
			landmark.draw( img, changeCoorFunc = changeCoorFunc, changeUnitFunc = changeUnitFunc )

	def solveScanLine( self, scanLine ):

		points = []

		for fieldLine in self._fieldLineList:

			for pp in fieldLine.solveScanLine( scanLine ):
				points.append( pp )

		return points

	def findClosestDistance( self, x, y ):

		distance = []

		for fieldLine in self._fieldLineList:
			distance.append( fieldLine.findClosestDistance( x, y ) )

		return min( distance )

	def createDistanceMap( self, paddingSize = 0 ):
		'''
		To create precompute distance map. value on a map will be closest distance to 
		field line
		optional:
			paddingSize : padding size of distance map.
		'''
		## Create precompute distance map.
		self._mapPaddingSize = paddingSize

		## One pixel of distance map will be 1x1 cm.
		lowerX = int( (self.lowerX - paddingSize) * 100 )
		lowerY = int( (self.lowerY - paddingSize) * 100 )
		upperX = int( (self.upperX + paddingSize) * 100 )
		upperY = int( (self.upperY + paddingSize) * 100 )

		width = upperX - lowerX
		height = upperY - lowerY

		self._distanceMap = np.zeros( ( width, height ) )

		for i, x in enumerate( xrange( lowerX, upperX ) ):
			for j, y in enumerate( xrange( lowerY, upperY ) ):

				x_true = float( x ) / 100
				y_true = float( y ) / 100

				d = self.findClosestDistance( x_true, y_true )

				print "{} at ({}, {})".format( d, x, y )

				self._distanceMap[ i, j ] = d

		plt.imshow( self._distanceMap )
		plt.show( )

	def createLikelihoodField( self, sigma ):
		dSquare = np.power( self._distanceMap, 2 )
		self._likelihoodField = self.getProbabilityFromDistance( self._distanceMap, sigma )

		self._maxLikelihood = self.getNormalizeProbFromDistance( 0, sigma )

		self._likelihood_sigma = sigma

	def createGradientField( self ):

		distMapPad = np.pad( self._distanceMap, 1, 'reflect' )

		kernelX = np.array( [	[-1,-2,-1],
								[ 0, 0, 0],
								[ 1, 2, 1]] )
		kernelY = np.array( [	[-1, 0, 1],
								[-2, 0, 2],
								[-1, 0, 1]] )

		self._gradientX = cv2.filter2D( self._distanceMap, -1, kernelX )
		self._gradientY = cv2.filter2D( self._distanceMap, -1, kernelY )

	def compareLandmark( self, name, particle, posFromSensor, confidence, sigmaD = 0.5, sigmaPhi = np.pi/10 ):

		maxScore = 0
		for landmark in self._landmarkList:
			if landmark.name != name:
				continue

			score = landmark.scoringFunction( posFromSensor, particle, sigmaD, sigmaPhi )*confidence

			if score > maxScore:
				maxScore = score

		return maxScore if maxScore > 0 else self._uniformWeight * ( 1 / self._scanLineMaxRange )

	def getClosestLandmark( self, name, particle, posFromSensor ):
		maxScore = -1
		selectedLandmark = None
		for landmark in self._landmarkList:
			if landmark.name != name:
				continue

			score = landmark.scoringFunction( posFromSensor, particle, 1, 1 )

			if score > maxScore:
				selectedLandmark = landmark
				maxScore = score

		return selectedLandmark

	def getDistanceMap( self ):
		return self._distanceMap

	def getDistanceMap_robotView( self, robot ):

		'''

		'''

		x,y,theta = robot.getPosition( )

		x *= 100
		y *= 100

		moveCenter = np.array( [[1, 0, self._distanceMap.shape[0] / 2],
								[0, 1, self._distanceMap.shape[1] / 2],
								[0, 0, 1] ] )

		moveCenterInv = np.array( [ [1, 0, -self._distanceMap.shape[0] / 2],
									[0, 1, -self._distanceMap.shape[1] / 2],
									[0, 0, 1] ] )

		rotation = np.array([ 	[math.cos(theta),	math.sin(theta),	0],
								[-math.sin(theta),	math.cos(theta),	0],
								[0,					0,					1] ])
		translation = np.array( [ 	[1, 0, x],
									[0, 1, y],
									[0, 0, 1] ])

		tranMat = np.matmul( rotation, translation )

		matrix = np.matmul( np.matmul( moveCenter, tranMat ), moveCenterInv )
		matrix = np.linalg.inv( matrix )

		return affine_transform(self._distanceMap, 
							matrix, offset=0.0, 
							output_shape=None, 
							output=None, order=3, 
							mode='nearest', cval=0.0, prefilter=True)

	def getLikelihoodField( self, sigma = None ):
		assert self._distanceMap is not None, "Distance must be created first before call this function."

		if self._likelihoodField is None:
			sigma = self._likelihood_sigma if sigma is None else sigma
			self.createLikelihoodField( sigma )
			return self._likelihoodField

		return self._likelihoodField

	def getPadding( self ):
		return self._mapPaddingSize

	def saveDistanceMap( self, fn ):
		if self._distanceMap is None:
			print "Map have not been created."
			return

		np.savez( fn, map = self._distanceMap, padding_size = self._mapPaddingSize )
		print "Save map"

	def getMapIndexFromMetres( self, x, y ):

		x -= self.lowerX - self._mapPaddingSize
		y -= self.lowerY - self._mapPaddingSize

		x_cm = x * 100
		y_cm = y * 100

		x_index = int( x_cm )
		y_index = int( y_cm )

		return x_index, y_index

	def getGradientVecFromMetres( self, x, y ):

		assert self._gradientX is not None, 'gredient field must be created before call this function.'

		x_index, y_index = self.getMapIndexFromMetres( x, y )

		x_index = max( 0, min( x_index, self._gradientX.shape[0]-1 ) )
		y_index = max( 0, min( y_index, self._gradientX.shape[1]-1 ) )

		return self._gradientX[x_index, y_index], self._gradientY[x_index, y_index]

	def getLikelihoodValFromMetres( self, x, y ):

		assert self._likelihoodField is not None, "likelihood field must be created before call this function."

		x_index, y_index = self.getMapIndexFromMetres( x, y )

		if 0 <= x_index < self._likelihoodField.shape[0] and 0 <= y_index < self._likelihoodField.shape[1]:
			return self._likelihoodField[ x_index, y_index ]

		else:
## TODO : visittor : find a better way to compute prob in this case.
			return self._uniformWeight * ( 1.0 / self._scanLineMaxRange )

	def getDistanceValToBoundary( self, x, y ):

		dx = max( self.lowerX - x, 0, x - self.upperX )
		dy = max( self.lowerY - y, 0, y - self.upperY )

		sign = 1

		if dx == 0 and dy == 0:
			dx = min( x - self.lowerX, self.upperX - x )
			dy = min( y - self.lowerY, self.upperY - y )

			sign = -1

		return sign * math.sqrt( dx*dx + dy*dy )

	def getLikelihoodValToBoundary( self, x, y ):

		d = self.getDistanceValToBoundary( x, y )

		prob = self.getNormalizeProbFromDistance( d, self._likelihood_sigma )
		# print d, ":", prob
		return prob

	def getProbabilityFromDistance( self, d, sigma = None ):
		sigma = sigma if sigma is not None else self._likelihood_sigma

		if type( d ) == float:

			prob_normal = math.exp( -(d**2) / (2*sigma**2) ) / (math.sqrt(2*math.pi)*sigma)

		else:

			prob_normal = np.exp( -(d**2) / (2*sigma**2) ) / (math.sqrt(2*math.pi)*sigma)

		prob = self._normalWeight*prob_normal + self._uniformWeight*( 1.0 / self._scanLineMaxRange ) 

		return prob

	def getNormalizeProbFromDistance( self, d, sigma = None ):

		prob = self.getProbabilityFromDistance( d, sigma )
		maxProb = self.getProbabilityFromDistance( 0, sigma )

		return prob / maxProb

	def getDistanceValFromMetres( self, x, y ):

		assert self._distanceMap is not None, "distance map should be loaded or created before call this function."

		x_index, y_index = self.getMapIndexFromMetres( x, y )

		if 0 <= x_index < self._distanceMap.shape[0] and 0 <= y_index < self._distanceMap.shape[1]:
			return self._distanceMap[ x_index, y_index ]

		else:
			return 10

	def loadDistanceMap( self, fn ):
		map_data = np.load( fn )

		try:
			self._distanceMap = map_data[ 'map' ].astype( np.float64 )
			self._mapPaddingSize = map_data[ 'padding_size' ]
		except:
			raise RuntimeError( "May be this not a map for this module." )
		
	@property
	def lowerX( self ):
		return self.__lowerX

	@property
	def upperX( self ):
		return self.__upperX

	@property
	def lowerY( self ):
		return self.__lowerY

	@property
	def upperY( self ):
		return self.__upperY

	@property
	def likelihood_sigma( self ):
		return self._likelihood_sigma