from Painter import Painter

from Robot import Robot, Camera
from Field import Field, FieldLine, CircleFieldLine, FieldBoundary, AbstractLandmark
from Painter import Painter

from geometry_msgs.msg import Pose2D
from newbie_hanuman.msg import localizationMsg

from cell.nodeCell import NodeBase

import numpy as np
import rospy

class Monitor( NodeBase ):

	def __init__( self ):
		super( Monitor, self ).__init__( "LocalizationMonitor" )
		
		self.rosInitNode( )

		self.rosInitSubscriber("/localization/robot_pose",
						Pose2D,
						self.getRobotPose
						)

		self.rosInitSubscriber("/localization/scan_line",
								localizationMsg,
								self.getActualSensorDataCallback )

		self._robot = Robot( 0, 0, 0, Camera(), forwardkinematic=None, nScanLine=8, ID=0 )

		mapFN = "/".join(__file__.split( '/' )[:-1] + ['map.npz'])

		self._field = self.createMap( mapFN )

		self._painter = Painter( imgW = 1500, imgH = 1000, trackerName = ('x', 'y', 'w') )
		self._painter.setFieldCoor( self._field )

		self._actualSensorPoint = []
		self._boundaryPoint = []
		self._landmark = []

		self.setFrequency( 10 )

	def createMap( self, mapFN ):
		field = Field( [], lowerX = -5.2, upperX = 5.2,
							lowerY = -3.7, upperY = 3.7 )

		field.loadDistanceMap( mapFN )

		field.addLandmark( AbstractLandmark( 'circle', (0,0) ) )
		
		field.addLandmark( AbstractLandmark( 'goal', (-4.5,  0.85 ) ) )
		field.addLandmark( AbstractLandmark( 'goal', (-4.5, -0.85 ) ) )
		field.addLandmark( AbstractLandmark( 'goal', ( 4.5,  0.85 ) ) )
		field.addLandmark( AbstractLandmark( 'goal', ( 4.5, -0.85 ) ) )

		return field

	def getRobotPose( self, msg ):

		self._robot.setBotPos( msg.x, msg.y, msg.theta )

	def getActualSensorDataCallback( self, msg ):

		js = msg.pointClound.jointState

		pan = js.position[ js.name.index( "pan" ) ]
		tilt = js.position[ js.name.index( "tilt" ) ]

		self.__currentPanTilt = [ pan, tilt ]

		max_range = msg.pointClound.max_range

		pointArray = np.array([ [p.x, p.y] for p in msg.pointClound.points ])
## TODO : visittor : Don't hard code scan line max range.
		self._actualSensorPoint = pointArray[ : msg.pointClound.splitting_index[0] ]
		if len( self._actualSensorPoint ) != 0:
			includeIdx = (self._actualSensorPoint[:,0]**2) + (self._actualSensorPoint[:,1]**2)<max_range**2
			self._actualSensorPoint = self._actualSensorPoint[ includeIdx ]

		self._boundaryPoint = pointArray[ msg.pointClound.splitting_index[0] : ]
		if len( self._boundaryPoint ) != 0:
			includeIdx = (self._boundaryPoint[:,0]**2) + (self._boundaryPoint[:,1]**2)<max_range**2
			self._boundaryPoint = self._boundaryPoint[ includeIdx ]

		self._landmark = [ (k,[v.x,v.y],c) for k,v,c in zip( msg.landmark.names, msg.landmark.pose, msg.landmark.confidences ) ]

	def visualize( self ):

		actualPoint = self._actualSensorPoint
		boundary = self._boundaryPoint
		landmarkPos = [ l[1] for l in self._landmark ]
		landmarkName = [ l[0] for l in self._landmark ]

		self._painter.drawField( )
		
		if len( actualPoint ) != 0:
			pointWorldCoor = self._robot.transformToWorldCoor( actualPoint )

			self._painter.drawPoints( pointWorldCoor )

		if len( boundary ) != 0:
			pointWorldCoor = self._robot.transformToWorldCoor( boundary )

			self._painter.drawPoints( pointWorldCoor, color = (255,0,0) )

		if len( landmarkPos ) != 0:
			pointWorldCoor = self._robot.transformToWorldCoor( landmarkPos )

			for p, n in zip(pointWorldCoor, landmarkName):

				if n == 'field_corner':
					color = (255, 0, 255)
				elif n == 'goal':
					color = (0, 255, 255)
				elif n == 'circle':
					color = (255,255,0)
				else:
					color = (0,0,0)

				self._painter.drawPoints( [p], color = color, size = 15 )

		self._painter.drawRobot2( self._robot, circleColor = (0,0,255) )

		self._painter.plotTrackData( list(self._robot.getPosition()) )

		self._painter.show( )

	def run( self ):
		rospy.loginfo( "Start Localization Monitor." )

		while not rospy.is_shutdown( ):
			self.visualize( )
			self.sleep( )


	def end( self ):
		rospy.loginfo( "End Localization Monitor." )

def main( ):

	node = Monitor( )

	try:
		node.run( )
	except rospy.ROSInterruptException as e:
		pass
	finally:
		node.end( )