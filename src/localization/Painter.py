import numpy as np 
import cv2
import matplotlib.pyplot as plt 
import matplotlib.animation as animation

class Painter( object ):

	def __init__( self, imgW = 1000, imgH = 1000, trackerName = None ):
		self.img = np.zeros( (imgH, imgW, 3), dtype = np.uint8 )

		self.top = 0
		self.bot = 1000
		self.left = 0
		self.right = 1000

		self._field = None

		self._fig = plt.figure( )
		self._ax = self._fig.add_axes( [0.0, 0.0, 0.7, 0.7] )
		self._axHis = self._fig.add_axes( [0.75, 0.75, 0.2, 0.2] )
		self._axPlot = self._fig.add_axes( [0.75, 0.30, 0.25, 0.4] )
		self._im = self._ax.imshow( self.img )
		# self._fig.show( )
		nTracker = 0 if trackerName is None else len( trackerName )
		self._trackerName = trackerName
		self._trackData = [[0] for i in range( nTracker )]
		# Writer = animation.writers['ffmpeg_file']
		# self._writer = Writer(fps=5, metadata=dict(artist='Me'), bitrate=1800)
		# self._writer.setup( self._fig, 'out.mp4', dpi = 500 )

	def setFieldCoor( self, field ):
		xMin, yMin = self.getImCoor( field.lowerX, field.lowerY )
		xMax, yMax = self.getImCoor( field.upperX, field.upperY )

		self.top = min(yMin, yMax)
		self.bot = max(yMax, yMin)
		self.left = xMin
		self.right = xMax

		self._field = field

	def changeMetreToImgUnit( self, value ):
		try:
			return int(value * 100)
		except ValueError:
			return 1000

	def getImCoor( self, x, y ):
		x = self.changeMetreToImgUnit( x )
		y = self.changeMetreToImgUnit( y )
		x_ = int( self.img.shape[1]/2 + x )
		y_ = int( self.img.shape[0]/2 - y )

		return  x_, y_ 

	def drawField( self ):
		self.img[:,:,:] = 0

		self.img[self.top:self.bot, self.left:self.right, 1] = 255

		if self._field is not None:
			self._field.drawFieldLine( self.img, changeCoorFunc = self.getImCoor,
									changeUnitFunc = self.changeMetreToImgUnit )

			likelihood = self._field.getLikelihoodField( ).T
			paddingSize = self._field.getPadding( )

			if likelihood.shape[0] > self.img.shape[0]:
				top_likelihood = (likelihood.shape[0] - self.img.shape[0]) / 2
				bot_likelihood = (likelihood.shape[0] + self.img.shape[0]) / 2

				top_img = 0
				bot_img = self.img.shape[0]

			else:
				top_img = (self.img.shape[0] - likelihood.shape[0])/2
				bot_img = (self.img.shape[0] + likelihood.shape[0])/2

				top_likelihood = 0
				bot_likelihood = likelihood.shape[0]

			if likelihood.shape[1] > self.img.shape[1]:
				left_likelihood = (likelihood.shape[1] - self.img.shape[1]) / 2
				right_likelihood = (likelihood.shape[1] + self.img.shape[1]) / 2

				left_img = 0
				right_img = self.img.shape[1]

			else:
				left_img = (self.img.shape[1] - likelihood.shape[1])/2
				right_img = (self.img.shape[1] + likelihood.shape[1])/2				

				left_likelihood = 0
				right_likelihood = likelihood.shape[1]

			top = self.top - int(paddingSize * 100)
			bot = self.bot + int(paddingSize * 100)
			left = self.left - int(paddingSize * 100)
			right = self.right + int(paddingSize * 100)

			self.img[top_img:bot_img, left_img:right_img, 0] = \
			(likelihood[top_likelihood:bot_likelihood, left_likelihood:right_likelihood]*255).astype( np.uint8 )


	def drawRobot( self, robot, color = (255, 0, 255), drawScanline = True ):

		colorCircle = tuple( max( 0, c - 50) for c in color )

		if drawScanline:
			self.drawScanLine( robot.getScanLine( ) )

		xWorld, yWorld, theta = robot.getPosition()

		start = ( xWorld, yWorld )

		stop = ( xWorld + 0.2*np.cos(theta), yWorld + 0.2*np.sin(theta) )

		start = self.getImCoor( *start )
		stop = self.getImCoor( *stop )

		cv2.arrowedLine( self.img, start, stop, color, 3, tipLength = 0.3 )
		cv2.circle( self.img, start, 4, colorCircle, -1 )

	def drawRobot2( self, robot, circleColor = (0,0,255) ):
		xWorld, yWorld, theta = robot.getPosition()
		# x, y = self.getImCoor( xWorld, yWorld )

		start = (  xWorld , yWorld  )

		stop = ( xWorld + 0.2*np.cos(theta), yWorld + 0.2*np.sin(theta) )

		start = self.getImCoor( *start )
		stop = self.getImCoor( *stop )

		center = ( (start[0] + stop[0])/2, (start[1] + stop[1])/2 )

		cv2.circle( self.img, center, 14, (0,0,0), -1 )
		cv2.circle( self.img, center, 13, circleColor, -1 )
		
		# cv2.arrowedLine( self.img, start, stop, (255,0,0), 4 )
		cv2.arrowedLine( self.img, start, stop, (0,0,0), 2, tipLength = 0.3 )

	def drawScanLine( self, scanLine ):

		for key, l in scanLine.items():

			x1, y1 = l.startPoint
			x2, y2 = l.endPoint

			x1, y1 = self.getImCoor( x1, y1 )
			x2, y2 = self.getImCoor( x2, y2 )

			cv2.line( self.img, (x1,y1), (x2,y2), (0,0,255), 1 )

	def drawPoints( self, pointList, size = 4, color = None ):

		if color is None:
			color = ( 0, 0, 255 )

		for p in pointList:
			x, y = self.getImCoor( *p )
			cv2.circle( self.img, (x,y), size, color, -1 )

	def plotScoreHistogram( self, scoreList ):
		# mx = max( scoreList )
		# s_ = [ s / mx for s in scoreList ]
		try:
			self._axHis.cla( )
			self._axHis.hist( scoreList, density = False, log = False )
		except:
			return

	def plotTrackData( self, data ):
		assert len( data ) == len( self._trackData )
		self._axPlot.cla( )
		for trackD, d in zip(self._trackData, data):
			if len( trackD ) > 100:
				trackD.pop( 0 )

			trackD.append( d )

			self._axPlot.plot( trackD )

		self._axPlot.legend( self._trackerName, loc = 'upper right' )

	def show( self, waitTime = 1 ):
		h,w = self.img.shape[:2]

		for i in range( 0, h, 100 ):
			self.img[ i, 0:w, : ] = 0

		for i in range( 0, w, 100 ):
			self.img[ 0:h, i, : ] = 0

		# cv2.imshow( 'field', self.img )
		# cv2.waitKey( waitTime )
		# plt.imshow( self.img[:,:,::-1] )
		# plt.pause( 0.0001 )
		self._im.set_data( self.img[ :, :, ::-1 ] )
		self._fig.canvas.draw( )
		# self._writer.grab_frame( )
		plt.pause( 0.001 )

	def destroy( self ):
		# cv2.destroyAllWindows( )
		# plt.show( )
		# self._writer.finish( )
		pass