#!/usr/bin/env python
import rospy

from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose2D

from newbie_hanuman.srv import reqMotorCortexIntegration, reqMotorCortexIntegrationResponse
from newbie_hanuman.srv import Localization_command, Localization_commandResponse, Localization_commandRequest
from newbie_hanuman.msg import localizationMsg
from newbie_hanuman.msg import HanumanStatusMsg

from cell.nodeCell import NodeBase
from MCL import AMCL
from KF import KF
from UKF2 import UKF
from Field import Field, FieldLine, CircleFieldLine, FieldBoundary, AbstractLandmark
from Robot import Camera, Robot

from spinalCord.VelocityAccumulator import VelocityAccumulator

from utility.HanumanForwardKinematic import loadDimensionFromConfig

import time
import timeit
import pickle
import numpy as np 
import math

import configobj

DEFAULT_CONFIG = {
					"Localization" : {  
										"n_particle" 		: 	"500",
										"sigma_scanline"	:	"0.2",
										"sigmaD_landmark"	:	"0.2",
										"sigmaAngle_landmark":	"20",
										"sigmaR"			:	"0.5",
										"sigmaTheta"		:	"20",
									 }
				}

class LocalizationNode( NodeBase ):

	def __init__( self ):

		super( LocalizationNode, self ).__init__( "LocalizationNode" )

		self.rosInitNode( )

		configPath = self.getParam( '/robot_config', None )

		mapFN = self.getParam( '/localization/precompute_map_FN', None)

		if mapFN is None:
			mapFN = "/".join(__file__.split( '/' )[:-1] + ['map.npz'])

		self._initX = self.getParam( '/localization/init_x', -2.5)
		self._initY = self.getParam( '/localization/init_y', -2.0 )
		self._initW = math.radians(float(self.getParam( '/localization/init_w', 90.0 )))

		if configPath is not None:
			loadDimensionFromConfig( configPath )

		self.field = self.createMap( mapFN )

		if configPath is not None:
			config = configobj.ConfigObj( configPath )

			camera = self.createCamera( config )

			if config.has_key( "Localization" ):
				self._mcl = self.createMCL( config["Localization"], camera, self.field )
				nParticle = int(config['Localization']['n_particle'])
			else:
				self._mcl = self.createMCL( DEFAULT_CONFIG['Localization'], camera, self.field )
				nParticle = int(DEFAULT_CONFIG['Localization']['n_particle'])

			if config["RobotVelocity"].has_key( "mat" ):
				mat = eval( config["RobotVelocity"]["mat"] )

			else:
				mat = [1, 0, 0, 0, 1, 0, 0, 0, 1]

		else:
			camera = self.createCamera( DEFAULT_CONFIG )
			self._mcl = self.createMCL( DEFAULT_CONFIG['Localization'], camera, self.field )
			nParticle = int(DEFAULT_CONFIG['Localization']['n_particle'])

			mat = [1, 0, 0, 0, 1, 0, 0, 0, 1]

		mat = np.array( mat, dtype = np.float64 ).reshape( 3, 3 )

		self._kf = KF( self._initX, self._initY, self._initW, self.field )
		# self._kf = UKF( self._initX, self._initY, self._initW, self.field )

		rospy.loginfo( "Spawning Particles...")

		t0 = timeit.default_timer( )
		self._mcl.spawnParticles( [ self._initX, self._initY, self._initW ], 
									0.7, np.pi/18, nParticle = nParticle )
		print self._mcl.nParticle
		elapse = timeit.default_timer( ) - t0

		rospy.loginfo( "Time For Spawning Particles : {}".format( elapse ) )

		self.setFrequencyFromParam( '/localization/frequency' )

		self.rosInitSubscriber("/localization/scan_line",
								localizationMsg,
								self.getActualSensorDataCallback )

		self.rosInitPublisher("/localization/robot_pose",
								Pose2D )

		self.__currentPanTilt = [ 0.0, 0.0 ]
		self._actualSensorPoint = []
		self._boundaryPoint = []
		self._landmark = []

		self.__velocityAccum = VelocityAccumulator( xInit = 0.0,
													yInit = 0.0,
													wInit = 0.0,
													covMat = np.diag( [0.25,0.25,np.pi/10] ),
													mat = mat, 
												)

		self.rosInitSubscriber("/spinalcord/hanuman_status",
								HanumanStatusMsg,
								self.getHanumanStatusCallback
								)

		self.rosInitService(	"/Localization/command", 
								Localization_command, 
								self.__serviceHandle)

		self.__isStop = False
		self.__reset = False

	def __serviceHandle( self, req ):

		if req.command == Localization_commandRequest.RESET:
			self.__reset = True

		elif req.command == Localization_commandRequest.SET_POS:
			self.__reset = True
			self._initX = req.x
			self._initY = req.y
			self._initW = req.w

		elif req.command == Localization_commandRequest.STOP:
			self.__isStop = True

		elif req.command == Localization_commandRequest.RESUME:
			self.__isStop = False

		else:
			pass

		status = Localization_commandRequest.STOP if self.__isStop else Localization_commandRequest.RESUME

		return Localization_commandResponse( status = status )

	def createCamera( self, config ):

		if not config.has_key( 'CameraParameters' ):
			rospy.logwarn( 'Robot config not contain \'CameraParameters\' section' )
		
			fx = 620.0
			fy = 620.0
			cx = 320.0
			cy = 240.0
			imgW = 640.0
			imgH = 480.0

		else:
			fx = float( config['CameraParameters']['fx'] )
			fy = float( config['CameraParameters']['fy'] )
			cx = float( config['CameraParameters']['cx'] )
			cy = float( config['CameraParameters']['cy'] )
			imgW = float( config['CameraParameters']['imgW'] )
			imgH = float( config['CameraParameters']['imgH'] )

		camera = Camera( fx = fx, fy = fy, cx = cx, cy = cy, imgW = imgW, imgH = imgH )

		return camera

	def createMCL( self, config, camera, field ):
		n_particle = config[ "n_particle" ]
		sigma_scanline = float(config[ "sigma_scanline"])
		sigmaD_landmark = float(config[ "sigmaD_landmark"])
		sigmaAngle_landmark = math.radians(float(config[ "sigmaAngle_landmark"]))
		sigmaR = float(config[ "sigmaR"])
		sigmaTheta = math.radians(float(config[ "sigmaTheta"]))

		mcl = AMCL( camera, field, sigma_scanline=sigma_scanline, sigmaD_landmark=sigmaD_landmark,
					sigmaAngle_landmark=sigmaAngle_landmark, sigmaR=sigmaR, sigmaTheta=sigmaTheta )

		return mcl
 
	def createMap( self, mapFN ):
		field = Field( [], lowerX = -5.2, upperX = 5.2,
							lowerY = -3.7, upperY = 3.7 )

		field.loadDistanceMap( mapFN )

		field.addLandmark( AbstractLandmark( 'circle', (0,0) ) )
		
		field.addLandmark( AbstractLandmark( 'goal', (-4.5,  0.85 ) ) )
		field.addLandmark( AbstractLandmark( 'goal', (-4.5, -0.85 ) ) )
		field.addLandmark( AbstractLandmark( 'goal', ( 4.5,  0.85 ) ) )
		field.addLandmark( AbstractLandmark( 'goal', ( 4.5, -0.85 ) ) )

		return field

	def getHanumanStatusCallback( self, msg ):

		# print "vel", msg.linearVelX, msg.linearVelY, msg.angularVel

		self.__velocityAccum.setVelocity( msg.linearVelX,
											msg.linearVelY, 
											msg.angularVel )
		self.__velocityAccum.startAccumulate( )

	def getActualSensorDataCallback( self, msg ):

		js = msg.pointClound.jointState

		pan = js.position[ js.name.index( "pan" ) ]
		tilt = js.position[ js.name.index( "tilt" ) ]

		self.__currentPanTilt = [ pan, tilt ]

		max_range = msg.pointClound.max_range
		min_range = msg.pointClound.min_range
		pointArray = np.array([ [p.x, p.y] for p in msg.pointClound.points ])
## TODO : visittor : Don't hard code scan line max range.
		self._actualSensorPoint = pointArray[ : msg.pointClound.splitting_index[0] ]
		if len( self._actualSensorPoint ) != 0:
			dist = (self._actualSensorPoint[:,0]**2) + (self._actualSensorPoint[:,1]**2)
			includeIdx = (dist < max_range**2) & (dist > min_range**2)
			self._actualSensorPoint = self._actualSensorPoint[ includeIdx ]

		self._boundaryPoint = pointArray[ msg.pointClound.splitting_index[0] : ]
		if len( self._boundaryPoint ) != 0:
			dist = (self._boundaryPoint[:,0]**2) + (self._boundaryPoint[:,1]**2)
			includeIdx = (dist < max_range**2) & (dist > min_range**2)
			self._boundaryPoint = self._boundaryPoint[ includeIdx ]

		self._landmark = [ (k,[v.x,v.y],c) for k,v,c in zip( msg.landmark.names, msg.landmark.pose, msg.landmark.confidences ) ]

	def getRobotMovement( self ):

		self.__velocityAccum.lock( )

		dx, dy, dw = self.__velocityAccum.getAccumulateVelocity( )

		self.__velocityAccum.setPosition( 0.0, 0.0, 0.0 )

		self.__velocityAccum.unlock( )

		self.__velocityAccum.startAccumulate( )

		return dx, dy, dw

	def updateParticleFilter( self, sensorMsg, isStable, softConstrain = None ):
		# if not isStable:
		# 	sensorMsg[ 'scanLine' ] = []
		# 	sensorMsg[ 'boundary' ] = []
		# 	sensorMsg[ 'landmark' ] = []

		rep_bot, varX, varY, varW = self._mcl.update( sensorMsg, 
											resampling=True, 
											visualize=False,
											resamplingConstrain = None,
											softConstrain = softConstrain )

		return rep_bot.getPosition( ), np.diag( [varX, varY, varW] )

	def updateKalman( self, sensorState, odom, uncertainty, sensorUncertainty, convMat ):

		primeState, primeConv = self._kf.update( sensorState, odom, uncertainty, 
								sensorUncertainty, convMat )

		return primeState, primeConv

	def run( self, gatherDataMode = False, recordDataFN = None ):

		if gatherDataMode:
			rospy.loginfo( "Start Particle Filter With Gathering Data Mode.")
			self.sensorData = []
		else:
			rospy.loginfo( "Start Particle Filter Node!!" )

		count = 0

		robot1 = Robot( self._initX, self._initY, self._initW, Camera( ) )
		robot2 = Robot( self._initX, self._initY, self._initW, Camera( ) )
		robot3 = Robot( self._initX, self._initY, self._initW, Camera( ) )

		resamplingConstrain = None

		if recordDataFN is not None:
			msgGenerator = self.getSensorMsgFromFile( recordDataFN )

		else:
			msgGenerator = self.getSensorMsg( )

		weightKF = 0.0
		weightPF = 2.0

		while not rospy.is_shutdown( ):

  			if self.__isStop:
  				continue

  			elif self.__reset:
				self._mcl.spawnParticles( [ self._initX, self._initY, self._initW ], 
						0.7, np.pi/18, nParticle = self._mcl.nParticle )

				self._kf = KF( self._initX, self._initY, self._initW, self.field )

				self.__velocityAccum.lock( )
				self.__velocityAccum.setPosition( 0.0, 0.0, 0.0 )
				self.__velocityAccum.setConvMat( np.diag( [0.25,0.25,np.pi/10] ) )
				self.__velocityAccum.unlock( )

				self.__reset = False

				continue

			sensorMsg = msgGenerator.next( )

  			count = ( count + 1 ) % 12

  			if gatherDataMode:
  				self.sensorData.append( sensorMsg )
  				rospy.loginfo( 'Number of data {}'.format( len(self.sensorData) + 1 ) )

  				# robot1.setBotPosIncremental( *sensorMsg['moveMent'] )
  				# self._mcl.visualize( [1]*self._mcl.nParticle, sensorMsg, robot1 )

  			else:

  				if all( v == 0 for v in sensorMsg['moveMent'] ):
  					isStable = True
  				else:
  					isStable = False 

  				robot3.setBotPosIncremental( *sensorMsg['moveMent'] )

  				# self._mcl.visualize( [1]*self._mcl.nParticle, sensorMsg, robot1 )

  				###########################################################
  				###########################################################

  				weightKF = 0.1 if isStable else 0.1
  				weightPF = 3.0 if isStable else 20.0

  				kf_state, kf_conv = self._kf.getStateAndConv( )

  				(x, y, w), R = self.updateParticleFilter(  sensorMsg, isStable,
  								softConstrain = (kf_state, kf_conv) )

  				sensorState = np.array( [x, y, w] )
  				convMat = self.__velocityAccum.getCurrentCov()

  				uncertainty = np.diag( [1,1,np.pi/9] ).astype( np.float64 )
  				uncertainty *= weightKF

  				R *= weightPF

  				primeState, primeConv = self.updateKalman( sensorState, sensorMsg['moveMent'],
  														uncertainty, R, convMat )

  				self.__velocityAccum.setConvMat( primeConv )

  				robot1.setBotPos( *primeState )
  				robot2.setBotPos( *(x,y,w) )
  				# self._mcl.visualize( [1]*self._mcl.nParticle, sensorMsg, robot2, 
  				# 						rep_robot2 = robot1,
  				# 						rep_robot3 = robot3
  				# 					)
  				msg = Pose2D( x = primeState[0], y = primeState[1], theta = primeState[2] )
  				self.publish( msg )

  			self.sleep( )

  	def getSensorMsg( self ):

  		while not rospy.is_shutdown():
  			dx, dy, dtheta = self.getRobotMovement( )

  			# self._startPosition = self._currPosition

  			sensorMsg = { 	
  							'scanLine'	:	self._actualSensorPoint,
  							'boundary'	:	self._boundaryPoint,
  							'moveMent'	:	[dx, dy, dtheta],
  							'panTilt'	:	self.__currentPanTilt,
  							'landmark'	:	self._landmark,
  						}

  			yield sensorMsg

  	def getSensorMsgFromFile( self, fileName ):
  		data = pickle.load( open( fileName, 'r' ) )

  		nData = len( data )

  		fusePeriod = 10

  		indx = 3
  		count = 0

  		mergeSensorMsg = {}

  		mergeSensorMsg['scanLine'] = data[ 0 ]['scanLine']
		mergeSensorMsg['boundary'] = data[ 0 ]['boundary']
		mergeSensorMsg['moveMent'] = data[ 0 ]['moveMent']
		mergeSensorMsg['panTilt'] = data[ 0 ]['panTilt']
		mergeSensorMsg['landmark'] = data[ 0 ]['landmark']

  		while not rospy.is_shutdown( ):

  			sensorMsg = data[ indx ]

			mergeSensorMsg['scanLine'] = sensorMsg['scanLine']
			mergeSensorMsg['boundary'] = sensorMsg['boundary']
			mergeSensorMsg['moveMent'][0] += sensorMsg['moveMent'][0]
			mergeSensorMsg['moveMent'][1] += sensorMsg['moveMent'][1]
			mergeSensorMsg['moveMent'][2] += sensorMsg['moveMent'][2]
			mergeSensorMsg['panTilt'] = sensorMsg['panTilt']
			mergeSensorMsg['landmark'] = sensorMsg['landmark']

  			if (indx + 1) % fusePeriod == 0:
  				# rospy.loginfo( "{} / {}".format( indx + 1, nData ) )

  				yield mergeSensorMsg

				mergeSensorMsg['moveMent'][0] = 0.0
				mergeSensorMsg['moveMent'][1] = 0.0
				mergeSensorMsg['moveMent'][2] = 0.0

  			indx = ( indx + 1 ) 
  			# indx %= nData
  			count = ( count + 1 ) % 2

  	def end( self ):
  		rospy.loginfo( "Exit Localization Node!!" )
  		self._mcl.end( )

  		try:
  			return self.sensorData
  		except:
  			return None

def main( ):
	import optparse
	parser = optparse.OptionParser()
	parser.add_option( '-m', '--gather_mode',
					type='string',
					action='store',
					dest='gather_mode',
					help='gather_mode',)
	parser.add_option( '-r', '--use_record_data',
					type='string',
					action='store',
					dest='use_record_data',
					help='use_record_data',)

	(options, args) = parser.parse_args()

	gatherMode = False
	if options.gather_mode is not None:
		gatherMode = True
		filePath = options.gather_mode

	node = LocalizationNode( )

	try:
		node.run( gatherDataMode = gatherMode, recordDataFN = options.use_record_data )

	except rospy.ROSInterruptException as e:
		pass
	# except Exception as e:
	# 	rospy.logwarn( str(e) )
	finally:
		sensorData = node.end()
		if gatherMode:
			print "save"
			pickle.dump( sensorData, open( filePath, 'w' ) )