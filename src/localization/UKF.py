import numpy as np 
import cv2
import configobj
import matplotlib.pyplot as plt

from utility.HanumanForwardKinematic import getMatrixForForwardKinematic, loadDimensionFromConfig

from Robot import Robot, Camera
from Field import Field, FieldLine, CircleFieldLine, FieldBoundary, AbstractLandmark
from Painter import Painter

import rospy

import time
import timeit
import math
import random

class UKF( object ):

	def __init__( self, initX, initY, initW, field, W0 = -0.0, convMat = None ):
		'''
		'''
		self._W0 = min( max( -1 + 1e-8, W0 ), 1 - 1e-8 )
		self._robot = Robot( 0.0, 0.0, 0.0, camera = Camera(),
					forwardkinematic = getMatrixForForwardKinematic,
					nScanLine = 1, ID = 0 )

		self._field = field

		self._state = np.array( [ [ initX, initY, initW ] ] ).T

		if convMat is None:
			convMat = np.diag( [ 0.2*0.2, 0.2*0.2, (np.pi/10)*(np.pi/10)] )

		self._convMat = convMat

		################################
		## For visualization
		################################
		self._sigmaPoint = np.array([])
		self._painter = Painter( imgW = 1500, imgH = 1000 )
		self._painter.setFieldCoor( self._field )
 
		return

	def _measurementUpdate( self, sigmaPoint, odometry ):

		dx = odometry[0,0]
		dy = odometry[1,0]
		dw = odometry[2,0]

		for i in range( sigmaPoint.shape[1] ):

			p = sigmaPoint[:, i]

			tranMat = np.array([[ math.cos(p[2]),	-math.sin(p[2]),	0 ],
								[ math.sin(p[2]),	math.cos(p[2]),		0 ],
								[ 0,				0,					1] ]
							)

			odomRobot = np.matmul( tranMat, odometry )

			p += odomRobot.flatten()

	def _generateSigmaPoint( self, state, convMat ):
		'''
		'''
		xMean, yMean, wMean = state.flatten( )

		sigmaPointsList = [ state.flatten( ) ]
		weightsList = [ self._W0 ]

		sqrtConv = np.linalg.cholesky( convMat )

		for i in range( 3 ):

			n = i + 1
			W = n / ( 1 - self._W0 )

			x = xMean + math.sqrt( W )*sqrtConv[0,i]
			y = yMean + math.sqrt( W )*sqrtConv[1,i]
			w = wMean + math.sqrt( W )*sqrtConv[2,i]

			sigmaPointsList.append( (x,y,w) )
			weightsList.append( (1.0 - self._W0) / (2.0*3.0) )

			x = xMean - math.sqrt( W )*sqrtConv[0,i]
			y = yMean - math.sqrt( W )*sqrtConv[1,i]
			w = wMean - math.sqrt( W )*sqrtConv[2,i]

			sigmaPointsList.append( (x,y,w) )
			weightsList.append( (1.0 - self._W0) / (2.0*3.0) )

		sigmaPointsList = np.array( sigmaPointsList ).T
		weightsList = np.array( weightsList )

		return sigmaPointsList, weightsList

	def _propagateScanLine( self, sigmaPointsList, sensorMsg ):
		'''
		'''
		allDeltaYList = []

		# sensorPoint = np.vstack( (sensorMsg['scanLine'], sensorMsg['boundary']) )
		sensorPoint = sensorMsg[ 'boundary' ]
 
		for ii in range( sigmaPointsList.shape[1] ):

			x = sigmaPointsList[ 0, ii ]
			y = sigmaPointsList[ 1, ii ]
			w = sigmaPointsList[ 2, ii ]

			self._robot.setBotPos( x, y, w )

			pointworldCoor = self._robot.transformToWorldCoor( sensorPoint[:,:2] )

			deltaYList = []

			for p in pointworldCoor:

				deltaY = self._field.getDistanceValToBoundary( *p )
				deltaYList.append( deltaY )

			allDeltaYList.append( deltaYList )
		
		allDeltaYList = np.array( allDeltaYList ).T 

		return allDeltaYList

	def _propagateLandmark( self, sigmaPointsList, landmarkPos ):
		'''
		'''
		allSelectedLandmark = []
		Z = []
		for ii in range( sigmaPointsList.shape[1] ):

			x = sigmaPointsList[ 0, ii ]
			y = sigmaPointsList[ 1, ii ]
			w = sigmaPointsList[ 2, ii ]

			delta = np.array( [ [landmarkPos[0] - x], 
								[landmarkPos[1] - y] ])

			q = np.matmul( delta.T, delta )
			z = np.array(  [
							math.sqrt(q), 
							math.atan2(delta[1],delta[0]) - w,
							1
							] )

			Z.append(z)

		Z = np.vstack( Z ).T

		return Z

	def predictionStep( self, state, convMat, odometry, uncertainty ):
		'''
		'''

		sigmaPointsList, weightsList = self._generateSigmaPoint( state, convMat )

		self._sigmaPoint = sigmaPointsList.copy()

		odometry = np.array( odometry )
		odometry = odometry.reshape( 3, 1 ) if odometry.ndim == 1 else odometry

		# sigmaPointsList += odometry
		self._measurementUpdate( sigmaPointsList, odometry )
		# sigmaPointsList[2,:] = ( sigmaPointsList[2,:] + np.pi) % (2 * np.pi ) - np.pi

		newState = np.average( sigmaPointsList, axis=1, weights=weightsList ).reshape( 3, 1 )

		zeroMeanSigmaPoint = sigmaPointsList - newState

		newConv = np.matmul( (zeroMeanSigmaPoint*weightsList), zeroMeanSigmaPoint.T ) + uncertainty

		return newState, newConv

	def measurementUpdate( self, predState, predConvMat, sensorMsg, 
						uncertainty ):
		'''
		'''
		sigmaPointsList, weightsList = self.sigmaPointUpdate( predState, predConvMat )
		sigmaMean = np.average( sigmaPointsList, axis=1, weights=weightsList ).reshape( 3, 1 )
		zeroMeanSigma = sigmaPointsList - sigmaMean

		k_z = np.zeros( (3,1) )
		k_p = np.zeros( (3,3) )

		landmarkDict = {}
		for name, posList in self._field.getAllLandmarkPosition().items( ):
			for pos in posList:

				Z = self._propagateLandmark( sigmaPointsList, pos )

				ZMean = np.average( Z, axis=1, weights=weightsList ).reshape( 3, 1 )

				zeroMeanZ = Z - ZMean

				Cov_y = np.matmul( (zeroMeanZ*weightsList), zeroMeanZ.T ) + uncertainty
			
				landmarkDict.setdefault( name, [] ).append( (Cov_y, ZMean, zeroMeanZ) )

		for name, pos, confidence in sensorMsg['landmark']:

			r = math.sqrt( pos[0]**2 + pos[1]**2 )
			theta = math.atan2( pos[1], pos[0] )
			Z = np.array([r, theta, confidence]).reshape(3,1)
			mahalanobis = lambda x: np.matmul( (Z-x[1]).T, np.matmul(np.linalg.inv(x[0]), Z-x[1]) )
			
			Cov_y, ZMean, zeroMeanZ = min( landmarkDict[name], key = lambda x: mahalanobis(x) )
			Cov_xy = np.matmul( (zeroMeanSigma*weightsList), zeroMeanZ.T )

			if mahalanobis( (Cov_y, ZMean, zeroMeanZ) ) > 0.7:
				print name
				print Z
				print Z - ZMean
				print mahalanobis( (Cov_y, ZMean, zeroMeanZ) )

				continue
			print mahalanobis( (Cov_y, ZMean, zeroMeanZ) )
			k = np.matmul(Cov_xy, np.linalg.inv(Cov_y))
			k_z += np.matmul( k, Z - ZMean )
			k_p += np.matmul( k, np.matmul(Cov_y, k.T) )

			# print np.matmul( k, Z - ZMean )

			print "-----------------------------"
		# print k_z
		print '=================================='

		newState = sigmaMean + k_z
		newConv = predConvMat - k_p

		try:
			np.linalg.cholesky( newConv )
		except np.linalg.LinAlgError:
			rospy.logwarn( "Non positive definitive detected." )

			newState = predState
			newConv = predConvMat

		return newState, newConv

	def sigmaPointUpdate( self, state, convMat ):
		'''
		'''
		return self._generateSigmaPoint( state, convMat )

	def update( self, sensorMsg, uncertainty, sensorUncertainty, isSensorValid = True ):
		'''
		'''

		odometry = sensorMsg[ 'moveMent' ]

		predState, predConv = self.predictionStep( self._state, self._convMat, 
													odometry, uncertainty )

		if isSensorValid and len(sensorMsg['landmark']) != 0:

			finalState, finalConv = self.measurementUpdate( predState, predConv, sensorMsg,
												sensorUncertainty )

		else:
			print 'No measurement.'
			finalState = predState
			finalConv = predConv


		self._state = finalState
		self._convMat = finalConv

		rep_bot = Robot( self._state[0,0], self._state[1,0], self._state[2,0],
						Camera( ) )

		rep_bot2 = Robot( predState[0,0], predState[1,0], predState[2,0],
						Camera( ) )

		return rep_bot, finalConv.copy( )

	def visualize( self, sensorMsg, rep_robot, rep_robot2 = None, rep_robot3 = None ):

		actualPoint = sensorMsg[ 'scanLine' ]
		boundary = sensorMsg[ 'boundary' ]
		landmarkPos = [ l[1] for l in sensorMsg[ 'landmark' ] ]
		landmarkName = [ l[0] for l in sensorMsg[ 'landmark' ] ]

		self._painter.drawField( )

		robotList = [ Robot( x, y, w, Camera() ) for x, y, w in self._sigmaPoint.T ]

		for robot in robotList:

			self._painter.drawRobot( robot, drawScanline = False )

		# self._painter.drawRobot2( rep_robot, circleColor = (0,0,255) )
		if len( actualPoint ) != 0:
			pointWorldCoor = rep_robot.transformToWorldCoor( actualPoint )

			self._painter.drawPoints( pointWorldCoor )

		if len( boundary ) != 0:
			pointWorldCoor = rep_robot.transformToWorldCoor( boundary )

			self._painter.drawPoints( pointWorldCoor, color = (255,0,0) )

		if len( landmarkPos ) != 0:
			pointWorldCoor = rep_robot.transformToWorldCoor( landmarkPos )

			for p, n in zip(pointWorldCoor, landmarkName):

				if n == 'field_corner':
					color = (255, 0, 255)
				elif n == 'goal':
					color = (0, 255, 255)
				elif n == 'circle':
					color = (255,255,0)
				else:
					color = (0,0,0)

				self._painter.drawPoints( [p], color = color, size = 15 )


		if rep_robot3 is not None:
			self._painter.drawRobot2( rep_robot3, circleColor = (255, 0, 0) )

		if rep_robot2 is not None:
			self._painter.drawRobot2( rep_robot2, circleColor = (0,255,0) )

		self._painter.show( )