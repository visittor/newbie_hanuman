import cv2
import numpy as np
import configobj
import matplotlib.pyplot as plt

from utility.HanumanForwardKinematic import getMatrixForForwardKinematic, loadDimensionFromConfig

from Robot import Robot, Camera
from Field import Field, FieldLine, CircleFieldLine, FieldBoundary, AbstractLandmark
from Painter import Painter

import rospy

import time
import timeit
import math
import random

class UKF( object ):

	def __init__( self, initX, initY, initW, field, W0 = -0.02, convMat = None ):
		'''
		'''
		self._W0 = min( max( -1 + 1e-8, W0 ), 1 - 1e-8 )
		self._robot = Robot( 0.0, 0.0, 0.0, camera = Camera(),
					forwardkinematic = getMatrixForForwardKinematic,
					nScanLine = 1, ID = 0 )

		self._field = field

		self._state = np.array( [ [ initX, initY, initW ] ] ).T

		if convMat is None:
			convMat = np.diag( [ 0.2*0.2, 0.2*0.2, (np.pi/10)*(np.pi/10)] )

		self._convMat = convMat

		################################
		## For visualization
		################################
		self._sigmaPoint = np.array([])
		self._painter = Painter( imgW = 1500, imgH = 1000 )
		self._painter.setFieldCoor( self._field )
 
		return

	def _measurementUpdate( self, sigmaPoint, odometry ):

		dx = odometry[0,0]
		dy = odometry[1,0]
		dw = odometry[2,0]

		for i in range( sigmaPoint.shape[1] ):

			p = sigmaPoint[:, i]

			tranMat = np.array([[ math.cos(p[2]),	-math.sin(p[2]),	0 ],
								[ math.sin(p[2]),	math.cos(p[2]),		0 ],
								[ 0,				0,					1] ]
							)

			odomRobot = np.matmul( tranMat, odometry )

			p += odomRobot.flatten()

	def _generateSigmaPoint( self, state, convMat ):
		'''
		'''
		xMean, yMean, wMean = state.flatten( )

		sigmaPointsList = [ state.flatten( ) ]
		weightsList = [ self._W0 ]

		sqrtConv = np.linalg.cholesky( convMat )

		for i in range( 3 ):

			n = i + 1
			W = n / ( 1 - self._W0 )

			x = xMean + math.sqrt( W )*sqrtConv[0,i]
			y = yMean + math.sqrt( W )*sqrtConv[1,i]
			w = wMean + math.sqrt( W )*sqrtConv[2,i]

			sigmaPointsList.append( (x,y,w) )
			weightsList.append( (1.0 - self._W0) / (2.0*3.0) )

			x = xMean - math.sqrt( W )*sqrtConv[0,i]
			y = yMean - math.sqrt( W )*sqrtConv[1,i]
			w = wMean - math.sqrt( W )*sqrtConv[2,i]

			sigmaPointsList.append( (x,y,w) )
			weightsList.append( (1.0 - self._W0) / (2.0*3.0) )

		sigmaPointsList = np.array( sigmaPointsList ).T
		weightsList = np.array( weightsList )

		return sigmaPointsList, weightsList

	def averageState( self, state, weightsList ):
		avgState = np.average( state, axis=1, weights=weightsList ).reshape( 3, 1 )
		sinWArr = np.sin( state[2,:] )
		cosWArr = np.cos( state[2,:] )

		avgSin = np.average( sinWArr, weights=weightsList )
		avgCos = np.average( cosWArr, weights=weightsList )

		avgW = math.atan2( avgSin, avgCos )

		avgState[2,0] = avgW

		return avgState

	def predictionStep( self, state, convMat, odometry, uncertainty ):
		'''
		'''

		sigmaPointsList, weightsList = self._generateSigmaPoint( state, convMat )

		self._sigmaPoint = sigmaPointsList.copy()

		odometry = np.array( odometry )
		odometry = odometry.reshape( 3, 1 ) if odometry.ndim == 1 else odometry

## NOTE : visittor : Change this function name pls.
		self._measurementUpdate( sigmaPointsList, odometry )
		sigmaPointsList[2,:] = ( sigmaPointsList[2,:] + np.pi) % (2 * np.pi ) - np.pi

		newState = self.averageState( sigmaPointsList, weightsList )

		zeroMeanSigmaPoint = sigmaPointsList - newState

		newConv = np.matmul( (zeroMeanSigmaPoint*weightsList), zeroMeanSigmaPoint.T ) + uncertainty

		return newState, newConv

	def measurementUpdate( self, predState, predConvMat, sensorState, uncertainty ):
		'''
		'''
		sigmaPointsList, weightsList = self._generateSigmaPoint( predState, predConvMat )
		sigmaMean = self.averageState( sigmaPointsList, weightsList )
		zeroMeanSigma = sigmaPointsList - sigmaMean

		Z = sigmaPointsList.copy( )
		ZMean = self.averageState( Z, weightsList )
		zeroMeanZ = Z - ZMean

		Cov_y = np.matmul( (zeroMeanZ*weightsList), zeroMeanZ.T ) + uncertainty
		Cov_xy = np.matmul( (zeroMeanSigma*weightsList), zeroMeanZ.T )

		K = np.matmul(Cov_xy, np.linalg.inv(Cov_y))
		newState = sigmaMean + np.matmul( K, sensorState - ZMean )
		newConv = predConvMat - np.matmul( K, np.matmul( Cov_y, K.T ) )
		print K
		print sensorState[2], ZMean[2]
		try:
			np.linalg.cholesky( newConv )
		except np.linalg.LinAlgError:
			rospy.logwarn( "Non positive definitive detected." )

			newState = predState
			newConv = predConvMat

		return newState, newConv

	def update( self, sensorState, odometry, uncertainty, sensorUncertainty, convMat = None ):

		'''
		'''
		sensorState = sensorState.reshape( 3, 1 )

		predState, predConv = self.predictionStep( self._state, self._convMat, 
													odometry, uncertainty )

		finalState, finalConv = self.measurementUpdate( predState, predConv, sensorState,
											sensorUncertainty )

		self._state = finalState
		self._convMat = finalConv

		rep_bot = Robot( self._state[0,0], self._state[1,0], self._state[2,0],
						Camera( ) )

		rep_bot2 = Robot( predState[0,0], predState[1,0], predState[2,0],
						Camera( ) )

		# self.visualize( None, rep_bot )

		return self._state.flatten(), finalConv.copy( )

	def visualize( self, sensorMsg, rep_robot, rep_robot2 = None, rep_robot3 = None ):

		# actualPoint = sensorMsg[ 'scanLine' ]
		# boundary = sensorMsg[ 'boundary' ]
		# landmarkPos = [ l[1] for l in sensorMsg[ 'landmark' ] ]
		# landmarkName = [ l[0] for l in sensorMsg[ 'landmark' ] ]

		self._painter.drawField( )

		robotList = [ Robot( x, y, w, Camera() ) for x, y, w in self._sigmaPoint.T ]

		for robot in robotList:

			self._painter.drawRobot( robot, drawScanline = False )

		# self._painter.drawRobot2( rep_robot, circleColor = (0,0,255) )
		# if len( actualPoint ) != 0:
		# 	pointWorldCoor = rep_robot.transformToWorldCoor( actualPoint )

		# 	self._painter.drawPoints( pointWorldCoor )

		# if len( boundary ) != 0:
		# 	pointWorldCoor = rep_robot.transformToWorldCoor( boundary )

		# 	self._painter.drawPoints( pointWorldCoor, color = (255,0,0) )

		# if len( landmarkPos ) != 0:
		# 	pointWorldCoor = rep_robot.transformToWorldCoor( landmarkPos )

		# 	for p, n in zip(pointWorldCoor, landmarkName):

		# 		if n == 'field_corner':
		# 			color = (255, 0, 255)
		# 		elif n == 'goal':
		# 			color = (0, 255, 255)
		# 		elif n == 'circle':
		# 			color = (255,255,0)
		# 		else:
		# 			color = (0,0,0)

		# 		self._painter.drawPoints( [p], color = color, size = 15 )


		if rep_robot3 is not None:
			self._painter.drawRobot2( rep_robot3, circleColor = (255, 0, 0) )

		if rep_robot2 is not None:
			self._painter.drawRobot2( rep_robot2, circleColor = (0,255,0) )

		self._painter.show( )

	def getStateAndConv( self ):
		return self._state.flatten().copy( ), self._convMat.copy( )