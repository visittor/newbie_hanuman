import numpy as np
import cv2
import configobj
import copy
import matplotlib.pyplot as plt

from newbie_hanuman.srv import local_map_service, local_map_serviceResponse

from localMap import OBJECT_NAME, createObj
from cell.nodeCell import NodeBase

import time
import timeit
import math
import random

import rospy

class LocalMapMonitor( NodeBase ):

	def __init__( self ):

		super( LocalMapMonitor, self ).__init__( 'LocalMapMonitor' )

		self.rosInitNode( )

		self.setFrequencyFromParam( '/local_map/frequency' )

		self.serviceHandler = rospy.ServiceProxy( 'local_map_info', local_map_service )

	def run( self ):
		rospy.loginfo( "Start Local Map Monitor.")

		while not rospy.is_shutdown( ):

			try:
				postDict = self.serviceHandler( reset = False ).postDict
			except rospy.ServiceException as exc:
				rospy.logwarn("[LocalMapMonitor]Service did not process request: " + str(exc))
				continue

			objectDict = {}
			for name, cart, confidence in zip( postDict.object_name, postDict.pos3D_cart, postDict.object_confidence ):

				objectDict.setdefault(name,[]).append( createObj( name, cart.x, cart.y, confidence ) )

			self.visualize( objectDict )

	def visualize( self, objectDict ):

		width = 800
		pixPerMetre = 100

		field = np.zeros( (width, width, 3), dtype = np.uint8 )

		for ii in range( 0, 180, 30 ):
			x1 = int((width / 2) * (1.5 * math.cos( math.radians( ii ) ) + 1 ))
			y1 = int((width / 2) * (1.5 * math.sin( math.radians( ii ) ) + 1 ))

			x2 = int((width / 2) * (-1.5 * math.cos( math.radians( ii ) ) + 1 ))
			y2 = int((width / 2) * (-1.5 * math.sin( math.radians( ii ) ) + 1 ))

			cv2.line( field, (x1,y1), (x2,y2), (255,255,255), 1 )

		for r in range( 0,  width, pixPerMetre ):
			cv2.circle( field, (width/2, width/2), r, (255,255,255), 1 )

			field[ :, r, : ] = [127, 127, 127]
			field[ r, :, : ] = [127, 127, 127]

		for n, objList in objectDict.items( ):

			for obj in objList:

				x, y = obj.getCartCoor( )
				x = int( x * pixPerMetre )
				y = int( y * pixPerMetre )
				y = (width / 2) - y
				x = (width / 2) - x

				scale = obj.score

				if n == 'field_corner':
					color = (255, 0, 255)
				elif n == 'goal':
					color = (0, 255, 255)
				elif n == 'ball':
					color = (255,255,0)
				elif n == 'orange':
					color = (0,128,255)
				elif n == 'blue':
					color = (255,0,0)
				elif n == 'magenta':
					color = (255,0,255)
				elif n == 'yellow':
					color = (0,255,255)
				else:
					color = (0,0,0)

				color = tuple( int( c * scale ) for c in color )

				cv2.circle( field, (y,x), 10, color, -1 )

		cv2.imshow( "3D", field )
		# cv2.imshow( "2D", blank)
		k = cv2.waitKey( 1 )

	def end( self ):
		cv2.destroyAllWindows( )
		rospy.loginfo( "Kill Local Map Monitor Node!!" )

def main( ):

	node = LocalMapMonitor( )

	try:
		node.run( )

	except rospy.ROSInterruptException as e:
		pass

	finally:
		node.end( )