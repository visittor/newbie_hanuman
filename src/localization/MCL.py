import numpy as np
import cv2
import configobj
import copy
import matplotlib.pyplot as plt
from scipy.spatial.distance import cdist
from scipy.stats import multivariate_normal

from utility.HanumanForwardKinematic import getMatrixForForwardKinematic, loadDimensionFromConfig

from Robot import Robot
from Field import Field, FieldLine, CircleFieldLine, FieldBoundary, AbstractLandmark
from Painter import Painter

import time
import timeit
import math
import random
import rospy

class AMCL( object ):
	## This color is for localization only.
	COLORLIST = np.random.uniform( low=0, high=175, size=(100, 3) ).astype(int)

	def __init__( self, camera, field, decay_slow = 0.01 , decay_fast = 0.6,
				sigma_scanline = 0.2, sigmaD_landmark = 0.2, sigmaAngle_landmark = np.pi/9,
				sigmaR = 0.5, sigmaTheta = np.pi/9 ):

		self._camera = camera

		## 95.44% of particle within 2 meters.
		self.__sigma_scanline = sigma_scanline
		self.__sigmaD_landmark = sigmaD_landmark
		self.__sigmaAngle_landmark = sigmaAngle_landmark

		self.__sigmaR = sigmaR
		self.__sigmaTheta = sigmaTheta

		self._field = field
		self._field.createLikelihoodField( sigma_scanline )

		self._lowerB = (self._field.lowerX, self._field.lowerY)
		self._upperB = (self._field.upperX, self._field.upperY)

		self._particles = []

		try:
			## For visualize only
			self._painter = Painter( imgW = 1500, imgH = 1000, trackerName = ('a', 's', 'f', 'm') )
			self._painter.setFieldCoor( self._field )
			
		except Exception as e:
			rospy.logwarn( "Error when creating painter." )
			rospy.logwarn( str( e ) )
			self._painter = None

		self._sensorMsg = { 'scanLine' : {},
							'boundary' : [],
							'moveMent' : [],
							'panTilt'  : [],
							'landmark' : [] }

		self._trackedScoreList = None

		self._weightAvg_slow = 1
		self._weightAvg_fast = 0

		self._decay_slow = decay_slow
		self._decay_fast = decay_fast

		self.__cache_maxCluster = None

	def isPosValid( self, x, y, constrain = None ):

		if self._lowerB[0]<=x<=self._upperB[0] and self._lowerB[1]<=y<=self._upperB[1]:

			if constrain is not None:
				dist = (x - constrain[0])**2 + (y - constrain[1])**2

				return True if dist**2 < constrain[2]**2 else False

			return True

		else:
			return False

	def spawnParticles( self, mean, sigma, sigmaAngle, nParticle = 100 ):
		self._particles = []
		meanX, meanY, meanTheta = mean

		for ID in xrange( nParticle ):

			valid_pos = False
			while not valid_pos:
				r = np.random.randn( 1 )[0] * sigma
				w = np.random.uniform( low=-2*np.pi, high=2*np.pi )

				x_init = r * math.cos( w ) + meanX
				y_init = r * math.sin( w ) + meanY

				valid_pos = self.isPosValid( x_init, y_init )

			theta_init = np.random.randn( 1 )[0]*sigmaAngle + meanTheta

			robot = Robot( x_init, y_init, theta_init, self._camera.copy(),
					forwardkinematic = getMatrixForForwardKinematic, nScanLine = 16, ID = ID )

			self._particles.append( robot )

			self._trackedScoreList = np.array([ 1 / nParticle for _ in range(nParticle) ])

			self.COLORLIST = np.random.uniform( low=0, high=255, size=(self.nParticle, 3) ).astype(int)

	def resampling( self, scoresList, constrain = None, minRandomRespawn_prob = 0.0, maxRandomRespawn_prob = 1.0,
					landmark = None ):
		nParticle = len( self._particles )

		selectedParticle = np.random.multinomial( nParticle, scoresList )

		newParticleList = []
		newTrackedScoreList = []

		self.__cache_maxCluster = None

		randomSpawnProb = 1.0 - (float(self._weightAvg_fast)/self._weightAvg_slow)
		# print "Random Prob : ", randomSpawnProb

		randomSpawnProb = max( minRandomRespawn_prob, min( maxRandomRespawn_prob, randomSpawnProb ) )
		randomSpawnProb = 0

		nRandomParticle = 0
		ID_pool = [ i for i in range( self.nParticle ) ]

		for i, num in enumerate( selectedParticle ):

			if num != 0:
				robot = self._particles[i].split( sigmaR=0.0, sigmaTheta=0.0, 
									checkingFunc = lambda *x : True )
				newParticleList.append( robot )
				newTrackedScoreList.append( self._trackedScoreList[ i ] )
				num -= 1

			for _ in range( num ):

				## Create some random particles to particle pool.
				if random.random( ) < randomSpawnProb:
					nRandomParticle += 1

				else:
					robot = self._particles[i].split( sigmaR=self.__sigmaR, 
													sigmaTheta=self.__sigmaTheta, 
													checkingFunc = lambda *x : self.isPosValid(*x, constrain = constrain) )

					newParticleList.append( robot )
					newTrackedScoreList.append( self._trackedScoreList[ i ] )

		robot, varX, varY, varTheta = self.getAverageParticlePosition( scoresList )

		for p in newParticleList:

			if p.ID in ID_pool:
				ID_pool.pop( ID_pool.index( p.ID ) )

		xMaxCluster, yMaxCluster, thetaMaxCluster = robot.getPosition( )

		meanScore = sum( self._trackedScoreList ) / self.nParticle

		posList = self._randomSampleFromLandmark( landmark, nRandomParticle )
		random.shuffle( ID_pool )
		for i in xrange( nRandomParticle ):
			# x_random = (random.random()*(self._upperB[0]-self._lowerB[0])) + self._lowerB[0]
			# y_random = (random.random()*(self._upperB[1]-self._lowerB[1])) + self._lowerB[1]
			# theta_random = random.random() * 2 * np.pi
			x_random, y_random, theta_random = posList[ i ]

			# d = math.sqrt((x_random - xMaxCluster)**2 + (y_random - yMaxCluster)**2)
			dx = math.fabs( x_random - xMaxCluster )
			dy = math.fabs( y_random - yMaxCluster )
			dTheta = math.fabs(theta_random - thetaMaxCluster)

			if dx < varX and dy < varY and dTheta < varTheta:
				ID = self.__cache_maxCluster['ID']

			else:
				ID = ID_pool.pop( 0 )

			robot = Robot( x_random, y_random, theta_random, self._camera.copy(),
							forwardkinematic = getMatrixForForwardKinematic,
							nScanLine = 16, ID = ID )

			newParticleList.append( robot )
			newTrackedScoreList.append( meanScore )

		self._particles = newParticleList
		self._trackedScoreList = np.array(newTrackedScoreList)

	def _randomSampleFromLandmark( self, landmark, numSample ):

		if landmark is None or len( landmark ) == 0:
			posList = []
			for i in range( numSample ):
				x = (random.random()*(self._upperB[0]-self._lowerB[0])) + self._lowerB[0]
				y = (random.random()*(self._upperB[1]-self._lowerB[1])) + self._lowerB[1]
				theta = random.random() * 2 * np.pi

				posList.append( (x,y,theta) )

			return posList

		allLandmark = self._field.getAllLandmarkPosition( )
		landmarkName = allLandmark.keys( )

		seenLandmark = []
		for name, pos, confidence in landmark:
			if name not in seenLandmark:
				seenLandmark.append( (name, pos) )

		posList = []
		for i in range( numSample ):
			name, pos = random.choice( seenLandmark )
			posLandmark = random.choice( allLandmark[ name ] )

			r = random.gauss( math.sqrt( pos[0]**2 + pos[1]**2 ), 0.5 )
			theta = random.gauss( math.atan2( pos[1], pos[0] ), np.pi / 10 )

			gamma = random.random( ) * 2 * np.pi

			x = posLandmark[ 0 ] + r * math.cos( gamma )
			y = posLandmark[ 1 ] + r * math.sin( gamma )
			theta = gamma - np.pi - theta

			posList.append( (x,y,theta) )

		return posList
 
	def getAverageParticlePosition( self, scoresList, threshold = 0.7 ):

		if self.__cache_maxCluster is not None:
			maxClusterInfo = self.__cache_maxCluster

		else:

			countNParticleDict = {}
			for p in self._particles:
				if countNParticleDict.has_key( p.ID ):

					countNParticleDict[ p.ID ] += 1

				else:
					countNParticleDict[ p.ID ] = 1

			maxClusterID = max( countNParticleDict.items( ), key = lambda x: x[1] )[0]

			posArr = np.vstack( [ list(p.getPosition()) + [scoresList[i]] for i,p in enumerate(self._particles) if p.ID == maxClusterID ] )

			xArr = posArr[:,0]
			yArr = posArr[:,1]
			wArr = posArr[:,2]
			weightArr = posArr[:,3] if posArr[:,3].sum() > 0 else np.ones( posArr[:,3].shape )

			sinWArr = np.sin( wArr )
			cosWArr = np.cos( wArr )

			avgX = np.average( xArr, weights = weightArr )
			avgY = np.average( yArr, weights = weightArr )
			
			avgSinWArr = np.average( sinWArr, weights = weightArr )
			avgCosWArr = np.average( cosWArr, weights = weightArr )

			avgW = math.atan2( avgSinWArr, avgCosWArr )

			varX = np.var( xArr	)
			varY = np.var( yArr )
			varW = np.var( wArr )

			maxClusterInfo = { 'avgX':avgX, 'avgY':avgY, 'avgW':avgW, 'varX':varX,
							'varY':varY, 'varW':varW, 'avgWeght':wArr.sum() / len(weightArr),
							'ID':maxClusterID }

		robot = Robot( maxClusterInfo['avgX'], maxClusterInfo['avgY'], 
						maxClusterInfo['avgW'], self._camera )

		return robot, maxClusterInfo['varX'], maxClusterInfo['varY'], maxClusterInfo['varW']

	def moveParticles( self, dx, dy, dTheta, pan, tilt ):

		self.__cache_maxCluster = None
		for robot in self._particles:
			# noise_x = random.gauss( 0.0, 0.05)
			# noise_y = random.gauss( 0.0, 0.02)
			# noise_z = random.gauss( 0.0, np.pi/8)
			dx_noise, dy_noise, dTheta_noise = self.sample_motion_model(dx, dy, dTheta,
												(0.33, np.pi/2, np.pi/2, 
												0.01, 0.001, 0.01, 0.005))

			robot.setBotPosIncremental( dx_noise,
										dy_noise,
										dTheta_noise )

			robot.setPanTilt( pan, tilt )

	def sample_motion_model( self, dx, dy, dTheta, alpha ):
		'''
		See also sample motion model odometry from probabilistic robotic.
		'''

		a1, a2, a3, a4, a5, a6, a7 = alpha

		high_erot = min( np.pi, a1*math.fabs(dTheta) + a2*math.fabs(dx) + a3*math.fabs(dy))
		low_erot = -1 * high_erot
		e_rot = random.triangular( low_erot, high_erot, 0.0 )

		e_tranX = random.gauss( 0.0, a4 * math.fabs( dx ) + a5*math.fabs( dy ) )

		e_tranY = random.gauss( 0.0, a6 * math.fabs( dy ) + a7*math.fabs( dx ) )

		dx = dx - e_tranX
		dy = dy - e_tranY
		dTheta = dTheta - e_rot

		return dx, dy, dTheta

	def cart2pol( self, x, y ):
		rho = np.sqrt(x**2 + y**2)
		phi = np.arctan2(y, x)
		return rho, phi
 
	def pol2cart( self, rho, phi ):
		x = rho * np.cos(phi)
		y = rho * np.sin(phi)
		return x, y	

	def likelihood_field( self, actualSensorPoints, boundary ):

		if actualSensorPoints is None or len( actualSensorPoints ) == 0:
## TODO : visittor : Find a better method to handle this case.
			score = [ 0 for p in range( self.nParticle ) ] ## since it loglikelihood
			return score

		score = []
		for p in self._particles:

			pointWorldCoor = p.transformToWorldCoor( actualSensorPoints )

			probability = 0 # since it log likelihood

			for point in pointWorldCoor:

				try:
					probability += math.log( self._field.getLikelihoodValFromMetres( *point ) )
				except ValueError:
					print self._field.getLikelihoodValFromMetres( *point )

## NOTE : visittor : Not sure, if condering a field boundary will make localization better or worse.
			boundary_worldCoor = p.transformToWorldCoor( boundary )

			for point in boundary_worldCoor:
				d = self._field.getDistanceValToBoundary( *point )
				probability += math.log(self._field.getNormalizeProbFromDistance( d, 0.2 ))
				
			score.append( probability )

		return score

	def compareLandmark( self, landmarkList ):
		score = []
		for p in self._particles:
			probability = 0 # since it loglikelihood
			
			for name, pos, confidence in landmarkList:
				probability += math.log(self._field.compareLandmark( name, p, pos, confidence,
																	sigmaD = self.__sigmaD_landmark,
																	sigmaPhi = self.__sigmaAngle_landmark ))
			score.append( probability )

		return score

	def softConstrainScore( self, softConstrain, epsilon = 0.2 ):
		mean = softConstrain[0]
		conv = softConstrain[1]
		try:
			var = multivariate_normal(mean=mean, cov= conv)
		except np.linalg.LinAlgError:
			print "FUCK"
			return [0] * self.nParticle
		
		epsilon = epsilon * var.pdf( mean )

		probability = []
		for p in self._particles:
			pos = list(p.getPosition( ))
			pos[2] = pos[2] % (2*np.pi)

			pdf = var.pdf( pos )
			pob = math.log( pdf ) if pdf > epsilon else -1e+10
			probability.append( pob )

		return probability

	def matchingSensorData( self, sensorMsg, softConstrain = None ):
		
		actualSensorPoints = sensorMsg[ 'scanLine' ]
		boundary = sensorMsg[ 'boundary' ]
		landmarkList = sensorMsg[ 'landmark' ]

		if boundary is None:
			boundary = []

		t0 = timeit.default_timer( )
		scoreLikelihood = self.likelihood_field( actualSensorPoints, boundary )
		t1 = timeit.default_timer( )
		scoreLandmark = self.compareLandmark( landmarkList )
		t2 = timeit.default_timer( )
		if softConstrain is not None:
			scoreSoftConstrain = self.softConstrainScore( softConstrain )
		else:
			scoreSoftConstrain = [0] * self.nParticle
		t3 = timeit.default_timer( )

		finalScore = [ s1+s2+s3 for s1,s2,s3 in zip(scoreLikelihood, scoreLandmark, scoreSoftConstrain) ]
		finalScore = np.array( finalScore )
		finalScore = np.exp( finalScore / 60 )
		t4 = timeit.default_timer( )

		# print "Time for cal point clound score {}".format( t1 - t0 )
		# print "Time for cal landmark score {}".format( t2 - t1 )
		# print "Time for cal softConstrain score {}".format( t3 - t2 )
		# print "Time for cal final score {}".format( t4 - t3 )

		return finalScore

	def _normalizedSofmax( self, scoresList ):

		expo = np.exp( scoresList - scoresList.mean() )

		return expo / expo.sum( )

	def _normalizedBySum( self, scoresList ):

		## This avoid round up problem in numpy
		scoresList = scoresList.astype( np.float64 )

		scoresList -= scoresList.min() 
		sum_ = scoresList.sum( )

		if sum_ == 0:
			return np.ones( (len(scoresList,)), dtype = np.float64) / len( scoresList )

		return scoresList / sum_

	def update( self, sensorMsg, resampling = True, visualize = True, resamplingConstrain = None,
				softConstrain = None ):
		t0 = timeit.default_timer( )
		self.moveParticles( *(sensorMsg[ 'moveMent' ]+sensorMsg["panTilt"]) )

		t1 = timeit.default_timer( )

		t2 = timeit.default_timer( )
		weightsList = self.matchingSensorData( sensorMsg, softConstrain = softConstrain )

		weightAvg = sum( weightsList ) / len( weightsList )

		self._weightAvg_fast += self._decay_fast*( weightAvg - self._weightAvg_fast )
		self._weightAvg_slow += self._decay_slow*( weightAvg - self._weightAvg_slow )

		t3 = timeit.default_timer( )
		rep_robot, varX, varY, varW = self.getAverageParticlePosition( weightsList )
		t4 = timeit.default_timer( )

## HACK : Not use tracked score.
		self._trackedScoreList = np.array([ 1*w2 for w1,w2 in zip(self._trackedScoreList, weightsList) ])

		if resampling:

			trackedScoreList_normalize = self._normalizedBySum( self._trackedScoreList )

			self.resampling( trackedScoreList_normalize, constrain = resamplingConstrain )

## TODO : visittor : Find a better thresholde for re-normalizing tracked weight.
		sumWeight = sum( self._trackedScoreList )
		if sumWeight < 0.5 or sumWeight > 1000:
			self._trackedScoreList = self._normalizedBySum( self._trackedScoreList )

		t5 = timeit.default_timer( )

## NOTE : visittor : Below is visualization only.
		theta = [ r.getPosition()[2] for r in self._particles ]

		if visualize:
			if len(sensorMsg['boundary']) != 0:
				sensorPoint = np.vstack( (sensorMsg['scanLine'], sensorMsg['boundary']) )
			else:
				sensorPoint = sensorMsg['scanLine']
			self.visualize( weightsList.tolist(), sensorMsg, rep_robot )
			# self.loop( sensorMsg )

		t6 = timeit.default_timer( )

		# print "moveParticles :", t1 - t0
		# print "getParticlesRangeValue", t2 - t1
		# print "matchingSensorData", t3 - t2
		# print "getAverageParticlePos", t4 - t3
		# print "resampling", t5 -t4
		# print "Total CALCULATION time", t5 - t0
		# print "Total VISUALIZATION time", t6 -t5
		# print "----------------------------------------"
		# print "\n"

		return rep_robot, varX, varY, varW

	def visualize( self, scoresList, sensorMsg, rep_robot, rep_robot2 = None, rep_robot3 = None ):

		if self._painter is None:
			return

		actualPoint = sensorMsg[ 'scanLine' ]
		boundary = sensorMsg[ 'boundary' ]
		landmarkPos = [ l[1] for l in sensorMsg[ 'landmark' ] ]
		landmarkName = [ l[0] for l in sensorMsg[ 'landmark' ] ]

		self._painter.drawField( )
		
		maxScore = max( scoresList )
		maxIdx = scoresList.index( maxScore )
		for robot, s in zip( self._particles, scoresList ):

		# 	# s = s / maxScore if maxScore != 0 else 0
		# 	# color = ( 255 - int(255.0*s), 0, int(255.0*s) )

		# 	# self._painter.drawRobot( robot, color = color, drawScanline = False )

			self._painter.drawRobot( robot, color = tuple(self.COLORLIST[robot.ID]), drawScanline = False ) 

		pMax = self._particles[ maxIdx ]

		if len( actualPoint ) != 0:
			pointWorldCoor = rep_robot.transformToWorldCoor( actualPoint )

			self._painter.drawPoints( pointWorldCoor )

		if len( boundary ) != 0:
			pointWorldCoor = rep_robot.transformToWorldCoor( boundary )

			self._painter.drawPoints( pointWorldCoor, color = (255,0,0) )

		if len( landmarkPos ) != 0:
			pointWorldCoor = rep_robot.transformToWorldCoor( landmarkPos )

			for p, n in zip(pointWorldCoor, landmarkName):

				if n == 'field_corner':
					color = (255, 0, 255)
				elif n == 'goal':
					color = (0, 255, 255)
				elif n == 'circle':
					color = (255,255,0)
				else:
					color = (0,0,0)

				self._painter.drawPoints( [p], color = color, size = 15 )

		self._painter.drawRobot2( rep_robot, circleColor = (0,0,255) )
		# print scoresList
		# self._painter.drawRobot2( pMax, circleColor = (255,0,0) )

		if rep_robot3 is not None:
			self._painter.drawRobot2( rep_robot3, circleColor = (255, 0, 0) )

		if rep_robot2 is not None:
			self._painter.drawRobot2( rep_robot2, circleColor = (0,255,0) )

		self._painter.plotScoreHistogram( scoresList )

		weightAvg = sum( scoresList ) / len( scoresList )

		self._painter.plotTrackData( [weightAvg, self._weightAvg_slow, self._weightAvg_fast, maxScore ] )

		self._painter.show( )

	def loop( self, sensorMsg ):

		width = 700

		field = np.zeros( (width, width, 3), dtype = np.uint8 )
		field[:,:,1] = 255

		binSize = ( 7, 7 )
		boundary = [ (0, 7), (-3.5, 3.5) ]

		xEdge = np.histogram_bin_edges( np.arange( 10 ), bins = binSize[0], 
				range = (0,1) )
		xEdge = 7 * np.power( xEdge, 1.5 )
		# xEdge *= 7

		yEdge = np.histogram_bin_edges( np.arange( 10 ), bins = binSize[1], 
				range = boundary[1] )

		yEdge += 3.5

		for ii in xEdge:
			ii *= 100
			ii = int( ii )
			ii = 700 - ii
			cv2.line( field, (0, ii), (width, ii), (0,0,0), 1 )

		for ii in yEdge:
			ii *= 100
			ii = int( ii )
			cv2.line( field, (ii, 0), (ii, width), (0,0,0), 1 )

		pt1 = ( width / 2, 550 )
		pt2 = ( (width / 2)-15, width )
		pt3 = ( (width / 2)+15, width )

		triangle_cnt = np.array( [pt1, pt2, pt3] )

		cv2.drawContours(field, [triangle_cnt], 0, (0, 0, 255), -1)

		for x, y in sensorMsg[ "scanLine" ]:

			x = int( x * 100.0 )
			y = int( y * 100.0 )
			y = (width / 2) - y
			x = width - x

			cv2.circle( field, (y,x), 3, (0,0,255), -1 )

		cv2.imshow( "img", field )
		cv2.waitKey( 1 )

	def end( self ):
		self._painter.destroy( )

	@property
	def nParticle( self ):
		return len( self._particles )

	def createRobot( self, x, y, theta ):

		return Robot( x, y, theta, self._camera )